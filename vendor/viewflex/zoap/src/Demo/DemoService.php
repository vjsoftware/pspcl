<?php

namespace Viewflex\Zoap\Demo;


use SoapFault;
use Viewflex\Zoap\Demo\DemoProvider as Provider;

/**
 * An example of a class that is used as a SOAP gateway to application functions.
 */
class DemoService
{
    /*
    |--------------------------------------------------------------------------
    | Public Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Authenticates user/password, returning status of true with token, or throws SoapFault.
     *
     * @param string $user
     * @param string $password
     * @return array
     * @throws SoapFault
     */
    // public function test($request)
    // {
    //
    //   if (Provider::vjTest($request)) {
    //     // code...
    //     return Provider::findProduct($productId);
    //   } else {
    //     header("Status: 401");
    //     throw new SoapFault('SOAP-ENV:Client', 'Test Failed.');
    //   }
    //
    // }
    public function auth(
      $subDivisionCode = '',
      $subDivisionName = '',
      $MRU = '',
      $MR_DOCNumber = '',
      $neighbourMeterId = '',
      $connectedPoleNINNumber,
      $consumerLegacyNumber,
      $contractAcNumber,
      $consumerName,
      $houseNumber,
      $streetNumber,
      $address,
      $category,
      $subCategory,
      $moveInFlag,
      $categoryChangeFlag,
      $categoryChangeDate,
      $oldCategory,
      $oldSubCategory,
      $industryCode,
      $billingCycle,
      $billingCycleType,
      $billingGroup,
      $triVectorMeterFlag,
      $installation,
      $meterMnfSerialNumber,
      $meterManufacturerName,
      $meterSAPSerialNumber,
      $multiplicationFactor,
      $overallMf,
      $oldMf,
      $numberOfDigits,
      $lineCtRatio,
      $externalPtRatio,
      $phaseCode,
      $sanctionedLoadKW,
      $contractedDemandKVA,
      $sanctionedLoadChangeFlag,
      $sanctionedLoadChangeDate,
      $oldSanctionedLoadKW,
      $admissibleVoltage,
      $supplyVoltage,
      $noOfBOardEmployees6,
      $noOfBOardEmployees7,
      $noOfBOardEmployees8,
      $scheduleMeterReadDate,
      $previousReadingDate,
      $previousBillDate,
      $previousReadingKWH,
      $previousReadingKVA,
      $previousReadingKVAH,
      $previousMeterStatus,
      $previousBillType,
      $meterChangeFlag,
      $meterChangeDate,
      $totalOldMeterConsumptionUnitsKWH,
      $totalOldMeterConsumptionUnitsKVAH,
      $oldMeterTotalRentAmount,
      $oldMeterTotalServiceRentAmount,
      $oldMeterTotalServiceChargeAmount,
      $oldMeterTotalMBCRentAmount,
      $newMeterInitialReadingKWH,
      $newMeterInitialReadingKVAH,
      $reconnectionFlag,
      $reconnectionDate,
      $consumptionBeforeDisConnectionKWH,
      $consumptionBeforeDisConnectionKVAH,
      $periodBeforeDisConnection,
      $edExemptedFlag,
      $edExemptionExpiry,
      $octraiChargesApplicableFlag,
      $meterRentRate,
      $serviceRentRate,
      $peakLoadExemptionCharges,
      $peakLoadExemptionChargesExpiry,
      $MMTSCorrectionFactor,
      $shuntCapacitorChargesApplicableFlag,
      $capacityOfCapacitor,
      $MBCRentRate,
      $serviceChargeRate,
      $seasonStartDate,
      $seasonEndDate,
      $sundryChargesSOP,
      $sundryChargesED,
      $sundryChargesOCTRAI,
      $sundryAllowancesSOP,
      $sundryAllowancesED,
      $sundryAllowancesOCTRAI,
      $waterChargesProvisionalAmount,
      $otherCharges,
      $roundAdjustmentAmount,
      $adjustmentAmountSOP,
      $adjustmentAmountED,
      $adjustmentAmountOCTRAI,
      $provisionalAdjustmentAmountSOP,
      $provisionalAdjustmentAmountED,
      $provisionalAdjustmentAmountOCTRAI,
      $arrearSOPCurrentYear,
      $arrearEDCurrentYear,
      $arrearOCTRAICurrentYear,
      $arrearSOPPreviousYear,
      $arrearEDPreviousYear,
      $arrearOCTRAIPreviousYear,
      $advanceConsumptionDeposit,
      $courtCaseAmount,
      $previousKWHCycle1,
      $previousKWHCycle2,
      $previousKWHCycle3,
      $previousKWHCycle4,
      $previousKWHCycle5,
      $previousKWHCycle6,
      $averageKWHofAbove6Cycles,
      $samePeriodLastYearConsumptionKWH,
      $PF1,
      $PF2,
      $PF3,
      $averageofAboveThreePF,
      $standardPFOfConsumerCategory,
      $MD1,
      $MD2,
      $gaushalaFlag,
      $arrearWaterChargesCurrentYear,
      $arrearWaterChargesPreviousYear,
      $dataExpiryDate,
      $maxOfAboveSixMDI,
      $factorForCustomerCategory,
      $DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory,
      $HValueInLDHFSupplyHoursPerDayForCustomerCategory,
      $previousTotalEstimatedConsumptionKWH,
      $previousTotalEstimatedConsumptionKVAH,
      $previousEstimatedMDI,
      $nearestCashCounter,
      $complaintCenterPhoneNumber,
      $previousPaymentStatus,
      $reasonForAdjustment,
      $miscExpensesDetails,
      $miscExpenses,
      $meterSecurityAmount,
      $waterChargesApplicableFlag,
      $transformerCode,
      $oldMeterRent,
      $expirationDateOfOldMeterRent,
      $oldMBCRent,
      $expirationDateOfOldMBCRent,
      $oldServiceCharges,
      $expirationDateOfOldServiceCharges,
      $meterLocation,
      $meterType,
      $version,
      $ML,
      $MT,
      $mobileNo,
      $feederCode,
      $SCWSDAmountWithHeld,
      $previousFyCons,
      $muncipalTaxApplicableFlag,
      $PLogicApplicableFlag,
      $limitForPNore
      )
    {
      return ['address' => $address];

    }

    /**
     * Returns boolean authentication result using given token or user/password.
     *
     * @param string $token
     * @param string $user
     * @param string $password
     * @return bool
     */
    // public function ping($token = '', $user = '', $password = '')
    // {
    //     return Provider::authenticate($token, $user, $password);
    // }

    /**
     * Returns a product by id.
     *
     * @param int $productId
     * @param string $token
     * @param string $user
     * @param string $password
     * @return \Viewflex\Zoap\Demo\Types\Product
     * @throws SoapFault
     */
    // public function getProduct($productId, $token = '', $user = '', $password = '')
    // {
    //
    //     if (! $productId) {
    //         header("Status: 400");
    //         throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
    //     }
    //
    //     if (! Provider::authenticate($token, $user, $password)) {
    //         header("Status: 401");
    //         throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
    //     }
    //
    //     return Provider::findProduct($productId);
    // }

    /**
     * Returns an array of products by search criteria.
     *
     * @param \Viewflex\Zoap\Demo\Types\KeyValue[] $criteria
     * @param string $token
     * @param string $user
     * @param string $password
     * @return \Viewflex\Zoap\Demo\Types\Product[]
     * @throws SoapFault
     */
    public function getProducts($criteria = [], $token = '', $user = '', $password = '')
    {

        if (! Provider::authenticate($token, $user, $password)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }

        // return Provider::findProductsBy(self::arrayOfKeyValueToArray($criteria));
        return Provider::findProductsBy(self::arrayOfKeyValueToArray($criteria));

    }

    /*
    |--------------------------------------------------------------------------
    | Utility
    |--------------------------------------------------------------------------
    */

    /**
     * Convert array of KeyValue objects to associative array, non-recursively.
     *
     * @param \Viewflex\Zoap\Demo\Types\KeyValue[] $objects
     * @return array
     */
    // protected static function arrayOfKeyValueToArray($objects)
    // {
    //     $return = array();
    //     foreach ($objects as $object) {
    //         $return[$object->key] = $object->value;
    //     }
    //
    //     return $return;
    // }

}
