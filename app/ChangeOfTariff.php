<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeOfTariff extends Model
{
    protected $fillable = [
      'sysId',
      'inputCode',
      'sheetNo',
      'pageNo',
      'numberOfEnties',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'tariffType',
      'bE1st',
      'bE2nd',
      'bE3rd',
      'bE4th',
      'bE5th',
      'numberOfBEs',
      'voltClass',
      'govtConnectionCode',
      'typesOfNRS',
      'remarks',
    ];
}
