<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SundryAllowance extends Model
{
  protected $fillable = [
    'sysId',
    'inputCode',
    'sheetNo',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'accountNo',
    'checkDigit',
    'sop',
    'ed',
    'oct',
    'code',
    'sundryRegister',
    'sundryItemNo',
    'controlNo',
    'remarks',
  ];
}
