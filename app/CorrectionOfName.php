<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorrectionOfName extends Model
{
    protected $fillable = [
      'sysId',
      'inputCode',
      'sheetNo',
      'pageNo',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'nameOfConsumer',
      'address',
      'connectedLoad',
      'additionalServiceRentals',
      'remarks',
    ];
}
