<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SundryCharge extends Model
{
  protected $fillable = [
    'sysId',
    'inputCode',
    'sheetNo',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'accountNo',
    'checkDigit',
    'sop',
    'ed',
    'oct',
    'idf',
    'cowCess',
    'waterCess',
    'code',
    'sundryRegister',
    'sundryPageNo',
    'noOfSundryCharges',
    'periodFrom',
    'periodTo',
    'served',
    'remarks',
  ];
}
