<?php

namespace App\Http\Controllers;

use App\CorrectionOfMeterParticular;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CorrectionOfMeterParticularController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function particular()
    {
        return view('correctionOfMeterParticulars');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = CorrectionOfMeterParticular::create($request->except('_token'));
        return redirect('/correctionOfMeterParticulars');
    }


    public function particularDataAjax(Request $request)
    {

      $data = CorrectionOfMeterParticular::create($request->except('_token'));

      if ($data) {
        $status = [
          'status' => 'success'
        ];
      } else {
        $status = [
          'status' => 'error'
        ];
      }

      return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CorrectionOfMeterParticular  $correctionOfMeterParticular
     * @return \Illuminate\Http\Response
     */
    public function show(CorrectionOfMeterParticular $correctionOfMeterParticular)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CorrectionOfMeterParticular  $correctionOfMeterParticular
     * @return \Illuminate\Http\Response
     */
    public function edit(CorrectionOfMeterParticular $correctionOfMeterParticular)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CorrectionOfMeterParticular  $correctionOfMeterParticular
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CorrectionOfMeterParticular $correctionOfMeterParticular)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CorrectionOfMeterParticular  $correctionOfMeterParticular
     * @return \Illuminate\Http\Response
     */
    public function destroy(CorrectionOfMeterParticular $correctionOfMeterParticular)
    {
        //
    }
}
