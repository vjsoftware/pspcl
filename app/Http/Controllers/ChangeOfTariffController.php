<?php

namespace App\Http\Controllers;

use App\ChangeOfTariff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangeOfTariffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tariff()
    {
        return view('ChangeOfTariff');
    }

    public function tariffChange(Request $request)
    {
      $this->validate($request,[
        'ledgerGroup' => 'required',
        ]);


        // $data = new ChangeOfTariff();
        $data = ChangeOfTariff::create($request->except('_token'));
        // $data->ledgerGroup = $request->ledgerGroup;
        // $data->accountNo = $request->accountNo;
        // $data->checkDigit = $request->checkDigit;
        // $data->tariffType = $request->tariffType;
        // $data->bE1st = $request->bE1st;
        // $data->bE2nd = $request->bE2nd;
        // $data->bE3rd = $request->bE3rd;
        // $data->bE4th = $request->bE4th;
        // $data->bE5th = $request->bE5th;
        // $data->numberOfBEs = $request->numberOfBEs;
        // $data->voltClass = $request->voltClass;
        // $data->govtConnectionCode = $request->govtConnectionCode;
        // $data->typesOfNRS = $request->typesOfNRS;
        // $data->remarks = $request->remarks;
        //
        // $data->save();

        return redirect('/changeOfTariff');

    }

    public function tariffChangeAjax(Request $request)
    {
      // $this->validate($request,[
      //   'ledgerGroup' => 'required',
      //   ]);
      //
        $data = ChangeOfTariff::create($request->except('_token'));

        if ($data) {
          $status = [
            'status' => 'success'
          ];
        } else {
          $status = [
            'status' => 'error'
          ];
        }

        return $data;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChangeOfTariff  $changeOfTariff
     * @return \Illuminate\Http\Response
     */
    public function show(ChangeOfTariff $changeOfTariff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChangeOfTariff  $changeOfTariff
     * @return \Illuminate\Http\Response
     */
    public function edit(ChangeOfTariff $changeOfTariff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChangeOfTariff  $changeOfTariff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChangeOfTariff $changeOfTariff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChangeOfTariff  $changeOfTariff
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChangeOfTariff $changeOfTariff)
    {
        //
    }
}
