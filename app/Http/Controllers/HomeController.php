<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\File;
use App\Bill;
use App\sapInput;

use Auth;
use Mapper;

use DateTime;
use Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function map()
    {
      Mapper::map(20.5937, 78.9629, ['center' => false, 'type' => 'ROADMAP']);
      Mapper::marker(13.0827, 80.2707, ['title' => 'Chennai']);
      Mapper::marker(17.3850, 78.4867, ['title' => 'Hyderabad']);
      Mapper::marker(13.64597, 79.42752, ['title' => 'vJ']);
        return view('map');
    }

    public function my_first_api()
    {
      $data = [
        'name' => 'vJ',
        'mobile' => '9490501349'
      ];

      // return $data;
      return response()->json($data);
    }

    public function upload()
    {
      return view('upload');
    }

    public function uploadStore(Request $request)
    {
      $this->validate($request,[
          'file' => 'required|mimes:txt',
        ]);
      $fileName = $request->file;
      $fileNameWithExtension = $fileName->getClientOriginalName();
      $fileName1 = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
      $fileExt1 = $fileName->getClientOriginalExtension();
      $photoToSave1 = "newfile".'_'.time().'.'. $fileExt1;
      $path1 = $fileName->move('uploadedtext', $photoToSave1);

      $user = Auth::user()->id;
      $file = new File();
      $file->file = $photoToSave1;
      $file->user_id = $user;

      $file->save();

      // return $file;
      $message = [];
      $url = config('app.url').'/uploadedtext/'.$photoToSave1;
      $fileResource  = fopen($url, "r");
      if ($fileResource) {
          while (($line = fgets($fileResource)) !== false) {
            $newLine = "$line";
            // return var_dump(substr($newLine, 542, 3));
            $bill = new Bill();
            $bill->fileId = $file->id;
            $bill->masterKey = substr($newLine, 0, 12);
            $bill->masterKey1 = substr($newLine, 12, 11);
            $bill->subDivisionCode = substr($newLine, 12, 3);
            $bill->accountNumber = substr($newLine, 7, 4);
            $bill->meterNo = substr($newLine, 23, 10);
            $bill->subDivisionName = substr($newLine, 33, 20);
            $bill->consumerName = substr($newLine, 53, 25);
            $bill->consumerAddress = substr($newLine, 78, 34);
            $bill->phaseCode = substr($newLine, 112, 1);
            $bill->tariffType = substr($newLine, 113, 1);
            $bill->connectedLoadInKW = substr($newLine, 114, 7);
            $bill->meterMultiplier = substr($newLine, 121, 8);
            $bill->perviousReading = substr($newLine, 129, 8);
            $bill->perviousCode = substr($newLine, 137, 1);
            $bill->previousReadingDate = substr($newLine, 138, 10);
            $bill->octroyFlag = substr($newLine, 148, 1);
            $bill->arrearSOP = substr($newLine, 149, 10);
            $bill->arrearED = substr($newLine, 159, 10);
            $bill->arrearOctroi = substr($newLine, 169, 10);
            $bill->surchargeToBeLeaved = substr($newLine, 179, 10);
            $bill->averageAdjustmentSOP = substr($newLine, 189, 10);
            $bill->averageAdjustmentED = substr($newLine, 199, 10);
            $bill->averageAdjustmentOctroi = substr($newLine, 209, 10);
            $bill->sundarySOP = substr($newLine, 219, 10);
            $bill->sundaryED = substr($newLine, 229, 10);
            $bill->sundaryOctroi = substr($newLine, 239, 10);
            $bill->allowanceSOP = substr($newLine, 249, 10);
            $bill->allowanceED = substr($newLine, 259, 10);
            $bill->allowanceOctroi = substr($newLine, 269, 10);
            $bill->currentMeterRent = substr($newLine, 279, 3);
            $bill->previousRoundingAmount = substr($newLine, 282, 2);
            $bill->beConcession = substr($newLine, 284, 5);
            $bill->beNo = substr($newLine, 289, 1);
            $bill->billingGroup = substr($newLine, 290, 1);
            $bill->billingCycle = substr($newLine, 291, 2);
            $bill->voltageClass = substr($newLine, 293, 1);
            $bill->paymentCollectionCenter = substr($newLine, 294, 40);
            $bill->complaintCenterPhoneNo = substr($newLine, 334, 40);
            $bill->lineCtRatio = substr($newLine, 374, 5);
            $bill->meterCtRatio = substr($newLine, 379, 5);
            $bill->overallMultiplingFactor = substr($newLine, 384, 7);
            $bill->securityDeposit = substr($newLine, 391, 6);
            $bill->others = substr($newLine, 397, 6);
            $bill->othersvoltageSurchargeAmpunt = substr($newLine, 403, 6);
            $bill->previousArrear = substr($newLine, 409, 9);
            $bill->currentArrear = substr($newLine, 418, 9);
            $bill->adjustmentAmount = substr($newLine, 427, 7);
            $bill->reason = substr($newLine, 434, 10);
            $bill->sundaryChargeDetail = substr($newLine, 444, 20);
            $bill->consumtionOfOldMeter = substr($newLine, 464, 8);
            $bill->fixedCharges = substr($newLine, 472, 8);
            $bill->fuelCostAdjustmentCharges = substr($newLine, 480, 8);
            $bill->periodOfAdjustmentDetail = substr($newLine, 488, 10);
            $bill->lastSixMonthConsumption = substr($newLine, 498, 36);
            $bill->rentChargeUpto = substr($newLine, 534, 8);
            $bill->currentServiceRent = substr($newLine, 542, 3);
            $bill->avrageLNI = substr($newLine, 545, 6);
            $bill->avrageOthers = substr($newLine, 551, 6);
            $bill->cowCessCode = substr($newLine, 557, 1);
            $bill->waterSewarageChargesCode = substr($newLine, 558, 1);
            $bill->specialCategoryCode = substr($newLine, 559, 2);
            $bill->arrearIDF = substr($newLine, 561, 9);
            $bill->arrearCowCess = substr($newLine, 570, 9);
            $bill->arrearWaterSewarageChargesCode = substr($newLine, 579, 9);
            $bill->sundryIDF = substr($newLine, 588, 9);
            $bill->sundryCowCess = substr($newLine, 597, 9);
            $bill->sundryWaterSewarageChargesCode = substr($newLine, 606, 9);
            $bill->allowanceIDF = substr($newLine, 615, 9);
            $bill->allowanceCowCess = substr($newLine, 624, 9);
            $bill->allowanceWaterSewarageChargesCode = substr($newLine, 633, 9);
            $bill->avgAdjustmentIDF = substr($newLine, 642, 9);
            $bill->avgAdjustmentCowCess = substr($newLine, 651, 9);
            $bill->avgAdjustmentWaterSewarageChargesCode = substr($newLine, 660, 9);
            $bill->save();
            // echo substr($newLine, 23, 10);
            // echo "<pre>";
            return redirect('/success');
          }
          fclose($fileResource);
      } else {
        $message['status'] = "Cannot read File";
      }

      return redirect('/success');
    }

    public function sapDataStore(Request $request)
    {
      $this->validate($request,[
          'file' => 'required|mimes:txt|max:25000',
        ]);
      $fileName = $request->file;
      $fileNameWithExtension = $fileName->getClientOriginalName();
      $fileName1 = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
      $fileExt1 = $fileName->getClientOriginalExtension();
      $photoToSave1 = $fileName1.'_'.time().'.'. $fileExt1;
      $path1 = $fileName->move('sapdata', $photoToSave1);

      $user = Auth::user()->id;
      $file = new File();
      $file->file = $photoToSave1;
      $file->user_id = $user;

      $file->save();

      // return $file;

      $url = config('app.url').'/sapdata/'.$photoToSave1;
      $fileResource  = fopen($url, "r");
      if ($fileResource) {
          while (($line = fgets($fileResource)) !== false) {
            $newLine = "$line";
            $textFromLine = explode(",", $newLine);
            // return var_dump(substr($newLine, 542, 3));
            $bill = new sapInput();
            // $bill->fileId = $file->id;
            $bill->subDivisionCode = $textFromLine[0];
            $bill->subDivisionName = $textFromLine[1];
            $bill->MRU = $textFromLine[2];
            $bill->MR_DOCNumber = $textFromLine[3];
            $bill->neighbourMeterId = $textFromLine[4];
            $bill->connectedPoleNINNumber = $textFromLine[5];
            // return $textFromLine[5];
            $bill->consumerLegacyNumber = $textFromLine[6];
            $bill->contractAcNumber = $textFromLine[7];
            $bill->consumerName = $textFromLine[8];
            $bill->houseNumber = $textFromLine[9];
            $bill->streetNumber = $textFromLine[10];
            $bill->address = $textFromLine[11];
            $bill->category = $textFromLine[12];
            $bill->subCategory = $textFromLine[13];
            $bill->moveInFlag = $textFromLine[14];
            $bill->categoryChangeFlag = $textFromLine[15];
            $bill->categoryChangeDate = $textFromLine[16];
            $bill->oldCategory = $textFromLine[17];
            $bill->oldSubCategory = $textFromLine[18];
            $bill->industryCode = $textFromLine[19];
            $bill->billingCycle = $textFromLine[20];
            $bill->billingCycleType = $textFromLine[21];
            $bill->billingGroup = $textFromLine[22];
            $bill->triVectorMeterFlag = $textFromLine[23];
            $bill->installation = $textFromLine[24];
            $bill->meterMnfSerialNumber = $textFromLine[25];
            $bill->meterManufacturerName = $textFromLine[26];
            $bill->meterSAPSerialNumber = $textFromLine[27];
            $bill->multiplicationFactor = $textFromLine[28];
            $bill->overallMf = $textFromLine[29];
            $bill->oldMf = $textFromLine[30];
            $bill->numberOfDigits = $textFromLine[31];
            $bill->lineCtRatio = $textFromLine[32];
            $bill->externalPtRatio = $textFromLine[33];
            $bill->phaseCode = $textFromLine[34];
            $bill->sanctionedLoadKW = $textFromLine[35];
            $bill->contractedDemandKVA = $textFromLine[36];
            $bill->sanctionedLoadChangeFlag = $textFromLine[37];
            $bill->sanctionedLoadChangeDate = $textFromLine[38];
            $bill->oldSanctionedLoadKW = $textFromLine[39];
            $bill->admissibleVoltage = $textFromLine[40];
            $bill->supplyVoltage = $textFromLine[41];
            $bill->noOfBOardEmployees6 = $textFromLine[42];
            $bill->noOfBOardEmployees7 = $textFromLine[43];
            $bill->noOfBOardEmployees8 = $textFromLine[44];
            $bill->scheduleMeterReadDate = $textFromLine[45];
            $bill->previousReadingDate = $textFromLine[46];
            $bill->previousReadingDateFormated = date('Y-m-d');
            $bill->previousBillDate = $textFromLine[47];
            $bill->previousBillDateFormated = date('Y-m-d');
            $bill->previousReadingKWH = $textFromLine[48];
            $bill->previousReadingKVA = $textFromLine[49];
            $bill->previousReadingKVAH = $textFromLine[50];
            $bill->previousMeterStatus = $textFromLine[51];
            $bill->previousBillType = $textFromLine[52];
            $bill->meterChangeFlag = $textFromLine[53];
            $bill->meterChangeDate = $textFromLine[54];
            $bill->meterChangeDateFormated = date('Y-m-d');
            $bill->totalOldMeterConsumptionUnitsKWH = $textFromLine[55];
            $bill->totalOldMeterConsumptionUnitsKVAH = $textFromLine[56];
            $bill->oldMeterTotalRentAmount = $textFromLine[57];
            $bill->oldMeterTotalServiceRentAmount = $textFromLine[58];
            $bill->oldMeterTotalServiceChargeAmount = $textFromLine[59];
            $bill->oldMeterTotalMBCRentAmount = $textFromLine[60];
            $bill->newMeterInitialReadingKWH = $textFromLine[61];
            $bill->newMeterInitialReadingKVAH = $textFromLine[62];
            $bill->reconnectionFlag = $textFromLine[63];
            $bill->reconnectionDate = $textFromLine[64];
            $bill->consumptionBeforeDisConnectionKWH = $textFromLine[65];
            $bill->consumptionBeforeDisConnectionKVAH = $textFromLine[66];
            $bill->periodBeforeDisConnection = $textFromLine[67];
            $bill->edExemptedFlag = $textFromLine[68];
            $bill->edExemptionExpiry = $textFromLine[69];
            $bill->edExemptionExpiryFormated = date('Y-m-d');
            $bill->octraiChargesApplicableFlag = $textFromLine[70];
            $bill->meterRentRate = $textFromLine[71];
            $bill->serviceRentRate = $textFromLine[72];
            if ($textFromLine[73] == '') {
              $textFromLine[73] = 0.00;
            } else {
              $textFromLine[73] = $textFromLine[73];
            }
            $bill->peakLoadExemptionCharges = $textFromLine[73];
            // return $textFromLine[74];
            $bill->peakLoadExemptionChargesExpiry = $textFromLine[74];
            $bill->peakLoadExemptionChargesExpiryFormated = date('Y-m-d');
            $bill->MMTSCorrectionFactor = $textFromLine[75];
            $bill->shuntCapacitorChargesApplicableFlag = $textFromLine[76];
            $bill->capacityOfCapacitor = $textFromLine[77];
            $bill->MBCRentRate = $textFromLine[78];
            $bill->serviceChargeRate = $textFromLine[79];
            $bill->seasonStartDate = $textFromLine[80];
            $bill->seasonStartDateFormated = date('Y-m-d');
            $bill->seasonEndDate = $textFromLine[81];
            $bill->seasonEndDateFormated = date('Y-m-d');
            $bill->sundryChargesSOP = $textFromLine[82];
            $bill->sundryChargesED = $textFromLine[83];
            $bill->sundryChargesOCTRAI = $textFromLine[84];
            $bill->sundryAllowancesSOP = $textFromLine[85];
            $bill->sundryAllowancesED = $textFromLine[86];
            $bill->sundryAllowancesOCTRAI = $textFromLine[87];
            $bill->waterChargesProvisionalAmount = $textFromLine[88];
            $bill->otherCharges = $textFromLine[89];
            $bill->roundAdjustmentAmount = $textFromLine[90];
            $bill->adjustmentAmountSOP = $textFromLine[91];
            $bill->adjustmentAmountED = $textFromLine[92];
            $bill->adjustmentAmountOCTRAI = $textFromLine[93];
            $bill->provisionalAdjustmentAmountSOP = $textFromLine[94];
            $bill->provisionalAdjustmentAmountED = $textFromLine[95];
            $bill->provisionalAdjustmentAmountOCTRAI = $textFromLine[96];
            $bill->arrearSOPCurrentYear = $textFromLine[97];
            $bill->arrearEDCurrentYear = $textFromLine[98];
            $bill->arrearOCTRAICurrentYear = $textFromLine[99];
            $bill->arrearSOPPreviousYear = $textFromLine[100];
            $bill->arrearEDPreviousYear = $textFromLine[101];
            $bill->arrearOCTRAIPreviousYear = $textFromLine[102];
            if (strpos($textFromLine[103], '.00-')) {
              $textFromLine[103] = "-".str_replace(".00-", ".00", $textFromLine[103]);
            }
            $bill->advanceConsumptionDeposit = $textFromLine[103];
            // return $textFromLine[103];
            $bill->courtCaseAmount = $textFromLine[104];
            $bill->previousKWHCycle1 = $textFromLine[105];
            $bill->previousKWHCycle2 = $textFromLine[106];
            $bill->previousKWHCycle3 = $textFromLine[107];
            $bill->previousKWHCycle4 = $textFromLine[108];
            $bill->previousKWHCycle5 = $textFromLine[109];
            $bill->previousKWHCycle6 = $textFromLine[110];
            $bill->averageKWHofAbove6Cycles = $textFromLine[111];
            $bill->samePeriodLastYearConsumptionKWH = $textFromLine[112];
            $bill->PF1 = $textFromLine[113];
            $bill->PF2 = $textFromLine[114];
            $bill->PF3 = $textFromLine[115];
            $bill->averageofAboveThreePF = $textFromLine[116];
            $bill->standardPFOfConsumerCategory = $textFromLine[117];
            $bill->MD1 = $textFromLine[118];
            $bill->MD2 = $textFromLine[119];
            $bill->gaushalaFlag = $textFromLine[120];
            $bill->arrearWaterChargesCurrentYear = $textFromLine[121];
            $bill->arrearWaterChargesPreviousYear = $textFromLine[122];
            $bill->dataExpiryDate = $textFromLine[123];
            $bill->dataExpiryDateFormated = date('Y-m-d');
            $bill->maxOfAboveSixMDI = $textFromLine[124];
            $bill->factorForCustomerCategory = $textFromLine[125];
            $bill->DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory = $textFromLine[126];
            $bill->HValueInLDHFSupplyHoursPerDayForCustomerCategory = $textFromLine[127];
            $bill->previousTotalEstimatedConsumptionKWH = $textFromLine[128];
            $bill->previousTotalEstimatedConsumptionKVAH = $textFromLine[129];
            $bill->previousEstimatedMDI = $textFromLine[130];
            $bill->nearestCashCounter = $textFromLine[131];
            $bill->complaintCenterPhoneNumber = $textFromLine[132];
            $bill->previousPaymentStatus = $textFromLine[133];
            $bill->reasonForAdjustment = $textFromLine[134];
            $bill->miscExpensesDetails = $textFromLine[135];
            $bill->miscExpenses = $textFromLine[136];
            $bill->meterSecurityAmount = $textFromLine[137];
            $bill->waterChargesApplicableFlag = $textFromLine[138];
            $bill->transformerCode = $textFromLine[139];
            $bill->oldMeterRent = $textFromLine[140];
            $bill->expirationDateOfOldMeterRent = $textFromLine[141];
            $bill->expirationDateOfOldMeterRentFormated = date('Y-m-d');
            $bill->oldMBCRent = $textFromLine[142];
            $bill->expirationDateOfOldMBCRent = $textFromLine[143];
            $bill->expirationDateOfOldMBCRentFormated = date('Y-m-d');
            $bill->oldServiceCharges = $textFromLine[144];
            $bill->expirationDateOfOldServiceCharges = $textFromLine[145];
            $bill->expirationDateOfOldServiceChargesFormated = date('Y-m-d');
            $bill->meterLocation = $textFromLine[146];
            $bill->meterType = $textFromLine[147];
            $bill->version = $textFromLine[148];
            $bill->ML = $textFromLine[149];
            $bill->MT = $textFromLine[150];
            $bill->mobileNo = $textFromLine[151];
            $bill->feederCode = $textFromLine[152];
            $bill->SCWSDAmountWithHeld = $textFromLine[153];
            if ($textFromLine[154] == '') {
              $textFromLine[154] = 0;
            } else {
              $textFromLine[154] = $textFromLine[154];
            }
            $bill->previousFyCons = $textFromLine[154];
            // return $textFromLine[154];
            $bill->muncipalTaxApplicableFlag = $textFromLine[155];
            $bill->PLogicApplicableFlag = $textFromLine[156];
            $bill->limitForPNore = $textFromLine[157];
            $bill->save();
            // echo substr($newLine, 23, 10);
            // echo "<pre>";
          }
          fclose($fileResource);
      }

      return redirect('/success');
    }

    public function uploadSuccess()
    {
      return view('uploadSuccess');
    }

    public function bill()
    {
      return view('bill');
    }

    public function billSearch(Request $request)
    {
      return redirect("/search/masterkey/$request->masterKey/$request->status");
    }

    public function searchView($masterkey, $status)
    {
      $bill = Bill::where('masterKey', $masterkey)->first();
      // return $bill;


      $data = [
        'bill' => $bill,
        'status' => $status
      ];
      // return $data;
      return view('search')->with('data', $data);
    }

    public function units(Request $request)
    {
      $bill = Bill::where('masterKey', $request->masterKey)->first();
      // return $request;


      $tempCurrentUnits = 0;
      $perviousReading = $bill->perviousReading;
      $oMLF = $bill->overallMultiplingFactor;
      $tariffType = $bill->tariffType;
      $billingGroup = $bill->billingGroup;
      $beConcession = $bill->beConcession;

      $currentStatus = $request->status;


      if ($bill->perviousCode == 'D' || $bill->perviousCode == 'R' || $bill->perviousCode == 'M' || $bill->perviousCode == 'F' || $bill->perviousCode == 'G' || $bill->perviousCode == "C") {
        $currentStatus = $bill->perviousCode;
      } else {
        // User Input
        $currestStatus = "O";
      }

      // if ($bill->perviousCode == "C") {
      //   $currentStatus = $bill->perviousCode;
      // }





      switch ($currentStatus) {
        case 'L':
          $currentUnits = $bill->avrageLNI;
          break;
        case 'D':
          $currentUnits = $bill->avrageOthers;
          break;
        case 'R':
          $currentUnits = $bill->avrageOthers;
          break;
        case 'M':
          $currentUnits = $bill->avrageOthers;
          break;
        case 'F':
          $currentUnits = $bill->avrageOthers;
          break;
        case 'G':
          $currentUnits = $bill->avrageOthers;
          break;
        case 'S':
          $currentUnits = $bill->avrageOthers;
          break;
        case 'C':
          $currentUnits = $bill->avrageOthers;
          break;
        case 'J':
          $currentUnits = $bill->avrageOthers;
          break;
        case 'E':

          break;

        default:
          $currentStatus = "O";
          break;
      }

      $currentReading = $request->units;

      // return $currentStatus;

      if ($currentReading < $perviousReading) {
        $message = "Please check reading";
        $currentStatus = "D";
        $currentUnits = $bill->avrageOthers;
      }

      if ($bill->perviousCode == "E") {
        if ($currentReading > $perviousReading) {
            $currentUnitsTemp = $currentReading - $perviousReading; // Units
            $currentUnits = $currentUnitsTemp + $bill->consumtionOfOldMeter;
            if ($oMLF > 0) {
              $currentUnits = $currentUnits * $oMLF;
            }
        } else {
          $currentStatus = $bill->perviousCode;
          $currentUnits = $bill->avrageOthers;
        }
      }

      if ($currentStatus == "O") {
        if ($currentReading > $perviousReading) {
          $currentUnits = $currentReading - $perviousReading; // Units

          if ($oMLF > 0) {
            $currentUnits = $currentUnits * $oMLF;
          }
        }

      }

      $previousReadingDate = $bill->previousReadingDate;
      $previousReadingDateFormated = Carbon\Carbon::createFromFormat('d/m/Y', $previousReadingDate)->format('Y-m-d');
      $currentDate = Date('Y-m-d');
      $lastBillDate = Carbon\Carbon::parse($previousReadingDateFormated);
      $current = Carbon\Carbon::parse($currentDate);
      $lengthOfAd = $current->diffInDays($lastBillDate);

      if ($currentStatus == "O") {
        if ($tariffType == 3) {
          if ($beConcession == 6) {
            $concessionUnits = 100;
          } elseif ($beConcession == 7) {
            $concessionUnits = 125;
          } elseif ($beConcession == 8) {
            $concessionUnits = 155;
          }

          if ($currentUnits > $concessionUnits) {
            $tempCurrentUnits = $currentUnits - $concessionUnits;
          } else {
            $tempCurrentUnits = 0;
          }
        }
        if ($tariffType == 5 || $tariffType == 6) {
          $tempCurrentUnits = 200;
        }
      } else {
        $tempCurrentUnits = $currentUnits;
      }

      if ($billingGroup == '1' || $billingGroup == '2' || $billingGroup == '3' || $billingGroup == '4') {
        # 60 Days
        if ($lengthOfAd >= 56 && $lengthOfAd <= 64) {
          $days = 60;

        } else {
          $days = $lengthOfAd;
        }

      } elseif ($billingGroup == '8') {
         // 30 Days
        if ($lengthOfAd >= 26 && $lengthOfAd <= 34) {
          $days = 30;
        } else {
          $days = $lengthOfAd;
        }
    }
      $normalEc = $this->ecCalculator($currentUnits, $billingGroup, $days, $tariffType, $beConcession);
      // return $currentUnits;
      $conecessionEc = $this->ecCalculator($tempCurrentUnits, $billingGroup, $days, $tariffType, $beConcession);
      echo "Concession Ec Charge - " . $conecessionEc;
      echo "<br>";
      echo "Billing Days - " . $lengthOfAd . ' - ' .$days;
      echo "<br>";
      echo "Units - " . $currentUnits;
      echo "<br>";
      echo "Concession Units - " . $tempCurrentUnits;
      echo "<br>";
      echo "Nornal EC Charge - " . $normalEc;
      echo "<br>";
      $chargablEc = $normalEc - $conecessionEc;
      echo "Chargable EC Charge - " . ($chargablEc) . ( ' -- 1');
      echo "<br>";
      $fixedCharge = $bill->fixedCharges;
      $fixedCharge = ($fixedCharge / 30) * $days;
      echo "Fixed Charge - " . $fixedCharge . ( ' -- 8 Fix Chanrges ' );
      echo "<br>";
      echo "Other Charge - " . $fixedCharge . ( ' -- 9 Other Chanrges ' );
      echo "<br>";
      echo "PFSURC - Relaction " . $fixedCharge . ( ' -- 10 PFSURC - Relaction ' );
      echo "<br>";
      $fuelCharge = $currentUnits * 0.03;
      echo "Fuel Cost Adjustment ( FCA ) - " . $fuelCharge . ( ' -- 7 ' );
      echo "<br>";
      $edCharges = (($normalEc + $fixedCharge + $fuelCharge) * 13) / 100;
      echo "Electricity Duty ( ED ) - " . $edCharges. (' -- 2 ');
      echo "<br>";
      echo "Oct - 0" . ( '-- 3' ) ;
      echo "<br>";
      $octroyFlag = $bill->octroyFlag;
      $mcTax = 0;
      if ($octroyFlag == "Y") {
        $mcTax = (($normalEc + $fixedCharge + $fuelCharge) * 2) / 100;
      } else {
        $mcTax = 0;
      }
      echo "Muncipal Council Tax ( MC Tax ) - " . $mcTax;
      echo "<br>";
      $cowCess = 0;
      $cowCessCode = $bill->cowCessCode;
      if ($cowCessCode == "Y") {
        $cowCess = $currentUnits * 0.02;
      } else {
        $cowCess = 0;
      }
      echo "Cow Cess - " . $cowCess . ( ' -- 11 ' );
      echo "<br>";
      $meterRent = $bill->currentMeterRent;
      $meterRent = ($meterRent / 30) * $days;
      echo "Meter Rent - " . $meterRent . ( ' -- 5 ' );
      echo "<br>";
      $serviceRent = $bill->currentServiceRent;
      $serviceRent = ($serviceRent / 30) * $days;
      echo "Service Rent - " . $serviceRent . ( ' -- 6 ' );
      echo "<br>";
      $idfCharges = (($normalEc + $fixedCharge + $fuelCharge) * 5) / 100;
      echo "Infrastructure Development Fund ( IDF ) - " . $idfCharges . ( ' -- 4' );
      echo "<br>";

}

    public function roundToTheNearestAnything($value, $roundTo)
    {
        $mod = $value%$roundTo;
        return $value+($mod<($roundTo/2)?-$mod:$roundTo-$mod);
    }

    // billingGroup = By Month ( 1,2,3,4 )  / Month ( 8 );
    // tariffType = Customer Type ( 'Domestic - 1,3,5,6', 'Non Domestic - 0,2,4,8' )

    public function ecCalculator($currentUnits, $billingGroup, $lengthOfAd, $tariffType)
    {
      // echo $currentUnits;
      // return 0;
      if ($billingGroup == '1' || $billingGroup == '2' || $billingGroup == '3' || $billingGroup == '4') {
        # 60 Days
        if ($lengthOfAd >= 56 && $lengthOfAd <= 64) {
          $days = 60;

        } else {
          $days = $lengthOfAd;
        }

      } elseif ($billingGroup == '8') {
         // 30 Days
        if ($lengthOfAd >= 26 && $lengthOfAd <= 34) {
          $days = 30;
        } else {
          $days = $lengthOfAd;
        }
    }

    // return $days;



        if ($tariffType == 1 || $tariffType == 3 || $tariffType == 5 || $tariffType == 6) {
          if ($billingGroup == 1 || $billingGroup == 2 || $billingGroup == 3 || $billingGroup == 4) {
            $byMonthUits = $currentUnits / 2;
            $slab1Units = ((100 / 30) * $days) / 2;
            $slab2Units = ((200 / 30) * $days) / 2;
            $slab3Units = ((200 / 30) * $days) / 2;
            $slab4Units = ((500 / 30) * $days) / 2;
          } elseif ($billingGroup == 8) {
            $byMonthUits = $currentUnits;
            $slab1Units = (100 / 30) * $days;
            $slab2Units = (200 / 30) * $days;
            $slab3Units = (200 / 30) * $days;
            $slab4Units = (500 / 30) * $days;

          }
        }


        if ($tariffType == 0 || $tariffType == 2 || $tariffType == 4 || $tariffType == 8) {
          // echo $currentUnits;
          // return 0;
          if ($billingGroup == 1 || $billingGroup == 2 || $billingGroup == 3 || $billingGroup == 4) {
            $byMonthUits = $currentUnits / 2;
            $slab1Units = ((100 / 30) * $days) / 2;
            $slab2Units = ((400 / 30) * $days) / 2;
            // $slab3Units = $byMonthUits;
          } elseif ($billingGroup == 8) {
            $byMonthUits = $currentUnits;
            $slab1Units = (100 / 30) * $days;
            $slab2Units = (400 / 30) * $days;
            // $slab3Units = (500 / 30) * $days;

          }


        }
        // return $byMonthUits;
        // return $slab1Units;

        $ec1 = 0;
        $ec2 = 0;
        $ec3 = 0;
        $ec4 = 0;


          // if ($tariffType == 1 || $tariffType == 3 || $tariffType == 5 || $tariffType == 6) {
          //   // code...
          // }
          //
          // if ($tariffType == 0 || $tariffType == 2 || $tariffType == 4 || $tariffType == 8) {
          //   // code...
          // }


          if ($tariffType == 1 || $tariffType == 3 || $tariffType == 5 || $tariffType == 6) {
            $slab1rate = 4.91;
            $slab2rate = 6.51;
            $slab3rate = 7.12;
            $slab4rate = 7.33;
            if ($byMonthUits > $slab1Units) {
              $ec1 = $slab1Units * $slab1rate;
              if (($byMonthUits - $slab1Units) > $slab2Units) {
                $ec1 = $slab1Units * $slab1rate;
                $ec2 = $slab2Units * $slab2rate;
                if (($byMonthUits - ($slab1Units + $slab2Units)) > $slab3Units) {
                  $ec1 = $slab1Units * $slab1rate;
                  $ec2 = $slab2Units * $slab2rate;
                  $ec3 = $slab3Units * $slab3rate;
                  if (($byMonthUits - ($slab1Units + $slab2Units + $slab3Units)) > 0) {
                    $ec1 = $slab1Units * $slab1rate;
                    $ec2 = $slab2Units * $slab2rate;
                    $ec3 = $slab3Units * $slab3rate;
                    $ec4 = ($byMonthUits - ($slab1Units + $slab2Units + $slab3Units)) * $slab4rate;
                  }
                } else {
                  $ec1 = $slab1Units * $slab1rate;
                  $ec2 = $slab2Units * $slab2rate;
                  $ec3 = ($byMonthUits - ($slab1Units + $slab2Units)) * $slab3rate;
                }
              } else {
                $ec1 = $slab1Units * $slab1rate;
                $ec2 = ($byMonthUits - ($slab1Units)) * $slab2rate;
              }
            } else {
              $ec1 = $byMonthUits * $slab1rate;
            }
          }

          if ($tariffType == 0 || $tariffType == 2 || $tariffType == 4 || $tariffType == 8) {
            $slab1rate = 6.86;
            $slab2rate = 7.12;
            $slab3rate = 7.24;
            if ($byMonthUits > $slab1Units) {
              // return $byMonthUits;
              // return 9;
              $ec1 = $slab1Units * $slab1rate;
              if (($byMonthUits - $slab1Units) > $slab2Units) {
                $ec1 = $slab1Units * $slab1rate;
                $ec2 = $slab2Units * $slab2rate;
                // return $slab2Units;
                if (($byMonthUits - ($slab1Units + $slab2Units)) > 0) {
                  $ec1 = $slab1Units * $slab1rate;
                  $ec2 = $slab2Units * $slab2rate;
                  $ec3 = ($byMonthUits - ($slab1Units + $slab2Units)) * $slab3rate;
                }
              } else {
                $ec1 = $slab1Units * $slab1rate;
                $ec2 = ($byMonthUits - ($slab1Units)) * $slab2rate;
              }
              // return 'ttttt';
            } else {
              $ec1 = $byMonthUits * $slab1rate;
            }
          }

          // return $ec3;


        $totalEc = $ec1 + $ec2 + $ec3 + $ec4;
        // $totalEc = $totalEc;
        $totalEc = ($totalEc / 30) * $days;
        return $totalEc;

    }


}
