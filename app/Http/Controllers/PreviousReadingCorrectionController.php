<?php

namespace App\Http\Controllers;

use App\PreviousReadingCorrection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PreviousReadingCorrectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function previous()
    {
        return view('previousReadingCorrection');
    }


    public function previousReading(Request $request)
    {
      $this->validate($request,[
        'ledgerGroup' => 'required',
        ]);

        // $data = new PreviousReadingCorrection();
        // return $request;
        $data = PreviousReadingCorrection::create($request->except('_token'));
        // $data->ledgerGroup = $request->ledgerGroup;
        // $data->accountNo = $request->accountNo;
        // $data->checkDigit = $request->checkDigit;
        // $data->correctedReading = $request->correctedReading;
        // $data->averageConsumption = $request->averageConsumption;
        // $data->remarks = $request->remarks;
        // $data->save();
        return redirect('/previousReadingCorrection');
    }

    public function previousReadingAjax(Request $request)
    {

      $data = PreviousReadingCorrection::create($request->except('_token'));

      if ($data) {
        $status = [
          'status' => 'success'
        ];
      } else {
        $status = [
          'status' => 'error'
        ];
      }

      return $status;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreviousReadingCorrection  $previousReadingCorrection
     * @return \Illuminate\Http\Response
     */
    public function show(PreviousReadingCorrection $previousReadingCorrection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreviousReadingCorrection  $previousReadingCorrection
     * @return \Illuminate\Http\Response
     */
    public function edit(PreviousReadingCorrection $previousReadingCorrection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreviousReadingCorrection  $previousReadingCorrection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreviousReadingCorrection $previousReadingCorrection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreviousReadingCorrection  $previousReadingCorrection
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreviousReadingCorrection $previousReadingCorrection)
    {
        //
    }
}
