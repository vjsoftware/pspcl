<?php

namespace App\Http\Controllers;

use App\SundryAllowance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SundryAllowanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allowance()
    {
        return view('sundryAllowance');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = SundryAllowance::create($request->except('_token'));
      return redirect('sundryAllowance');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SundryAllowance  $sundryAllowance
     * @return \Illuminate\Http\Response
     */
    public function show(SundryAllowance $sundryAllowance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SundryAllowance  $sundryAllowance
     * @return \Illuminate\Http\Response
     */
    public function edit(SundryAllowance $sundryAllowance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SundryAllowance  $sundryAllowance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SundryAllowance $sundryAllowance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SundryAllowance  $sundryAllowance
     * @return \Illuminate\Http\Response
     */
    public function destroy(SundryAllowance $sundryAllowance)
    {
        //
    }
}
