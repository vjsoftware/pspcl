<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;
use Auth;

class EmployeeRegisterController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function register(Request $request)
  {
    $this->validate($request,[
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
      'role' => 'required',
      ]);

      $data = new User();
      $data->name = $request->name;
      $data->email = $request->email;
      $data->password = Hash::make($request['password']);
      $data->role = $request->role;
      $data->save();
      $data->roles()->attach($request['role']);

      return redirect('/userSuccess');
  }
  public function sbaregister(Request $request)
  {
    $this->validate($request,[
      'name' => 'required|string|max:255',
      'fatherName' => 'required|string|max:255',
      'address' => 'required',
      'email' => 'required|string|max:255|unique:users',
      'mobile'=> 'required|min:10|max:10',
      'aadhaarNo'=> 'required|max:16',
      'photo' => 'required|mimes:jpeg,jpg,png|max:2500',
      'aadhaarPhoto' => 'required|mimes:jpeg,jpg,png|max:2500',
      'password' => 'required|string|min:8',

      ]);

      if ($request->hasFile('photo')) {

          $file1 = $request->photo;
          $fullPhotoNameWithExt1 = $file1->getClientOriginalName();
          $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
          $fileExt1 = $file1->getClientOriginalExtension();
          $photoToSave1 = $fileName1.'_'.time().'.'. $fileExt1;
          $path1 = $file1->move('profilepic', $photoToSave1);
        }else {
          $photoToSave1 = ' ';
        }
      if ($request->hasFile('aadhaarPhoto')) {

          $file2 = $request->aadhaarPhoto;
          $fullPhotoNameWithExt2 = $file2->getClientOriginalName();
          $fileName2 = pathinfo($fullPhotoNameWithExt2, PATHINFO_FILENAME);
          $fileExt2 = $file2->getClientOriginalExtension();
          $photoToSave2 = $fileName2.'_'.time().'.'. $fileExt2;
          $path2 = $file2->move('aadharpic', $photoToSave2);
        }else {
          $photoToSave2 = ' ';
        }

      $data = new User();
      $data->name = $request->name;
      $data->fatherName = $request->fatherName;
      $data->address = $request->address;
      $data->email = $request->email;
      $data->mobile = $request->mobile;
      $data->aadhaarNo = $request->aadhaarNo;
      $data->photo = $photoToSave1;
      $data->aadhaarPhoto = $photoToSave2;
      $data->password = Hash::make($request['password']);
      $data->role = 2;
      $data->save();
      $data->roles()->attach($request['role']);

      return redirect('/sbaSuccess');
  }
  public function changepassword(Request $request)
  {
    $this->validate($request,[
      'password' => 'required|string|min:6|confirmed',
      ]);
      $userId = Auth::user()->id;
      $data = User::find($userId);
      $data->password = Hash::make($request['password']);
      $data->save();

      return redirect('/passwordSuccess');
  }

  public function edit($id)
  {
    // return $id;
    $meterReader = User::find($id);
    return view('auth/sbaregisteredit')->with('data', $meterReader);
  }

  public function sbaRegisterUpdate(Request $request)
  {
    // return $request;
    $meterReader = User::find($request->id);
    $meterReader->name = $request->name;
    $meterReader->fatherName = $request->fatherName;
    $meterReader->address = $request->address;
    $meterReader->permanentAddress = $request->permanentAddress;
    $meterReader->communicationAddress = $request->communicationAddress;
    $meterReader->mobile = $request->mobile;
    $meterReader->aadhaarNo = $request->aadhaarNo;
    $meterReader->simNumber = $request->simNumber;
    $meterReader->imeiNumber = $request->imeiNumber;
    if ($request->password != null) {
      $meterReader->password = Hash::make($request->password);
    }
    $meterReader->save();
    return redirect('/meter-readers');
  }
}
