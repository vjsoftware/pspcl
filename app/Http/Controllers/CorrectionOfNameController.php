<?php

namespace App\Http\Controllers;

use App\CorrectionOfName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CorrectionOfNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function name()
    {
        return view('correctionOfName');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request;
        $data = CorrectionOfName::create($request->except('_token'));
        return redirect('correctionOfName');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CorrectionOfName  $correctionOfName
     * @return \Illuminate\Http\Response
     */
    public function show(CorrectionOfName $correctionOfName)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CorrectionOfName  $correctionOfName
     * @return \Illuminate\Http\Response
     */
    public function edit(CorrectionOfName $correctionOfName)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CorrectionOfName  $correctionOfName
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CorrectionOfName $correctionOfName)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CorrectionOfName  $correctionOfName
     * @return \Illuminate\Http\Response
     */
    public function destroy(CorrectionOfName $correctionOfName)
    {
        //
    }
}
