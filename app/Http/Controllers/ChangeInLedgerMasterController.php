<?php

namespace App\Http\Controllers;

use App\ChangeInLedgerMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangeInLedgerMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ledger()
    {
        return view('changeInLedgerMaster');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = ChangeInLedgerMaster::create($request->except('_token'));
      return redirect('changeInLedgerMaster');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChangeInLedgerMaster  $changeInLedgerMaster
     * @return \Illuminate\Http\Response
     */
    public function show(ChangeInLedgerMaster $changeInLedgerMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChangeInLedgerMaster  $changeInLedgerMaster
     * @return \Illuminate\Http\Response
     */
    public function edit(ChangeInLedgerMaster $changeInLedgerMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChangeInLedgerMaster  $changeInLedgerMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChangeInLedgerMaster $changeInLedgerMaster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChangeInLedgerMaster  $changeInLedgerMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChangeInLedgerMaster $changeInLedgerMaster)
    {
        //
    }
}
