<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Bill;
use App\Billed;

use Mapper;

class ReportsController extends Controller
{
    public function pendingBills()
    {
      $meterReaders = User::where('role', 2)->get();
      return view('pendingBills')->with('data', $meterReaders);
    }

    public function pendingBillsSearch(Request $request)
    {
      $this->validate($request,[
        'divisionCode' => 'required',
        'meterReader' => 'required',
        ]);

      if ($request->divisionCode == "all") {
        $pendingBills = Bill::where('status', 1)
                              ->where('meterReader', $request->meterReader)
                              ->get();
      } else {
        $pendingBills = Bill::where('divisionCode', $request->divisionCode)
                              ->where('status', 1)
                              ->where('meterReader', $request->meterReader)
                              ->get();
      }
      // return $request;
      if (count($pendingBills)) {
        $message = "Success";
      } else {
        $message = "No Data Found";
      }
      $data = [
        'pendingList' => $pendingBills,
        'message' => $message
      ];
      return view('pendingList')->with('data', $data);
    }

    public function trackMeterReader()
    {
      $meterReaders = User::where('role', 2)->get();
      $data = [
        'meterReaders' => $meterReaders
      ];
      return view('trackMeterReader')->with('data', $data);
    }

    public function trackMeterReaderSearch(Request $request)
    {
      $this->validate($request,[
        'from' => 'required',
        'to' => 'required',
        'subDivisionCode' => 'required',
        'meterReader' => 'required',
        ]);

      $billsSearch = Billed::whereBetween('blPresentReadingDateFormated', [$request->from, $request->to])
      ->where('blSubDivisionCode', $request->subDivisionCode)
      ->where('blMeterReader', $request->meterReader)
      ->where('blLat', '!=', '0.0')
      ->where('blLong', '!=', '0.0')
      ->get();
      $meterReaders = User::where('role', 2)->get();
      $data = [
        'meterReaders' => $meterReaders,
        'bills' => $billsSearch
      ];

      // echo count($data['bills']);
      // return $data['bills'];

      Mapper::map(20.5937, 78.9629, ['center' => false, 'type' => 'ROADMAP', 'marker' => false]);
      foreach ($billsSearch as $geo) {
        Mapper::marker($geo->blLat, $geo->blLong, ['title' => $geo->blConsumerName]);
      }
      return view('trackMeterReader')->with('data', $data);
    }

    public function miniLedger()
    {

      return view('miniLedger');
    }

    public function miniLedgerSearch(Request $request)
    {
      $this->validate($request,[
        'from' => 'required',
        'to' => 'required',
        'subDivisionCode' => 'required',
        'billingGroup' => 'required',
        'billingCycle' => 'required',
        ]);
      // return $request;
      $billsSearch = Billed::whereBetween('created_at', [$request->from, $request->to])
      ->where('blSubDivisionCode', $request->subDivisionCode)
      ->where('blBillingGroup', $request->billingGroup)
      ->where('blBillingCycle', $request->billingCycle)
      ->get();
      // return $billsSearch;
      return view('miniLedgerSearch')->with('data', $billsSearch);
    }

    public function meterReaders()
    {
      $meterReaders = User::where('role', 2)->get();
      return view('meterReaders')->with('data', $meterReaders);
    }

    public function meterReaderPerformance()
    {
      return view('meterReaderPerformance');
    }

    public function meterReaderPerformanceSearch(Request $request)
    {
      $this->validate($request,[
        'from' => 'required',
        'to' => 'required',
        'subDivisionCode' => 'required',
        'billingGroup' => 'required',
        'billingCycle' => 'required'
        ]);

      $meterReaders = User::where('role', 2)->get();
      $data = [
        'meterReaders' => $meterReaders,
        'from' => $request->from,
        'to' => $request->to,
        'subDivisionCode' => $request->subDivisionCode,
        'billingGroup' => $request->billingGroup,
        'billingCycle' => $request->billingCycle,
      ];

      // return $data['from'];

      return view('meterReaderPerformance')->with('data', $data);
    }
    public function exceptional()
    {
      return view('exceptionalReports');
    }
    public function summary()
    {
      return view('summaryReports');
    }
    public function category()
    {
      return view('categoryWiseReports');
    }
    public function division()
    {
      return view('divisionWisePerfomanceReports');
    }
    public function multiple()
    {
      return view('multipleBillsSameLocation');
    }
    public function arrears()
    {
      return view('viewArrears');
    }
}
