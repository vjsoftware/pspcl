<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\File;
use App\Bill;
use App\Billed;

use DateTime;
use Carbon;

class BillingController extends Controller
{
    public function viewBillingData()
    {
      $files = File::all();
      return view('viewBillingData')->with('data', $files);
    }

    public function viewBillingDataRecords($fileId)
    {
      $bills = Bill::where('fileId', $fileId)->select('masterKey1', 'meterNo', 'subDivisionName', 'consumerName', 'consumerAddress', 'connectedLoadInKW', 'perviousCode')->get();
      return view('viewBillingDataRecords')->with('data', $bills);
    }

    public function view()
    {
      return view('exportOutputFile');
    }

    public function viewSpotBill()
    {
      return view('exportSpotBillOutputFile');
    }

    public function exportBillingDataRecords(Request $request)
    {
      $bills =  Billed::where('blSubDivisionCode', $request->subDivisionCode)
                        ->where('blBillingGroup', $request->billingGroup)
                        ->where('blBillingCycle', $request->billingCycle)->get();
      // return $bills;
      $txt = '';
      foreach ($bills as $bill) {
        $cashDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blDueDateByCash)->format('d/m/y');
        $chequeDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blDueDateByCheque)->format('d/m/y');
        // return $cashDate;
        // $cashDate = Carbon\Carbon::parse($dateFormated);
        $details = Bill::where('masterkey', $bill->blMasterKey)->first();
        // return $details;
        if ($bill->blBillingGroup == 8) {
          if ($bill->blTariffType == 1 || $bill->blTariffType == 3 || $bill->blTariffType == 5 || $bill->blTariffType == 6) {
            $billType = "R";
          } elseif ($bill->blTariffType == 0 || $bill->blTariffType == 2 || $bill->blTariffType == 4 || $bill->blTariffType == 8) {
            $billType = "B";
          }
        } else {
          if ($bill->blTariffType == 1 || $bill->blTariffType == 3 || $bill->blTariffType == 5 || $bill->blTariffType == 6) {
            $billType = "D";
          } elseif ($bill->blTariffType == 0 || $bill->blTariffType == 2 || $bill->blTariffType == 4 || $bill->blTariffType == 8) {
            $billType = "N";
          }
        }
        $txt .= $this->addSpace($bill->blMasterKey, 12).',';
        $txt .= $this->addZeros($billType, 1).',';
        $txt .= $this->addZeros($bill->blBillingCycle, 2).',';
        $txt .= $this->addZeros($bill->blBillingGroup, 1).',';
        $txt .= $this->addZeros($bill->blBillingGroup, 1).',';
        $txt .= $this->addSpace($bill->blConsumerName, 20).',';
        $txt .= $this->addSpace(trim($details->consumerAddress), 25).',';
        $txt .= $this->vlen($bill->blNetSop, 10).',';
        $txt .= $this->vlen($bill->blNetEd, 9).',';
        $txt .= $this->vlen($bill->blNetOctroi, 9).',';
        $txt .= $this->vlen($bill->blAmountBeforeDue, 10).',';
        $txt .= $this->vlen($bill->blSurCharge, 9).',';
        $txt .= $this->addZerosSignN('', 8).',';
        $txt .= $this->vlen($bill->blAmountAfterDue, 10).',';
        // $txt .= $this->addZerosSignN($bill->blAmountAfterDue, 9).',';
        $txt .= $this->addSpace($cashDate, 8).',';
        $txt .= $this->addSpace($chequeDate, 8).',';
        $txt .= $this->addZeros('', 9).',';
        $txt .= $this->addZeros('', 1).',';
        $txt .= $this->addSpace($cashDate, 8).',';
        $txt .= $this->addSpace($chequeDate, 8).',';
        $txt .= $this->vlen($bill->bltotalIDFWithSign, 9).',';
        $txt .= $this->vlen($bill->blTotalCowCessWithSign, 9).',';
        $txt .= $this->vlen($bill->blTotalCurrentWaterSewarageChargeWithSign, 9).',';
        $txt .= "\r\n";
      }
      return response($txt)
                ->withHeaders([
                    'Content-Type' => 'text/plain',
                    'Cache-Control' => 'no-store, no-cache',
                    'Content-Disposition' => 'attachment; filename="logs.txt',
                ]);
      return $bills;
    }

    public function exportNewBillingDataRecords(Request $request)
    {
      // return $request;
      $bills =  Billed::where('blSubDivisionCode', $request->subDivisionCode)
                        ->where('blBillingGroup', $request->billingGroup)
                        ->where('blBillingCycle', $request->billingCycle)->get();
      $txt = '';
      foreach ($bills as $bill) {
        $details = Bill::where('masterkey', $bill->blMasterKey)->first();
        $time = $bill->created_at->format('h:i');
        // $cashDate = Carbon\Carbon::createFromFormat('Y-m-d', $bill->blDueDateByCash)->format('d/m/Y');
        // $chequeDate = Carbon\Carbon::createFromFormat('Y-m-d', $bill->blDueDateByCheque)->format('d/m/Y');
        // return $dateFormated;
        // $cashDate = Carbon\Carbon::parse($dateFormated);
        $txt .= $this->addSpace($bill->blMasterKey, 12);
        $txt .= $this->addSpace($bill->blConsumerName, 25);
        $txt .= $this->addSpace($details->consumerAddress, 25);
        $txt .= $this->addZeros($bill->blTariffType, 1);
        $txt .= $this->addZeros($bill->blBillingGroup, 1);
        $txt .= $this->addZeros($bill->blBillingCycle, 2);
        $txt .= $this->addZeros((int) $bill->blCurrentMeterRent, 6);
        $txt .= $this->addZeros((int) $bill->blCurrentServiceRent, 6);
        $txt .= $this->addZeros((int) $bill->blPresentReading, 8);
        $txt .= $this->addSpace($bill->blPresentReadingDate, 10);
        $txt .= $this->addSpace(strtoupper($bill->blPresentStatus), 1);
        $txt .= $this->addZeros((int) $bill->blUnitsConsumed, 8);
        $txt .= $this->addZeros($bill->blDays, 5);
        $txt .= $this->addZeros($bill->blMMCFlag, 3);
        $txt .= $this->addZeros($bill->blCurrentSop, 10);
        $txt .= $this->addZeros($bill->blCurrentEd, 10);
        $txt .= $this->addZeros($bill->blCurrentOctroi, 10);
        if ($bill->blNetSop > 0) {
          $blNetSop = '+'.$this->addZeros($bill->blNetSop, 9);
        } elseif ($bill->blNetSop == 0) {
          $blNetSop = $this->addZeros('', 10);
        } else {
          $abs = abs($bill->blNetSop);
          $abs = sprintf("%.2f", $abs);
          // $abs = number_format($abs, 2);
          $blNetSop = '-'.$this->addZeros($abs, 9);
          // $blNetSop = $abs;
        }
        if ($bill->blNetEd > 0) {
          $blNetEd = '+'.$this->addZeros($bill->blNetEd, 9);
        } elseif ($bill->blNetEd == 0) {
          $blNetEd = $this->addZeros('', 10);
        } else {
          $abs = abs($bill->blNetEd);
          $abs = sprintf("%.2f", $abs);
          // $abs = number_format($abs, 2);
          $blNetEd = '-'.$this->addZeros($abs, 9);
          // $blNetSop = $abs;
        }
        if ($bill->blNetOctroi > 0) {
          $blNetOctroi = '+'.$this->addZeros($bill->blNetOctroi, 9);
        } elseif ($bill->blNetOctroi == 0) {
          $blNetOctroi = $this->addZeros('', 10);
        } else {
          $abs = abs($bill->blNetOctroi);
          $abs = sprintf("%.2f", $abs);
          // $abs = number_format($abs, 2);
          $blNetOctroi = '-'.$this->addZeros($abs, 9);
          // $blNetSop = $abs;
        }

        $txt .= $this->vle($bill->blNetSop, 10);
        $txt .= $this->vle($bill->blNetEd, 10);
        $txt .= $this->vle($bill->blNetOctroi, 10);
        $txt .= $this->addSpaceFront($bill->blCurrentRoundingAmt, 2);
        $txt .= $this->vle($bill->blAmountBeforeDue, 10);
        // $txt .= $this->addZerosSign($bill->blAmountBeforeDue, 10);
        $txt .= $this->addZerosSign((int)$bill->blSurCharge, 7);
        $txt .= $this->addSpace($bill->blDueDateByCash, 10);
        $txt .= $this->addSpace($bill->blDueDateByCheque, 10);
        $txt .= $this->vlen($bill->blAmountAfterDue, 7);
        $txt .= $this->addSpace($time, 5);
        $txt .= $this->addSpace('', 1);
        $txt .= $this->addSpace('', 6);
        $txt .= $this->addSpace('', 6);
        $txt .= $this->addZeros('', 5);
        $txt .= $this->addZeros('', 5);
        $txt .= $this->addZeros('', 4);
        $txt .= $this->addSpace('', 10);
        $txt .= $this->addZeros('', 7);
        $txt .= $this->addZeros('', 7);
        $txt .= $this->addZeros(substr((int) $bill->blConcessionalUnits, 0, 4), 4);
        $txt .= $this->addSpace('', 4);
        $txt .= $this->addZeros('', 4);
        $txt .= $this->addZeros('', 1);
        $txt .= $this->addZeros('', 1);
        $txt .= $this->vlen($bill->blCurrentIDFWithSign, 9);
        $txt .= $this->vlen($bill->blCurrentCowCessWithSign, 9);
        $txt .= $this->vlen($bill->blTotalCurrentWaterSewarageChargeWithSign, 9);
        $txt .= $this->vlen($bill->bltotalIDFWithSign, 9);
        $txt .= $this->vlen($bill->blTotalCowCessWithSign, 9);
        $txt .= $this->vlen($bill->blTotalCurrentWaterSewarageChargeWithSign, 9);
        $txt .= $this->vlen($bill->blCurrentFCAChargesWithSign, 9);
        $txt .= $this->vlen($bill->blCurrentFixedChargesWithSign, 9);
        // $txt .= $this->addZeros($bill->blSurCharge, 8).',';
        // $txt .= $this->addZeros('', 8).',';
        // $txt .= $this->addZeros($bill->blAmountAfterDue, 9).',';
        // $txt .= $this->addSpace($bill->blDueDateByCash, 8).',';
        // $txt .= $this->addSpace($bill->blDueDateByCheque, 8).',';
        // $txt .= $this->addZeros('', 9).',';
        // $txt .= $this->addZeros('', 1).',';
        // $txt .= $this->addSpace('', 8).',';
        // $txt .= $this->addSpace('', 8).',';
        // $txt .= $this->addZeros($bill->bltotalIDFWithSign, 8).',';
        // $txt .= $this->addZeros($bill->blTotalCowCessWithSign, 8).',';
        // $txt .= $this->addZeros($bill->blTotalCurrentWaterSewarageChargeWithSign, 8);
        $txt .= "\r\n";
      }
      return response($txt)
                ->withHeaders([
                    'Content-Type' => 'text/plain',
                    'Cache-Control' => 'no-store, no-cache',
                    'Content-Disposition' => 'attachment; filename="logs.txt',
                ]);
      return $bills;
    }



    public function viewDs()
    {
      return view('exportDsOutputFile');
    }

    public function exportDs(Request $request)
    {
      // return $request;
      $bills =  Billed::where('blSubDivisionCode', $request->subDivisionCode)
                        ->where('blBillingGroup', $request->billingGroup)
                        ->where('blBillingCycle', $request->billingCycle)->get();
      $txt = '';
      foreach ($bills as $bill) {
        $details = Bill::where('masterkey', $bill->blMasterKey)->first();
        $issueDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blPresentReadingDate)->format('d-M-Y');
        $issueYear = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blPresentReadingDate)->format('Y');
        $cashDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blDueDateByCash)->format('d-M-Y');
        $chequeDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blDueDateByCheque)->format('d-M-Y');
        $presentReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blPresentReadingDate)->format('d-M-Y');
        $previousReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $details->previousReadingDate)->format('d-M-Y');
        $finYear = Date('Y');
        $finYearPlus = Date('y')+1;

        $tariffType = '';
        if ($bill->blTariffType == 1) {
          $tariffType = "DS";
        } elseif ($bill->blTariffType == 3) {
          $tariffType = "BE";
        } elseif ($bill->blTariffType == 5) {
          $tariffType = "WSD";
        } elseif ($bill->blTariffType == 6) {
          $tariffType = "BPL";
        } elseif ($bill->blTariffType == 0) {
          $tariffType = "BO";
        } elseif ($bill->blTariffType == 2) {
          $tariffType = "NRS";
        } elseif ($bill->blTariffType == 4) {
          $tariffType = "RLY";
        } elseif ($bill->blTariffType == 8) {
          $tariffType = "CGO";
        }

        if ($details->previousArrer > 0) {
          $previousArrear = '+'.(int)$details->previousArrer;
        } elseif ($details->previousArrer == 0) {
          $previousArrear = '';
        } else {
          $previousArrear = $details->previousArrer;
        }
        if ($details->others > 0) {
          $others = '+'.(int)$details->others;
        } elseif ($details->others == 0) {
          $others = '';
        } else {
          $others = $details->others;
        }

        if ($details->currentArrear > 0) {
          $currentArrear = '+'.(int)$details->currentArrear;
        } elseif ($details->currentArrear == 0) {
          $currentArrear = '';
        } else {
          $currentArrear = $details->currentArrear;
        }

        $subdryCharge = ($details->sundaryED) + ($details->sundryIDF) + ($details->sundarySOP) + ($details->sundaryOctroi) + ($details->sundryCowCess);
        if ($subdryCharge > 0) {
          $subdryCharge = '+'.(int)$subdryCharge;
        } elseif ($subdryCharge == 0) {
          $subdryCharge = '';
        } else {
          $subdryCharge = $subdryCharge;
        }
        $subdryAllowance = ($details->allowanceED) + ($details->allowanceIDF) + ($details->allowanceSOP) + ($details->allowanceOctroi) + ($details->allowanceCowCess);
        if ($subdryAllowance > 0) {
          $subdryAllowance = '+'.(int)$subdryAllowance;
        } elseif ($subdryAllowance == 0) {
          $subdryAllowance = '';
        } else {
          $subdryAllowance = $subdryAllowance;
        }

        if ($bill->blCurrentRoundingAmt >= 0) {
          $currentRoundAMount = '+'.$bill->blCurrentRoundingAmt;
        } else {
          $currentRoundAMount = $bill->blCurrentRoundingAmt;
        }

        if ($details->previousRoundingAmount >= 0) {
          $previousReadindAmount = '+'.(int)$details->previousRoundingAmount;
        } else {
          $previousReadindAmount = $details->previousRoundingAmount;
        }
        $blPresentStatusCheck = strtoupper($bill->blPresentStatus);
        if ($blPresentStatusCheck == "O" || $blPresentStatusCheck == "A" || $blPresentStatusCheck == "E" || $blPresentStatusCheck == "T" || $blPresentStatusCheck == "X" || $blPresentStatusCheck == "H" || $blPresentStatusCheck == "W") {
          $blPresentStatus = "OK";
        } elseif ($blPresentStatusCheck == "D" || $blPresentStatusCheck == "M" || $blPresentStatusCheck == "R" || $blPresentStatusCheck == "F" || $blPresentStatusCheck == "G" || $blPresentStatusCheck == "S" || $blPresentStatusCheck == "C" || $blPresentStatusCheck == "L" || $blPresentStatusCheck == "N" || $blPresentStatusCheck == "I") {
          $blPresentStatus = "AVG";
        } else {
          $blPresentStatus = "MMC";
        }
        // return $dateFormated;
        // $cashDate = Carbon\Carbon::parse($dateFormated);
        // return $consumerAddress->address;
        $txt .= $this->addSpace($bill->blMasterKey, 12).'"';
        $txt .= $this->addSpace($bill->blConsumerName, 20).'"';
        $txt .= $this->addSpace(trim($details->consumerAddress), 20).'"';
        $txt .= $this->addSpace($issueDate, 11).'"';
        $txt .= $this->addSpace($cashDate, 11).'"';
        $txt .= $this->addSpace($chequeDate, 11).'"';
        $txt .= $this->addSpace('', 20).'"'; // ( 17 )
        $txt .= $this->addSpace(trim($details->subDivisionName), 18).'"';
        $txt .= $this->addZeros('', 6).'"'; // ( Bill Number )
        $txt .= $this->addSpace($presentReadingDate, 11).'"';
        $txt .= $this->addSpace($previousReadingDate, 11).'"';
        $txt .= $this->addSpaceFront($bill->blDays, 4).'"';
        $txt .= $this->addSpace($tariffType, 3).'"';
        $txt .= $this->addSpaceFront((double) $details->connectedLoadInKW, 7).'"';
        $txt .= $this->addSpace('', 7).'"';
        $txt .= $this->addSpace(trim($details->meterNo), 9).'"';
        $txt .= $this->addSpace($details->securityDeposit, 7).'"';
        $txt .= $this->addSpace($blPresentStatus, 3).'"';
        $txt .= $this->addZeros($bill->blBillingCycle, 2).'"';
        $txt .= $this->addZeros($bill->blBillingGroup, 1).'"';
        $txt .= $this->addSpaceFront($bill->blPresentReading, 8).'"'; // ( prt-mtr-rdg-curr )
        $txt .= $this->addSpaceFront($details->perviousReading, 8).'"'; // ( prt-mtr-rdg-prev )
        $txt .= $this->addSpace($details->lineCtRatio, 5).'"';
        $txt .= $this->addSpace($details->meterCtRatio, 5).'"';
        $txt .= $this->addSpaceFront(sprintf("%.2f", $details->meterMultiplier), 7).'"';
        $txt .= $this->addSpaceFront(sprintf("%.2f", $details->overallMultiplingFactor), 7).'"';
        $txt .= $this->addSpace(strtoupper($bill->blPresentStatus), 1).'"'; // ( 48 )
        $newCon = (int) $bill->blUnitsConsumed - (int) $bill->blUnitsConsumedOld;
        $txt .= $this->addSpaceFront($newCon, 6).'"'; // new
        $txt .= $this->addZeros((int) $bill->blUnitsConsumedOld, 6).'"';
        $txt .= $this->addSpaceFront((int) $bill->blUnitsConsumed, 6).'"';
        $txt .= $this->addSpaceFront((int) $bill->blCurrentSop, 8).'"';
        $txt .= $this->addSpaceFront((int) $bill->blCurrentEd, 7).'"';
        $txt .= $this->addSpaceFront((int) $bill->blCurrentOctroi, 6).'"';
        $txt .= $this->addSpaceFront((int) $bill->blCurrentMeterRent, 4).'"';
        $txt .= $this->addSpaceFront((int) $bill->blCurrentServiceRent, 4).'"';
        $txt .= $this->addSpaceFront((int) $details->adjustmentAmount, 7).'"'; // (57)
        $txt .= $this->addSpaceFront(trim($details->periodOfAdjustmentDetail), 8).'"'; // (58)
        $txt .= $this->addSpace(trim($details->reason), 12).'"'; // (59)
        $txt .= $this->addSpaceFront((int) $bill->blConcessionalUnits, 5).'"'; // (60)
        $txt .= $this->addSpaceFront((int) $bill->blCurrentFixedChargesWithSign, 7).'"'; // (61)
        $txt .= $this->addSpaceFront((int) $bill->blCurrentFCAChargesWithSign, 5).'"'; // (62)
        $txt .= $this->addSpace('', 6).'"';
        $txt .= $this->addSpaceFront($previousArrear, 6).'"';
        $txt .= $this->addSpaceFront((int) $currentArrear, 6).'"';
        $txt .= $this->addSpaceFront($others, 4).'"'; // (68)
        $txt .= $this->addSpaceFront($subdryCharge, 9).'"'; // (69)
        $txt .= $this->addSpaceFront(abs($subdryAllowance), 9).'"'; // (70)
        $txt .= $this->addSpaceFront($currentRoundAMount, 2).'"';
        $txt .= $this->addSpaceFront($previousReadindAmount, 2).'"';
        $txt .= $this->addSpaceFront((int) $bill->blNetSop, 7).'"';
        $txt .= $this->addSpaceFront((int) $bill->blNetEd, 7).'"';
        $txt .= $this->addSpaceFront((int) $bill->blNetOctroi, 7).'"';
        $txt .= $this->addSpaceFront((int) $bill->blAmountBeforeDue, 7).'"';
        $txt .= $this->addSpaceFront((int) $bill->blSurCharge, 7).'"';
        $txt .= $this->addSpaceFront((int) $bill->blAmountAfterDue, 8).'"';
        $txt .= $this->addSpace($issueYear, 4).'"';
        $txt .= $this->addSpace(substr($details->sundaryChargeDetail, 0, 19), 19).'"';
        $txt .= $this->addSpace('', 6).'"';
        $txt .= $this->addSpace('', 6).'"';
        $txt .= $this->addSpace(trim($details->complaintCenterPhoneNo), 70).'"';
        $txt .= $this->addSpace(trim($details->nearestCashCounter), 70).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 0 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 6 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 12 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 18 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 24 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 30 ,6), 6).'"';
        $txt .= $this->addSpaceFront('', 7).'"'; // ( 78 )
        $txt .= $this->addSpaceFront('', 11).'"'; // ( 78 )
        $txt .= $this->addSpaceFront('', 10).'"'; // ( 78 )
        $txt .= $this->addSpaceFront($finYear.$finYearPlus, 6).'"'; // ( 78 )
        $txt .= $this->addZeros('', 1).'"'; // ( 78 )
        $txt .= $this->vlen( $bill->blCurrentIDFWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen( $bill->blCurrentCowCessWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen( $bill->blCurrentWaterSewageChargeWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen( $bill->bltotalIDFWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen( $bill->blTotalCowCessWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen( $bill->blTotalCurrentWaterSewarageChargeWithSign, 9).'"'; // ( 78 )
        $txt .= "\r\n";
      }
      return response($txt)
                ->withHeaders([
                    'Content-Type' => 'text/plain',
                    'Cache-Control' => 'no-store, no-cache',
                    'Content-Disposition' => 'attachment; filename="logs.txt',
                ]);
      return $bills;
    }


        public function viewGt()
        {
          return view('viewGt');
        }

    public function exportGt(Request $request)
    {
      // return $request;
      $bills =  Billed::where('blSubDivisionCode', $request->subDivisionCode)
                        ->where('blBillingGroup', $request->billingGroup)
                        ->where('blBillingCycle', $request->billingCycle)->get();
      $txt = '';
      foreach ($bills as $bill) {

        $details = Bill::where('masterkey', $bill->blMasterKey)->first();
        $issueDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blPresentReadingDate)->format('d-M-Y');
        $issueYear = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blPresentReadingDate)->format('Y');
        $cashDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blDueDateByCash)->format('d-M-Y');
        $chequeDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blDueDateByCheque)->format('d-M-Y');
        $presentReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $bill->blPresentReadingDate)->format('d-M-Y');
        $previousReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $details->previousReadingDate)->format('d-M-Y');
        $finYear = Date('Y');
        $finYearPlus = Date('y')+1;

        $tariffType = '';
        if ($bill->blTariffType == 1) {
          $tariffType = "DS";
        } elseif ($bill->blTariffType == 3) {
          $tariffType = "BE";
        } elseif ($bill->blTariffType == 5) {
          $tariffType = "WSD";
        } elseif ($bill->blTariffType == 6) {
          $tariffType = "BPL";
        } elseif ($bill->blTariffType == 0) {
          $tariffType = "BO";
        } elseif ($bill->blTariffType == 2) {
          $tariffType = "NRS";
        } elseif ($bill->blTariffType == 4) {
          $tariffType = "RLY";
        } elseif ($bill->blTariffType == 8) {
          $tariffType = "CGO";
        }

        if ($details->previousArrer > 0) {
          $previousArrear = '+'.(int)$details->previousArrer;
        } elseif ($details->previousArrer == 0) {
          $previousArrear = '';
        } else {
          $previousArrear = $details->previousArrer;
        }
        if ($details->others > 0) {
          $others = '+'.(int)$details->others;
        } elseif ($details->others == 0) {
          $others = '';
        } else {
          $others = $details->others;
        }

        if ($details->currentArrear > 0) {
          $currentArrear = '+'.(int)$details->currentArrear;
        } elseif ($details->currentArrear == 0) {
          $currentArrear = '';
        } else {
          $currentArrear = $details->currentArrear;
        }

        $subdryCharge = ($details->sundaryED) + ($details->sundryIDF) + ($details->sundarySOP) + ($details->sundaryOctroi) + ($details->sundryCowCess);
        if ($subdryCharge > 0) {
          $subdryCharge = '+'.(int)$subdryCharge;
        } elseif ($subdryCharge == 0) {
          $subdryCharge = '';
        } else {
          $subdryCharge = $subdryCharge;
        }
        $subdryAllowance = ($details->allowanceED) + ($details->allowanceIDF) + ($details->allowanceSOP) + ($details->allowanceOctroi) + ($details->allowanceCowCess);
        if ($subdryAllowance > 0) {
          $subdryAllowance = '+'.(int)$subdryAllowance;
        } elseif ($subdryAllowance == 0) {
          $subdryAllowance = '';
        } else {
          $subdryAllowance = $subdryAllowance;
        }

        if ($bill->blCurrentRoundingAmt >= 0) {
          $currentRoundAMount = '+'.$bill->blCurrentRoundingAmt;
        } else {
          $currentRoundAMount = $bill->blCurrentRoundingAmt;
        }

        if ($details->previousRoundingAmount >= 0) {
          $previousReadindAmount = '+'.(int)$details->previousRoundingAmount;
        } else {
          $previousReadindAmount = $details->previousRoundingAmount;
        }
        $blPresentStatusCheck = strtoupper($bill->blPresentStatus);
        if ($blPresentStatusCheck == "O" || $blPresentStatusCheck == "A" || $blPresentStatusCheck == "E" || $blPresentStatusCheck == "T" || $blPresentStatusCheck == "X" || $blPresentStatusCheck == "H" || $blPresentStatusCheck == "W") {
          $blPresentStatus = "OK";
        } elseif ($blPresentStatusCheck == "D" || $blPresentStatusCheck == "M" || $blPresentStatusCheck == "R" || $blPresentStatusCheck == "F" || $blPresentStatusCheck == "G" || $blPresentStatusCheck == "S" || $blPresentStatusCheck == "C" || $blPresentStatusCheck == "L" || $blPresentStatusCheck == "N" || $blPresentStatusCheck == "I") {
          $blPresentStatus = "AVG";
        } else {
          $blPresentStatus = "MMC";
        }

        if ($bill->blDays <= 99) {
          $blDays = $bill->blDays.".0";
        } else {
          $blDays = $bill->blDays;
        }
        // return $dateFormated;
        // $cashDate = Carbon\Carbon::parse($dateFormated);
        // return $consumerAddress->address;
        $txt .= $this->addSpace($bill->blMasterKey, 12).'"';
        $txt .= $this->addSpace($bill->blConsumerName, 20).'"';
        $txt .= $this->addSpace(trim($details->consumerAddress), 20).'"';
        $txt .= $this->addSpace($issueDate, 11).'"';
        $txt .= $this->addSpace($cashDate, 11).'"';
        $txt .= $this->addSpace($chequeDate, 11).'"';
        $txt .= $this->addSpace('', 20).'"'; // ( 17 )
        $txt .= $this->addSpace(trim($details->subDivisionName), 18).'"';
        $txt .= $this->addZeros('', 6).'"'; // ( Bill Number )
        $txt .= $this->addSpace($presentReadingDate, 11).'"';
        $txt .= $this->addSpace($previousReadingDate, 11).'"';
        $txt .= $this->addSpaceFront($blDays, 4).'"';
        $txt .= $this->addSpace($tariffType, 3).'"';
        $txt .= $this->addZeros((double) $details->connectedLoadInKW, 7).'"';
        $txt .= $this->addSpace('', 7).'"';
        $txt .= $this->addSpace(trim($details->meterNo), 9).'"';
        $txt .= $this->addSpace($details->securityDeposit, 7).'"';
        $txt .= $this->addSpace($blPresentStatus, 3).'"';
        $txt .= $this->addZeros($bill->blBillingCycle, 2).'"';
        $txt .= $this->addZeros($bill->blBillingGroup, 1).'"';
        $txt .= $this->addZeros($bill->blPresentReading, 8).'"'; // ( prt-mtr-rdg-curr )
        $txt .= $this->addZeros($details->perviousReading, 8).'"'; // ( prt-mtr-rdg-prev )
        $txt .= $this->addSpace($details->lineCtRatio, 5).'"';
        $txt .= $this->addSpace($details->meterCtRatio, 5).'"';
        $txt .= $this->addZeros(number_format((float)$details->meterMultiplier, 2, '.', ''), 7).'"';
        $txt .= $this->addZeros(number_format((float)$details->overallMultiplingFactor, 2, '.', ''), 7).'"';
        $txt .= $this->addSpace(strtoupper($bill->blPresentStatus), 1).'"'; // ( 48 )
        $newCon = (int) $bill->blUnitsConsumed - (int) $bill->blUnitsConsumedOld;
        $txt .= $this->addSpaceFront($newCon, 6).'"'; // new
        $txt .= $this->addZeros((int) $bill->blUnitsConsumedOld, 6).'"';
        $txt .= $this->addSpaceFront((int) $bill->blUnitsConsumed, 6).'"';
        $txt .= $this->addZeros((int) $bill->blCurrentSop, 8).'"';
        $txt .= $this->addZeros((int) $bill->blCurrentEd, 7).'"';
        $txt .= $this->addZeros((int) $bill->blCurrentOctroi, 6).'"';
        $txt .= $this->addZeros((int) $bill->blCurrentMeterRent, 4).'"';
        $txt .= $this->addZeros((int) $bill->blCurrentServiceRent, 4).'"';
        $txt .= $this->addZeros((int) $details->adjustmentAmount, 7).'"'; // (57)
        $txt .= $this->addSpaceFront(trim($details->periodOfAdjustmentDetail), 8).'"'; // (58)
        $txt .= $this->addSpace(trim($details->reason), 12).'"'; // (59)
        $txt .= $this->addSpaceFront((int) $bill->blConcessionalUnits, 5).'"'; // (60)
        $txt .= $this->addSpaceFront((int) $bill->blCurrentFixedChargesWithSign, 7).'"'; // (61)
        $txt .= $this->addSpaceFront((int) $bill->blCurrentFCAChargesWithSign, 5).'"'; // (62)
        $txt .= $this->addSpace('', 6).'"';
        $txt .= $this->addSpaceFront($previousArrear, 6).'"';
        $txt .= $this->addSpaceFront((int) $currentArrear, 7).'"';
        $txt .= $this->addSpaceFront($others, 4).'"'; // (68)
        $txt .= $this->vlen($subdryCharge, 9).'"'; // (69)
        $txt .= $this->addSpaceFront($subdryAllowance, 9).'"'; // (70)
        $txt .= $this->addSpaceFront($currentRoundAMount, 2).'"';
        $txt .= $this->addSpaceFront($previousReadindAmount, 2).'"';
        $txt .= $this->vlen($bill->blNetSop, 8).'"';
        $txt .= $this->vlen($bill->blNetEd, 8).'"';
        $txt .= $this->vlen($bill->blNetOctroi, 8).'"';
        $txt .= $this->vlen($bill->blAmountBeforeDue, 8).'"';
        $txt .= $this->addZeros((int) $bill->blSurCharge, 7).'"';
        $txt .= $this->vlen($bill->blAmountAfterDue, 9).'"';
        $txt .= $this->addSpace($issueYear, 4).'"';
        $txt .= $this->addSpace(substr($details->sundaryChargeDetail, 0, 19), 19).'"';
        $txt .= $this->addSpace('', 6).'"';
        $txt .= $this->addSpace('', 6).'"';
        $txt .= $this->addSpace(trim($details->complaintCenterPhoneNo), 70).'"';
        $txt .= $this->addSpace(trim($details->nearestCashCounter), 70).'"';
        $txt .= $this->addZeros('', 7).'"';
        $txt .= $this->addZerosSign('', 9).'"';
        $txt .= $this->addSpace('', 9).'"';
        $txt .= $this->addZeros('', 7).'"';
        $txt .= $this->addZerosSign('', 9).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 0 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 6 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 12 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 18 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 24 ,6), 6).'"';
        $txt .= $this->addZeros(substr($details->lastSixMonthConsumption, 30 ,6), 6).'"';
        $txt .= $this->addSpaceFront('', 7).'"'; // ( 78 )
        $txt .= $this->addSpaceFront('', 11).'"'; // ( 78 )
        $txt .= $this->addSpaceFront('', 10).'"'; // ( 78 )
        $txt .= $this->addSpaceFront($finYear.$finYearPlus, 6).'"'; // ( 78 )
        $txt .= $this->vlen($bill->blCurrentIDFWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen($bill->blCurrentCowCessWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen($bill->blCurrentWaterSewageChargeWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen($bill->bltotalIDFWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen($bill->blTotalCowCessWithSign, 9).'"'; // ( 78 )
        $txt .= $this->vlen($bill->blTotalCurrentWaterSewarageChargeWithSign, 9).'"'; // ( 78 )

        $txt .= "\r\n";
      }
      return response($txt)
                ->withHeaders([
                    'Content-Type' => 'text/plain',
                    'Cache-Control' => 'no-store, no-cache',
                    'Content-Disposition' => 'attachment; filename="logs.txt',
                ]);
      return $bills;
    }

    public function vle($value = '', $count)
    {
      $countMain = $count;
      $count = $count - 1;
      if ($value > 0) {
        $name = '+'.$this->addZeros($value, $count);
      } elseif ($value == 0) {
        $abs = 0;
        $abs = sprintf("%.2f", $abs);
        $name = '+'.$this->addZeros($abs, $count);
      } else {
        $abs = abs($value);
        $abs = sprintf("%.2f", $abs);
        // $abs = number_format($abs, 2);
        $name = '-'.$this->addZeros($abs, $count);
        // $blNetSop = $abs;
      }
      return $name;
    }
    public function vlen($value = '', $count)
    {
      $countMain = $count;
      $count = $count - 1;
      if ($value > 0) {
        $name = '+'.$this->addZeros((int) $value, $count);
      } elseif ($value == 0) {
        $name = '+'.$this->addZeros('', $count);
      } else {
        $abs = abs((int) $value);
        // $abs = number_format($abs, 2);
        $name = '-'.$this->addZeros($abs, $count);
        // $blNetSop = $abs;
      }
      return $name;
    }

    public function addZerosSign($value='', $length)
    {
      $count = strlen($value);
      if ($value >= 1 || $value == 0 || $value == 0.00) {
        $sign = "+";
      } else {
        $sign = "-";
      }
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=1; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $sign.$zeros.$value;
      return $newValue;
    }


    public function addZerosSignN($value='', $length)
    {
      $count = strlen($value);
      if ($value >= 1 || $value == 0 || $value == 0.00) {
        $sign = "+";
      } else {
        $sign = "-";
      }
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $sign.$zeros.$value;
      return $newValue;
    }

    public function addZeros($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $zeros.$value;
      return $newValue;
    }

    public function addSpace($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= ' ';
      }

      $newValue = $value.$zeros;
      return $newValue;
    }

    public function addSpaceFront($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= ' ';
      }

      $newValue = $zeros.$value;
      return $newValue;
    }
}
