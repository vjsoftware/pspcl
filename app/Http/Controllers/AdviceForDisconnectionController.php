<?php

namespace App\Http\Controllers;

use App\AdviceForDisconnection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdviceForDisconnectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function advice()
    {
        return view('adviceForDisconnection');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = AdviceForDisconnection::create($request->except('_token'));
        return redirect('adviceForDisconnection');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdviceForDisconnection  $adviceForDisconnection
     * @return \Illuminate\Http\Response
     */
    public function show(AdviceForDisconnection $adviceForDisconnection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdviceForDisconnection  $adviceForDisconnection
     * @return \Illuminate\Http\Response
     */
    public function edit(AdviceForDisconnection $adviceForDisconnection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdviceForDisconnection  $adviceForDisconnection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdviceForDisconnection $adviceForDisconnection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdviceForDisconnection  $adviceForDisconnection
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdviceForDisconnection $adviceForDisconnection)
    {
        //
    }
}
