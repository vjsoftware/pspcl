<?php

namespace App\Http\Controllers;

use App\SundryCharge;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SundryChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sundry()
    {
        return view('sundryCharges');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = SundryCharge::create($request->except('_token'));
        return redirect('sundryCharges');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SundryCharge  $sundryCharge
     * @return \Illuminate\Http\Response
     */
    public function show(SundryCharge $sundryCharge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SundryCharge  $sundryCharge
     * @return \Illuminate\Http\Response
     */
    public function edit(SundryCharge $sundryCharge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SundryCharge  $sundryCharge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SundryCharge $sundryCharge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SundryCharge  $sundryCharge
     * @return \Illuminate\Http\Response
     */
    public function destroy(SundryCharge $sundryCharge)
    {
        //
    }
}
