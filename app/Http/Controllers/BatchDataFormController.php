<?php

namespace App\Http\Controllers;

use App\BatchDataForm;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BatchDataFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function batch()
    {
        return view('batchDataForm');
    }

    public function batchData(Request $request)
    {
      $this->validate($request,[
        'accountNo' => 'required',
        'connectionDate' => 'required',
        ]);

        $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
        // return $request->connectionDate;



        // $data = new BatchDataForm();
        $data = BatchDataForm::create($request->except('_token'));
        return redirect('/batchDataForm');
        // $data->accountNo = $request->accountNo;
        // $data->consumerName = $request->consumerName;
        // $data->address = $request->address;
        // $data->feederCode = $request->feederCode;
        // $data->tariffType = $request->tariffType;
        // $data->employeeConsessionCode = $request->employeeConsessionCode;
        // $data->voltClassCode = $request->voltClassCode;
        // $data->connectedLoad = $request->connectedLoad;
        // $data->meterRatio = $request->meterRatio;
        // $data->connectionDate = $connectionDate;
        // $data->lineCtRatio = $request->lineCtRatio;
        // $data->meterMultiplier = $request->meterMultiplier;
        // $data->overallMultiply = $request->overallMultiply;
        // $data->phaseCode = $request->phaseCode;
        // $data->meterSerialNo = $request->meterSerialNo;
        // $data->amps = $request->amps;
        // $data->mfsCode = $request->mfsCode;
        // $data->mcbRent = $request->mcbRent;
        // $data->meterReading = $request->meterReading;
        // $data->ruralUrbenConsumer = $request->ruralUrbenConsumer;
        // $data->meterLocationCode = $request->meterLocationCode;
        // $data->meterSecurity = $request->meterSecurity;
        // $data->acd = $request->acd;
        // $data->nrs = $request->nrs;
        $data->save();

        return redirect('/batchDataForm');
      }

      public function batchDataAjax(Request $request)
      {
        
        $data = BatchDataForm::create($request->except('_token'));

        if ($data) {
          $status = [
            'status' => 'success'
          ];
        } else {
          $status = [
            'status' => 'error'
          ];
        }

        return $data;
      }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BatchDataForm  $batchDataForm
     * @return \Illuminate\Http\Response
     */
    public function show(BatchDataForm $batchDataForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BatchDataForm  $batchDataForm
     * @return \Illuminate\Http\Response
     */
    public function edit(BatchDataForm $batchDataForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BatchDataForm  $batchDataForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BatchDataForm $batchDataForm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BatchDataForm  $batchDataForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(BatchDataForm $batchDataForm)
    {
        //
    }
}
