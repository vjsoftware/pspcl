<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bind;
use App\User;
use App\Bill;

use Auth;

class BindMeterReaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meterReaders = User::where('role', 2)->get();
        return view('bind')->with('data', $meterReaders);
    }

    public function bindSuccess()
    {
        return view('bindSuccess');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
          'meterReader' => 'required',
          'accountNoG' => 'required',
          'accountNoL' => 'required',
          'subDivisionCode' => 'required|min:3|max:3',
          'billingCycle' => 'required',
          'billingGroup' => 'required',
          'ledgerCode' => 'required|min:3|max:5',
        ]);

        $data  = [];
        $log = new Bind();
        $log->meterReader = $request->meterReader;
        $log->accountNoG = $request->accountNoG;
        $log->accountNoL = $request->accountNoL;
        $log->subDivisionCode = $request->subDivisionCode;
        $log->billingCycle = $request->billingCycle;
        $log->billingGroup = $request->billingGroup;
        $log->ledgerCode = $request->ledgerCode;
        $log->user_id = Auth::user()->id;
        $log->save();

        if ($log) {
          $searchBills = Bill::where('status', 0)
                             ->where('subDivisionCode', $request->subDivisionCode)
                             ->where('billingCycle', '>=', $request->billingCycle)
                             ->where('billingGroup', '>=', $request->billingGroup)
                             ->where('accountNumber', '>=', $request->accountNoG)
                             ->where('accountNumber', '<=', $request->accountNoL)
                             ->update(['meterReader' => $request->meterReader, 'status' => 1]);
        }

        // return $searchBills->success();

        if (count($searchBills) > 0) {
          $data['status'] = 'Bind Meter Reader Success';
          return view('message')->with('data', $data);
        } else {
          $data['status'] = 'no';
          return view('message')->with('data', $data);
        }

        // return $searchBills;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
