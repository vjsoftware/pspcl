<?php

namespace App\Http\Controllers;

use App\sapOutput;
use Illuminate\Http\Request;

class SapOutputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sapOutput  $sapOutput
     * @return \Illuminate\Http\Response
     */
    public function show(sapOutput $sapOutput)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sapOutput  $sapOutput
     * @return \Illuminate\Http\Response
     */
    public function edit(sapOutput $sapOutput)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sapOutput  $sapOutput
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sapOutput $sapOutput)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sapOutput  $sapOutput
     * @return \Illuminate\Http\Response
     */
    public function destroy(sapOutput $sapOutput)
    {
        //
    }
}
