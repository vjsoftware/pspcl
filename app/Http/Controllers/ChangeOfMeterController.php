<?php

namespace App\Http\Controllers;

use App\ChangeOfMeter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangeOfMeterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function meter()
    {
        return view('changeOfMeter');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = ChangeOfMeter::create($request->except('_token'));
      return redirect('changeOfMeter');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChangeOfMeter  $changeOfMeter
     * @return \Illuminate\Http\Response
     */
    public function show(ChangeOfMeter $changeOfMeter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChangeOfMeter  $changeOfMeter
     * @return \Illuminate\Http\Response
     */
    public function edit(ChangeOfMeter $changeOfMeter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChangeOfMeter  $changeOfMeter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChangeOfMeter $changeOfMeter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChangeOfMeter  $changeOfMeter
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChangeOfMeter $changeOfMeter)
    {
        //
    }
}
