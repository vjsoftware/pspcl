<?php

namespace App\Http\Controllers;

use App\ChangeOfAcd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangeOfAcdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function acd()
    {
        return view('changeOfAcd');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = ChangeOfAcd::create($request->except('_token'));
      return redirect('changeOfAcd');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChangeOfAcd  $changeOfAcd
     * @return \Illuminate\Http\Response
     */
    public function show(ChangeOfAcd $changeOfAcd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChangeOfAcd  $changeOfAcd
     * @return \Illuminate\Http\Response
     */
    public function edit(ChangeOfAcd $changeOfAcd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChangeOfAcd  $changeOfAcd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChangeOfAcd $changeOfAcd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChangeOfAcd  $changeOfAcd
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChangeOfAcd $changeOfAcd)
    {
        //
    }
}
