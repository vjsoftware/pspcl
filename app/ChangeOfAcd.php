<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeOfAcd extends Model
{
  protected $fillable = [
    'sysId',
    'inputCode',
    'sheetNo',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'accountNoFrom',
    'accountNoTo',
    'villageName',
    'meterSecurity',
    'oldAcd',
    'additionalAcd',
    'dateOfDeposite',
    'meterLocation',
    'remarks',
  ];
}
