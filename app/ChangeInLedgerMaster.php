<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeInLedgerMaster extends Model
{
  protected $fillable = [
    'sysId',
    'inputCode',
    'sheetNo',
    'numberOfEnties',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'villageName',
    'bGroup',
    'bSub',
    'octroiCode',
    'admCode',
    'remarks',
  ];
}
