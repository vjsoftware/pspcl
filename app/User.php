<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use App\OAuthServerException;
use League\OAuth2\Server\Exception\OAuthServerException;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'simNumber'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
      return $this->belongsToMany(Role::class, 'role_users');
    }

    public function files()
    {
      return $this->belongsToMany(File::class);
    }

    public function hasAccess(array $permissions)
    {
      foreach ($this->roles as $role) {
        if ($role->hasAccess($permissions)) {
          return true;
        }
      }
      return false;
    }

    public function findForPassport($username, $simNumber, $imeiNumber) {
      // return $identifier;
      // echo username;
      $user = $this->where('email', $username)->first();
      if($user != null && $user->simNumber != '') {
        $login = $this->Where('email', $username)->Where('simNumber', $simNumber)->Where('imeiNumber', $imeiNumber)->first();
        if ($login !== null) {
          return $login;
        } else {
          throw new OAuthServerException('Mobile Device Changed', 6, 'mobile_changed', 401);
        }
      } else {
        $login = $this->where('email', $username)->first();
        $login->simNumber = $simNumber;
        $login->imeiNumber = $imeiNumber;
        $login->save();
        if ($login !== null) {
          return $login;
        } else {
          throw new OAuthServerException('Invalid Credientials', 6, 'Login Failed', 401);
        }

      }
        // return $this->Where('email', $username)->Where('simNumber', $simNumber)->Where('imeiNumber', $imeiNumber)->first();
      }
}
