<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchDataForm extends Model
{
  protected $fillable = [
     '_token',
     'sysId',
     'inputCode',
     'trType',
     'subdivisionCode',
     'octraiCode',
     'ledgerGroup',
     'billingGroup',
     'billingSubGroup',
     'villageName',
     'accountNo',
     'consumerName',
     'address',
     'feederCode',
     'tariffType',
     'employeeConsessionCode',
     'voltClassCode',
     'connectedLoad',
     'meterRatio',
     'connectionDate',
     'lineCtRatio',
     'meterMultiplier',
     'overallMultiply',
     'phaseCode',
     'meterSerialNo',
     'amps',
     'mfsCode',
     'mcbRent',
     'meterReading',
     'ruralUrbenConsumer',
     'meterLocationCode',
     'meterSecurity',
     'acd',
     'nrs',
     // add all other fields
 ];
}
