<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdviceForDisconnection extends Model
{
    protected $fillable = [
      'sysId',
      'inputCode',
      'sheetNo',
      'pageNo',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'dateOfAffecting',
      'meterReadingAffecting',
      'connectionStatusCode',
      'remarks',
    ];
}
