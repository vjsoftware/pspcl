<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeOfMeter extends Model
{
  protected $fillable = [
    'sysId',
    'inputCode',
    'sheetNo',
    'numberOfEnties',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'accountNo',
    'checkDigit',
    'meterSerialNo',
    'phaseCode',
    'amps',
    'dateOfInstalling',
    'initialReading',
    'currentReading',
    'dateOfCurrentReading',
    'codeOfMco',
    'meterMultiplier',
    'meterCtRatio',
    'lineCtRatio',
    'overAllMultiplyingFactor',
    'meterMfrsCode',
    'numberOfDigits',
    'unitBilledAgainstOldMeter',
    'remarks',
  ];
}
