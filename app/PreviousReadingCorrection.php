<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreviousReadingCorrection extends Model
{
    protected $fillable = [
      'sysId',
      'inputCode',
      'sheetNo',
      'numberOfEnties',
      'pageNo',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'correctedReading',
      'averageConsumption',
      'remarks',
    ];
}
