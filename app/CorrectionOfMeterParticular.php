  <?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorrectionOfMeterParticular extends Model
{
    protected $fillable = [
      'sysId',
      'inputCode',
      'sheetNo',
      'pageNo',
      'numberOfEnties',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'phaseCode',
      'meterNo',
      'ampere',
      'meterMultipiler',
      'meterCtRatio',
      'lineCtRatio',
      'overAllMultiplyingFactor',
      'additionalMeterRentals',
      'numberOfDigits',
      'mfrsCode',
      'admCode',
      'remarks',
    ];
}
