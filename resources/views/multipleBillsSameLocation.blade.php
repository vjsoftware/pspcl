<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Multiple Bills Same Location - STERLING Transformers</title>

    <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
    <meta property="og:site_name" content="Codebase">
    <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> {{--
    <link rel="stylesheet" href="assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> --}}
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    {{-- <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/select2/select2-bootstrap.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/dropzonejs/min/dropzone.min.css"> --}}
    <!-- Codebase framework -->
    <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>

<body>
    <!-- Page Container -->
    <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
    <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
        <!-- Side Overlay-->

        <!-- END Side Overlay -->

        <!-- Sidebar -->
        <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
        @include('includes/headerAndSidebar')

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <!-- Bootstrap Design -->
                <h2 class="content-heading">Multiple Bills Same Location</h2>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default Elements -->
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Search Form</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option">
                                        <i class="si si-wrench"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <form class="" action="{{ route('miniLedger') }}" method="post">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="divisionCode">Division Code <span class="text-danger">*</span> </label>
                                            <select class="form-control" name="divisionCode" id="divisionCode" required>
                                                  	<option value="">Select</option>
                                                  	<option value="B1">B1</option>
                                                  	<option value="B4">B4</option>
                                                  	<option value="C1">C1</option>
                                                  	<option value="C3">C3</option>
                                                  	<option value="E1">E1</option>
                                                  	<option value="E2">E2</option>
                                                  	<option value="E3">E3</option>
                                                  	<option value="E4">E4</option>
                                                  	<option value="G2">G2</option>
                                                  	<option value="G5">G5</option>
                                                  	<option value="G6">G6</option>
                                                  	<option value="H2">H2</option>
                                                  	<option value="H3">H3</option>
                                                  	<option value="J6">J6</option>
                                                  	<option value="K3">K3</option>
                                                  	<option value="M2">M2</option>
                                                  	<option value="M6">M6</option>
                                                  	<option value="O1">O1</option>
                                                  	<option value="R1">R1</option>
                                                  	<option value="R4">R4</option>
                                                  	<option value="R6">R6</option>
                                                  	<option value="S1">S1</option>
                                                  	<option value="S4">S4</option>
                                                  	<option value="S5">S5</option>
                                                  	<option value="S7">S7</option>
                                                  	<option value="W3">W3</option>
                                                  	<option value="W4">W4</option>
                                                  	<option value="W5">W5</option>
                                                  	<option value="X1">X1</option>
                                                  	<option value="X2">X2</option>
                                                  	<option value="X3">X3</option>
                                                  	<option value="X4">X4</option>
                                                  	<option value="X5">X5</option>
                                                  	<option value="Y3">Y3</option>
                                                  	<option value="Y4">Y4</option>
                                                  	<option value="Y5">Y5</option>
                                                  	<option value="Z2">Z2</option>
                                                  	<option value="Z3">Z3</option>
                                                  	<option value="Z7">Z7</option>
                                                  </select>

                                        {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('divisionCode'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('divisionCode') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="billingGroup">Billing Group <span class="text-danger">*</span> </label>
                                            <select class="form-control" name="billingGroup" id="billingGroup" required>
                                                <option value="">Select</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="8">8</option>
                                            </select>
                                            {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('billingGroup'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('billingGroup') }}</strong>
                                              </span> @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="billingCycle">Billing Cycle <span class="text-danger">*</span></label>
                                            <select class="form-control" name="billingCycle" id="billingCycle" required>
                                                <option value="">Select</option>
                                                <option value="01">1</option>
                                                <option value="02">2</option>
                                                <option value="03">3</option>
                                                <option value="04">4</option>
                                                <option value="05">5</option>
                                                <option value="06">6</option>
                                                <option value="07">7</option>
                                                <option value="08">8</option>
                                                <option value="09">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('billingCycle'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('billingCycle') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-1">
                                            <label for="accountNoL">. </label>
                                            <button type="submit" class="btn btn-alt-primary">View</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END Default Elements -->
                    </div>

                </div>

                <!-- END Bootstrap Design -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        @include('includes/footer')
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

    <!-- Page JS Plugins -->
    <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    {{-- <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script> --}}
    {{-- <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script> --}}
    {{-- <script src="{{ config('app.url') }}/assets/js/plugins/select2/select2.full.min.js"></script> --}}
    <script src="{{ config('app.url') }}/assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js"></script>
    {{-- <script src="{{ config('app.url') }}/assets/js/plugins/masked-inputs/jquery.maskedinput.min.js"></script> --}}
    {{-- <script src="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script> --}}
    {{-- <script src="{{ config('app.url') }}/assets/js/plugins/dropzonejs/min/dropzone.min.js"></script> --}}

    <!-- Page JS Code -->
    <script src="{{ config('app.url') }}/assets/js/pages/be_forms_plugins.js"></script>
    <script>
        jQuery(function() {
            // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
            Codebase.helpers(['datepicker']);
        });
    </script>
</body>

</html>

{{-- @extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload</div>

                <div class="card-body">
                    @can ('upload-bill') yes
                    <form class="" action="{{ route('uploadStore') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <input type="submit" name="submit" value="Upload">
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
