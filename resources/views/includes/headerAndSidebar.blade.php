
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="content-header content-header-fullrow px-15">
                            <!-- Mini Mode -->
                            <div class="content-header-section sidebar-mini-visible-b">
                                <!-- Logo -->
                                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                                    <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                                </span>
                                <!-- END Logo -->
                            </div>
                            <!-- END Mini Mode -->

                            <!-- Normal Mode -->
                            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                                <!-- Close Sidebar, Visible only on mobile screens -->
                                <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                    <i class="fa fa-times text-danger"></i>
                                </button>
                                <!-- END Close Sidebar -->

                                <!-- Logo -->
                                <div class="content-header-item">
                                    <a class="link-effect font-w700" href="{{ config('app.url') }}">
                                        <i class="si si-fire text-primary"></i>
                                        <span class="text-dual-primary-dark">Sterling Transformers</span>
                                    </a>
                                </div>
                                <!-- END Logo -->
                            </div>
                            <!-- END Normal Mode -->
                        </div>
                        <!-- END Side Header -->

                        <!-- Side User -->
                        <div class="content-side content-side-full content-side-user px-10 align-parent">
                            <!-- Visible only in mini mode -->
                            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                                <img class="img-avatar img-avatar32" src="assets/img/avatars/avatar15.jpg" alt="">
                            </div>
                            <!-- END Visible only in mini mode -->

                            <!-- Visible only in normal mode -->
                            <div class="sidebar-mini-hidden-b text-center">
                                <a class="img-link" href="/profile">
                                    <img class="img-avatar" src="assets/img/avatars/avatar15.jpg" alt="">
                                </a>
                                <ul class="list-inline mt-10">
                                    <li class="list-inline-item">
                                        <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="be_pages_generic_profile.html">{{ Auth::user()->name }}</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                                        <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="link-effect text-dual-primary-dark" href onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                                        </form>
                                            <i class="si si-logout"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- END Visible only in normal mode -->
                        </div>
                        <!-- END Side User -->

                        <!-- Side Navigation -->
                        <div class="content-side content-side-full">
                            <ul class="nav-main">
                                <li>
                                    <a class="active" href="{{ config('app.url') }}"><i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li>
                                {{-- <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Bill</span></li> --}}
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Users</span></a>
                                    <ul>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/meter-readers"><span class="sidebar-mini-hide">Meter Reader's List</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/sbaregister"><span class="sidebar-mini-hide">Register Meter Reader</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/register"><span class="sidebar-mini-hide">Register User</span></a>
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Bill</span></a>
                                    <ul>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/upload"><span class="sidebar-mini-hide">Import Bill</span></a>
                                      </li>
                                      {{-- <li>
                                          <a class="" href="{{ config('app.url') }}/bill"><span class="sidebar-mini-hide">Demo Bill</span></a>
                                      </li> --}}
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/bind"><span class="sidebar-mini-hide">Bind Meter Reader</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/view-billing-data"><span class="sidebar-mini-hide">View Billing Data</span></a>
                                      </li>

                                      <li>
                                          <a class="" href="{{ config('app.url') }}/exportOutputFileSpotBill"><span class="sidebar-mini-hide">Billing Data Export NIELIT</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/exportOutputFile"><span class="sidebar-mini-hide">Cash Collection Export</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/exportOutputFileds"><span class="sidebar-mini-hide">Export DS Format</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/gtexport"><span class="sidebar-mini-hide">Export GT Format</span></a>
                                      </li>
                                      <li>
                                          <a href="{{ config('app.url') }}/pending-bills"><span class="sidebar-mini-hide">View Pending Bills</span></a>
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Reports</span></a>
                                    <ul>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/track-meter-reader"><span class="sidebar-mini-hide">Track Meter Reader</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/mini-ledger"><span class="sidebar-mini-hide">Mini Ledger</span></a>
                                      </li>
                                      <li>

                                          <a class="" href="{{ config('app.url') }}/exceptionalReports"><span class="sidebar-mini-hide">Exceptional Reports</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/summaryReports"><span class="sidebar-mini-hide">Summary Reports</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/categoryWiseReports"><span class="sidebar-mini-hide">Category wise Reports</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/divisionWisePerfomanceReports"><span class="sidebar-mini-hide">Division wise Perfomance</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/multipleBillsSameLocation"><span class="sidebar-mini-hide">Multiple Bills</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/viewArrears"><span class="sidebar-mini-hide">View Arrears</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/meter-reader-performance"><span class="sidebar-mini-hide">Meter Reader Performance</span></a>
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Data Entry</span></a>
                                    <ul>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/batchDataForm"><span class="sidebar-mini-hide">Batch Data Form</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfTariff"><span class="sidebar-mini-hide">Change Of Tariff - 71</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/previousReadingCorrection"><span class="sidebar-mini-hide">Previous Reading Correction-72</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/correctionOfMeterParticulars"><span class="sidebar-mini-hide">Correction Of Meter Particulars-73</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/adviceForDisconnection"><span class="sidebar-mini-hide">Advice For Disconnection-74</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/correctionOfName"><span class="sidebar-mini-hide">Correctin of Name-75</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/sundryCharges"><span class="sidebar-mini-hide">Sundry Charges</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfMeter"><span class="sidebar-mini-hide">Change of Meter-79</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/sundryAllowance"><span class="sidebar-mini-hide">Sundry Allowance-86</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeInLedgerMaster"><span class="sidebar-mini-hide">Change in Ledger Master-87</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfAcd"><span class="sidebar-mini-hide">Change of ACD-80</span></a>
                                      </li>
                                    </ul>
                                </li>
                                {{-- <li>
                                    <a class="" href="{{ config('app.url') }}/upload"><i class="si si-layers"></i><span class="sidebar-mini-hide">Import Bill</span></a>
                                </li>
                                <li>
                                    <a class="" href="{{ config('app.url') }}/bill"><i class="si si-layers"></i><span class="sidebar-mini-hide">Demo Bill</span></a>
                                </li> --}}
                            </ul>
                        </div>
                        <!-- END Side Navigation -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div class="content-header-section">
                        <!-- Toggle Sidebar -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-navicon"></i>
                        </button>
                        <!-- END Toggle Sidebar -->

                        <!-- Open Search Section -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="header_search_on">
                            {{-- <i class="fa fa-search"></i> --}}
                        </button>
                        <!-- END Open Search Section -->

                        <!-- Layout Options (used just for demonstration) -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->

                        <!-- END Layout Options -->

                        <!-- Color Themes (used just for demonstration) -->
                        <!-- Themes functionality initialized in Codebase() -> uiHandleTheme() -->

                        <!-- END Color Themes -->
                    </div>
                    <!-- END Left Section -->

                    <!-- Right Section -->
                    <div class="content-header-section">
                        <!-- User Dropdown -->
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}<i class="fa fa-angle-down ml-5"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                                <a class="dropdown-item" href="/profile">
                                    <i class="si si-user mr-5"></i> Profile
                                </a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();"class="clearfix">
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                              </form>
                                    <i class="si si-logout mr-5"></i> Sign Out
                                </a>
                            </div>
                        </div>
                        <!-- END User Dropdown -->

                        <!-- Toggle Side Overlay -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        {{-- <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="side_overlay_toggle">
                            <i class="fa fa-tasks"></i>
                        </button> --}}
                        <!-- END Toggle Side Overlay -->
                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->

                <!-- Header Search -->
                <div id="page-header-search" class="overlay-header">
                    <div class="content-header content-header-fullrow">
                        <form action="be_pages_generic_search.html" method="post">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <!-- Close Search Section -->
                                    <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                                    <button type="button" class="btn btn-secondary" data-toggle="layout" data-action="header_search_off">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <!-- END Close Search Section -->
                                </div>
                                <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-secondary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END Header Search -->

                <!-- Header Loader -->
                <!-- Please check out the Activity page under Elements category to see examples of showing/hiding it -->
                <div id="page-header-loader" class="overlay-header bg-primary">
                    <div class="content-header content-header-fullrow text-center">
                        <div class="content-header-item">
                            <i class="fa fa-sun-o fa-spin text-white"></i>
                        </div>
                    </div>
                </div>
                <!-- END Header Loader -->
            </header>
            <!-- END Header -->
