@php
  use App\User;
  use App\Bill;
@endphp
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Mini Ledger Data | STERLING Transformers</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            @include('includes/headerAndSidebar')
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Mini Ledger Data</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        {{-- <div class="block-header block-header-default">
                            <h3 class="block-title">Dynamic Table <small></small></h3>
                        </div> --}}
                        <div class="block-content block-content-full">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                            <table class="table-bordered table-responsive table-striped table-vcenter js-dataTable-full">
                                <thead>
                                    <tr>
                                        <th>Master key</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Mobile No</th>
                                        <th>Email ID</th>
                                        <th>Meter No</th>
                                        <th>Prevoius Code</th>
                                        <th>Previous Reading</th>
                                        <th>Present Reading</th>
                                        <th>Present Status</th>
                                        <th>Present Reading Date</th>
                                        <th>Units Consumed</th>
                                        <th>Days</th>
                                        <th>Current SOP</th>
                                        <th>Current ED</th>
                                        <th>Current Octroi</th>
                                        <th>Current IDF</th>
                                        <th>Current Cow Cess</th>
                                        <th>Current Water Sewarage Charges</th>
                                        <th>Net SOP</th>
                                        <th>Net ED</th>
                                        <th>Net Octroi</th>
                                        <th>Current Arrears</th>
                                        <th>Due Date Cash</th>
                                        <th>Due Date Cheque</th>
                                        <th>Amount After Due</th>
                                        <th>Surcharge</th>
                                        <th>Amount Before Due</th>
                                        <th>Meter Reader ID</th>
                                        <th>Meter Reader</th>
                                        <th>BillDate</th>
                                        <th>Total IDF</th>
                                        <th>Total Cow Cess</th>
                                        <th>Total Water Sewarage Charges</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if (isset($data))
                                    @foreach ($data as $file)
                                      <tr>
                                          <td class="">{{ $file->blMasterKey }}</td>
                                          <td class="font-w600">{{ $file->blConsumerName }}</td>
                                          @php
                                            $address = Bill::where('masterKey', $file->blMasterKey)->get();
                                            // var_dump($address);
                                          @endphp
                                          <td class="font-w600">{{ $address[0]['consumerAddress'] }}</td>
                                          <td class="font-w600">{{ $file->blMobile }}</td>
                                          <td class="font-w600">{{ $file->blEmail }}</td>
                                          <td class="font-w600">{{ $address[0]['meterNo'] }}</td>
                                          <td class="font-w600">{{ $address[0]['perviousCode'] }}</td>
                                          <td class="font-w600">{{ $address[0]['perviousReading'] }}</td>
                                          <td class="font-w600">{{ $file->blPresentReading }} </td>
                                          <td class="font-w600">{{ strtoupper($file->blPresentStatus) }} </td>
                                          <td class="font-w600">{{ $file->blPresentReadingDate }} </td>
                                          <td class="font-w600">{{ $file->blUnitsConsumed }} </td>
                                          <td class="font-w600">{{ $file->blDays }} </td>
                                          <td class="font-w600">{{ $file->blCurrentSop }} </td>
                                          <td class="font-w600">{{ $file->blCurrentEd }} </td>
                                          <td class="font-w600">{{ $file->blCurrentOctroi }} </td>
                                          <td class="font-w600">{{ $file->blCurrentIDFWithSign }} </td>
                                          <td class="font-w600">{{ $file->blCurrentCowCessWithSign }} </td>
                                          <td class="font-w600">{{ $file->blCurrentWaterSewageChargeWithSign }} </td>
                                          <td class="font-w600">{{ $file->blNetSop }} </td>
                                          <td class="font-w600">{{ $file->blNetEd }} </td>
                                          <td class="font-w600">{{ $file->blNetOctroi }} </td>
                                          <td class="font-w600">Vj</td>
                                          <td class="font-w600">{{ $file->blDueDateByCash}} </td>
                                          <td class="font-w600">{{ $file->blDueDateByCheque}} </td>
                                          <td class="font-w600">{{ $file->blAmountAfterDue}} </td>
                                          <td class="font-w600">{{ $file->blSurCharge}} </td>
                                          <td class="font-w600">{{ $file->blAmountBeforeDue}} </td>
                                          <td class="font-w600">{{ $file->blMeterReader }}</td>
                                          @php
                                            $meterReader = User::find($file->blMeterReader);
                                          @endphp
                                          <td class="font-w600">{{ $meterReader->name }}</td>
                                          <td class="font-w600">{{ date('d-m-Y', strtotime($file->created_at)) }}</td>
                                          <td class="font-w600">{{ $file->bltotalIDFWithSign}} </td>
                                          <td class="font-w600">{{ $file->blTotalCowCessWithSign}} </td>
                                          <td class="font-w600">{{ $file->blTotalCurrentWaterSewarageChargeWithSign}} </td>
                                      </tr>
                                    @endforeach
                                  @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_tables_datatables.js"></script>
    </body>
</html>
