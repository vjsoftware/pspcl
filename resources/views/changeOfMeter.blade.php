<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Change of Meter/CT/PT Units for D.S/N.R.S Consumers - STERLING Transformers</title>

    <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
    <meta property="og:site_name" content="Codebase">
    <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Codebase framework -->
    <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}/assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>

<body>
    <!-- Page Container -->
    <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
    <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
        <!-- Side Overlay-->

        <!-- END Side Overlay -->

        <!-- Sidebar -->
        <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
        @include('includes/headerAndSidebar')

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <!-- Bootstrap Design -->
                <h2 class="content-heading">Change of Meter/CT/PT Units for D.S/N.R.S Consumerss</h2>

                <div class="row">
                    <div class="col-md-12">
                        <!-- Default Elements -->
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Form</h3>
                            </div>
                            <div class="block-content">
                                <form class="" action="{{ route('changeOfMeter') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                  <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="subdivisionCode">Sub Division Code <span class="text-danger">*</span> </label>
                                        <select class="form-control" name="subdivisionCode" id="subdivisionCode" required>
                                                <option value="">Select</option>
                                                <option value="H11">H11</option>
                                                <option value="H12">H12</option>
                                                <option value="H13">H13</option>
                                                <option value="H14">H14</option>
                                                <option value="H15">H15</option>
                                                <option value="H21">H21</option>
                                                <option value="H22">H22</option>
                                                <option value="H23">H23</option>
                                                <option value="H24">H24</option>
                                                <option value="H25">H25</option>
                                                <option value="H26">H26</option>
                                                <option value="H27">H27</option>
                                                <option value="H31">H31</option>
                                                <option value="H32">H32</option>
                                                <option value="H33">H33</option>
                                                <option value="J12">H12</option>
                                                <option value="H34">H34</option>
                                                <option value="H35">H35</option>
                                                <option value="H36">H36</option>
                                                <option value="H39">H39</option>
                                                <option value="H41">H41</option>
                                                <option value="H42">H42</option>
                                                <option value="H43">H43</option>
                                                <option value="H44">H44</option>
                                                <option value="J11">J11</option>
                                                <option value="J13">J13</option>
                                                <option value="J14">J14</option>
                                                <option value="J15">J15</option>
                                                <option value="J16">J16</option>
                                                <option value="J17">J17</option>
                                                <option value="J21">J21</option>
                                                <option value="J22">J22</option>
                                                <option value="J23">J23</option>
                                                <option value="J41">J41</option>
                                                <option value="J42">J42</option>
                                                <option value="J43">J43</option>
                                                <option value="J44">J44</option>
                                                <option value="J45">J45</option>
                                                <option value="J46">J46</option>
                                                <option value="J51">J51</option>
                                                <option value="J52">J52</option>
                                                <option value="J53">J53</option>
                                                <option value="J54">J54</option>
                                                <option value="J55">H12</option>
                                                <option value="J56">J56</option>
                                                <option value="N21">N21</option>
                                                <option value="N22">N22</option>
                                                <option value="N23">N23</option>
                                                <option value="N24">N24</option>
                                                <option value="N25">N25</option>
                                                <option value="N26">N26</option>
                                                <option value="N31">N31</option>
                                                <option value="N32">N32</option>
                                                <option value="N33">N33</option>
                                                <option value="N34">N34</option>
                                                <option value="N35">N35</option>
                                                <option value="N36">N36</option>
                                                <option value="N41">N41</option>
                                                <option value="N42">N42</option>
                                                <option value="N43">N43</option>
                                                <option value="N44">N44</option>
                                                <option value="N45">N45</option>
                                                <option value="X21">X21</option>
                                                <option value="X22">X22</option>
                                                <option value="X23">X23</option>
                                                <option value="X24">X24</option>
                                                <option value="X25">X25</option>
                                                <option value="X26">X26</option>
                                                <option value="X31">X31</option>
                                                <option value="X32">X32</option>
                                                <option value="X33">X33</option>
                                                <option value="X34">X34</option>
                                                <option value="X35">X35</option>
                                                <option value="X41">X41</option>
                                                <option value="X42">X42</option>
                                                <option value="X43">X43</option>
                                                <option value="X44">X44</option>
                                                <option value="X45">X45</option>
                                                <option value="X51">X51</option>
                                                <option value="X52">X52</option>
                                                <option value="X53">X53</option>
                                                <option value="X54">X54</option>
                                                <option value="X55">X55</option>
                                                <option value="X56">X56</option>
                                                <option value="X11">X11</option>
                                                <option value="X12">X12</option>
                                                <option value="X13">X13</option>
                                                <option value="X14">X14</option>
                                                <option value="X15">X15</option>
                                                <option value="X16">X16</option>
                                              </select>
                                    {{--
                                        <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('subdivisionCode'))
                                        <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('subdivisionCode') }}</strong>
                                          </span> @endif
                                    </div>                                   
                                    <div class="col-md-2">
                                        <label for="billingCycle">Billing Cycle <span class="text-danger">*</span> </label>
                                        <select class="form-control" name="billingCycle" id="billingCycle" required>
                                          <option value="">Select</option>
                                          <option value="01">1</option>
                                          <option value="02">2</option>
                                          <option value="03">3</option>
                                          <option value="04">4</option>
                                          <option value="05">5</option>
                                          <option value="06">6</option>
                                          <option value="07">7</option>
                                          <option value="08">8</option>
                                          <option value="09">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                        </select>
                                    {{--
                                        <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('billingCycle'))
                                        <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('billingCycle') }}</strong>
                                          </span> @endif
                                    </div>
                                    <div class="col-md-2">
                                        <label for="billingGroup">Billing Group <span class="text-danger">*</span> </label>
                                        <select class="form-control" name="billingGroup" id="billingGroup" required>
                                          <option value="">Select</option>
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>
                                          <option value="8">8</option>
                                        </select>
                                    {{--
                                        <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('billingGroup'))
                                        <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('billingGroup') }}</strong>
                                          </span> @endif
                                    </div>
                                    <div class="col-md-2">
                                        <label for="sysId">System ID<span class="text-danger">*</span> </label>
                                          <input type="text" class="form-control" id="sysId" name="sysId" placeholder="System ID" value="{{ old('sysId') }}">
                                    {{--
                                        <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('sysId'))
                                        <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('sysId') }}</strong>
                                          </span> @endif
                                    </div>
                                    <div class="col-md-2">
                                        <label for="inputCode">Input Code<span class="text-danger">*</span> </label>
                                        <input type="text" class="form-control" id="inputCode" name="inputCode" placeholder="Input Code" value="{{ old('inputCode') }}">
                                    {{--
                                        <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('inputCode'))
                                        <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('inputCode') }}</strong>
                                          </span> @endif
                                    </div>
                                    <div class="col-md-2">
                                        <label for="sheetNo">Sheet No<span class="text-danger">*</span> </label>
                                        <input type="text" class="form-control" id="sheetNo" name="sheetNo" placeholder="Sheet No" value="{{ old('sheetNo') }}">
                                    {{--
                                        <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('sheetNo'))
                                        <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('sheetNo') }}</strong>
                                          </span> @endif
                                    </div>
                                    <div class="col-md-2">
                                        <label for="numberOfEnties">No.of Entries<span class="text-danger">*</span> </label>
                                        <input type="text" class="form-control" id="numberOfEnties" name="numberOfEnties" placeholder="No.of Entries" value="{{ old('numberOfEnties') }}">
                                    {{--
                                        <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('numberOfEnties'))
                                        <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('numberOfEnties') }}</strong>
                                          </span> @endif
                                    </div>
                                    <div class="col-md-2">
                                        <label for="pageNo">Page NO<span class="text-danger">*</span> </label>
                                        <input type="text" class="form-control" id="pageNo" name="pageNo" placeholder="Page NO" value="{{ old('pageNo') }}">
                                    {{--
                                        <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('pageNo'))
                                        <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('pageNo') }}</strong>
                                          </span> @endif
                                    </div>
                                  </div>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label for="ledgerGroup">Ledger Group<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="ledgerGroup" name="ledgerGroup" placeholder="Ledger Group" value="{{ old('ledgerGroup') }}" required > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('ledgerGroup'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('ledgerGroup') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-2">
                                            <label for="accountNo">Account Number<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="accountNo" name="accountNo" placeholder="Account Number" value="{{ old('accountNo') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('accountNo'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('accountNo') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-2">
                                            <label for="checkDigit">Check Digit<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="checkDigit" name="checkDigit" placeholder="Check Digit" value="{{ old('checkDigit') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('checkDigit'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('checkDigit') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-2">
                                            <label for="meterSerialNo">Meter Serial No<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="meterSerialNo" name="meterSerialNo" placeholder="Meter Serial No" value="{{ old('meterSerialNo') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterSerialNo'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('meterSerialNo') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-2">
                                            <label for="phaseCode">Phase Code<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="phaseCode" name="phaseCode" placeholder="Phase Code" value="{{ old('phaseCode') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('phaseCode'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('phaseCode') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-2">
                                            <label for="amps">AMPS<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="amps" name="amps" placeholder="AMPS" value="{{ old('amps') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('amps'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('amps') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="dateOfInstalling">Data of Installing the Meter<span class="text-danger">*</span> </label>
                                            <input type="dateOfAffecting" class="js-datepicker form-control" id="dateOfInstalling" name="dateOfInstalling" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" autocomplete="off" value="{{ old('dateOfInstalling') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('dateOfInstalling'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('dateOfInstalling') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="initialReading">Initial Reading<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="initialReading" name="initialReading" placeholder="Initial Reading" value="{{ old('initialReading') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('initialReading'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('initialReading') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="currentReading">Current Reading<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="currentReading" name="currentReading" placeholder="Current Reading" value="{{ old('currentReading') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('currentReading'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('currentReading') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="dateOfCurrentReading">Data of Current Reading<span class="text-danger">*</span> </label>
                                            <input type="text" class="js-datepicker form-control" id="dateOfCurrentReading" name="dateOfCurrentReading" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" autocomplete="off" value="{{ old('dateOfCurrentReading') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('dateOfCurrentReading'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('dateOfCurrentReading') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="codeOfMco">Code of MCO</label>
                                            <input type="text" class="form-control" id="codeOfMco" name="codeOfMco" placeholder="Code of MCO" value="{{ old('codeOfMco') }}"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('codeOfMco'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('codeOfMco') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="meterMultiplier">Meter Multiplier<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="meterMultiplier" name="meterMultiplier" placeholder="Meter Multiplier" value="{{ old('meterMultiplier') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterMultiplier'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('meterMultiplier') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="meterCtRatio">Meter Ratio<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="meterCtRatio" name="meterCtRatio" placeholder="Meter Ratio" value="{{ old('meterCtRatio') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterCtRatio'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('meterCtRatio') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="lineCtRatio">Line C.T Ratio<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="lineCtRatio" name="lineCtRatio" placeholder="Line C.T Ratio" value="{{ old('lineCtRatio') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('lineCtRatio'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('lineCtRatio') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="overAllMultiplyingFactor">Overall Multiplying Factor<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="overAllMultiplyingFactor" name="overAllMultiplyingFactor" placeholder="Overall Multiplying Factor" value="{{ old('overAllMultiplyingFactor') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('overAllMultiplyingFactor'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('overAllMultiplyingFactor') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="meterMfrsCode">Meter Mfrs Code<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="meterMfrsCode" name="meterMfrsCode" placeholder="Meter Mfrs Code" value="{{ old('meterMfrsCode') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterMfrsCode'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('meterMfrsCode') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="numberOfDigits">Number of digit of meter<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="numberOfDigits" name="numberOfDigits" placeholder="Number of digit of meter" value="{{ old('numberOfDigits') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('numberOfDigits'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('numberOfDigits') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="unitBilledAgainstOldMeter">Units to be billed Against Old Meter<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="unitBilledAgainstOldMeter" name="unitBilledAgainstOldMeter" placeholder="Number of digit of meter" value="{{ old('numberOfDigits') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('unitBilledAgainstOldMeter'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('unitBilledAgainstOldMeter') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="remarks">Remarks<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="remarks" name="remarks" placeholder="Remarks" value="{{ old('remarks') }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('remarks'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('remarks') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-1">
                                            <label for="">.  </label>
                                            <button type="submit" class="btn btn-alt-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END Default Elements -->
                    </div>

                </div>

                <!-- END Bootstrap Design -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        @include('includes/footer')
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

    <!-- Page JS Plugins -->
    <script src="{{ config('app.url') }}/assets/js/plugins/chartjs/Chart.bundle.min.js"></script>
    <script src="{{config('app.url')}}/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ config('app.url') }}/assets/js/pages/be_pages_dashboard.js"></script>

    <script>
            jQuery(function () {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
            });
        </script>
</body>

</html>

{{-- @extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload</div>

                <div class="card-body">
                    @can ('upload-bill') yes
                    <form class="" action="{{ route('uploadStore') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <input type="submit" name="submit" value="Upload">
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
