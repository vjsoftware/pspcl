<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeOfMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_of_meters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sysId');
            $table->string('inputCode');
            $table->string('sheetNo');
            $table->string('numberOfEnties');
            $table->string('pageNo');
            $table->string('billingCycle');
            $table->string('billingGroup');
            $table->string('subdivisionCode');
            $table->string('ledgerGroup');
            $table->string('accountNo');
            $table->string('checkDigit');
            $table->string('meterSerialNo');
            $table->string('phaseCode');
            $table->string('amps');
            $table->string('dateOfInstalling');
            $table->string('initialReading');
            $table->string('currentReading');
            $table->string('dateOfCurrentReading');
            $table->string('codeOfMco');
            $table->string('meterMultiplier');
            $table->string('meterCtRatio');
            $table->string('lineCtRatio');
            $table->string('overAllMultiplyingFactor');
            $table->string('meterMfrsCode');
            $table->string('numberOfDigits');
            $table->string('unitBilledAgainstOldMeter');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_of_meters');
    }
}
