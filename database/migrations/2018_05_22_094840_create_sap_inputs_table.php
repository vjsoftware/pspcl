<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSapInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sap_inputs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('subDivisionCode', 4);
            $table->string('subDivisionName', 20);
            $table->string('MRU', 8);
            $table->string('MR_DOCNumber', 20);
            $table->string('neighbourMeterId', 30);
            $table->string('connectedPoleNINNumber', 21);
            $table->string('consumerLegacyNumber', 12);
            $table->string('contractAcNumber', 12);
            $table->string('consumerName', 30);
            $table->string('houseNumber', 10);
            $table->string('streetNumber', 30);
            $table->string('address', 100);
            $table->string('category', 5);
            $table->string('subCategory', 8);
            $table->string('moveInFlag', 1);
            $table->string('categoryChangeFlag', 1);
            $table->string('categoryChangeDate', 10);
            $table->string('oldCategory', 5);
            $table->string('oldSubCategory', 8);
            $table->integer('industryCode');
            $table->integer('billingCycle');
            $table->integer('billingCycleType');
            $table->string('billingGroup', 2);
            $table->string('triVectorMeterFlag', 1);
            $table->string('installation', 12);
            $table->string('meterMnfSerialNumber', 30);
            $table->string('meterManufacturerName', 30);
            $table->string('meterSAPSerialNumber', 18);
            $table->double('multiplicationFactor', 6, 2);
            $table->double('overallMf', 6, 2);
            $table->double('oldMf', 6, 2);
            $table->integer('numberOfDigits');
            $table->string('lineCtRatio', 15);
            $table->string('externalPtRatio', 15);
            $table->integer('phaseCode');
            $table->double('sanctionedLoadKW', 6, 2);
            $table->double('contractedDemandKVA', 6, 2);
            $table->string('sanctionedLoadChangeFlag', 1);
            $table->string('sanctionedLoadChangeDate', 10);
            $table->double('oldSanctionedLoadKW', 6, 2);
            $table->string('admissibleVoltage', 2);
            $table->string('supplyVoltage', 2);
            $table->integer('noOfBOardEmployees6');
            $table->integer('noOfBOardEmployees7');
            $table->integer('noOfBOardEmployees8');
            $table->string('scheduleMeterReadDate', 10);
            $table->string('previousReadingDate', 10);
            $table->date('previousReadingDateFormated');
            $table->string('previousBillDate', 10);
            $table->date('previousBillDateFormated');
            $table->double('previousReadingKWH', 12, 3);
            $table->double('previousReadingKVA', 12, 3);
            $table->double('previousReadingKVAH', 12, 3);
            $table->string('previousMeterStatus', 2);
            $table->integer('previousBillType');
            $table->string('meterChangeFlag', 1);
            $table->string('meterChangeDate', 10);
            $table->date('meterChangeDateFormated');
            $table->double('totalOldMeterConsumptionUnitsKWH', 12, 3);
            $table->double('totalOldMeterConsumptionUnitsKVAH', 12, 3);
            $table->double('oldMeterTotalRentAmount', 10, 2);
            $table->double('oldMeterTotalServiceRentAmount', 10, 2);
            $table->double('oldMeterTotalServiceChargeAmount', 10, 2);
            $table->double('oldMeterTotalMBCRentAmount', 10, 2);
            $table->double('newMeterInitialReadingKWH', 12, 3)->nullable();
            $table->double('newMeterInitialReadingKVAH', 12, 3)->nullable();
            $table->string('reconnectionFlag', 1)->nullable();
            $table->string('reconnectionDate', 10)->nullable();
            $table->double('consumptionBeforeDisConnectionKWH', 12, 3)->nullable();
            $table->double('consumptionBeforeDisConnectionKVAH', 12, 3)->nullable();
            $table->integer('periodBeforeDisConnection')->nullable();
            $table->string('edExemptedFlag', 1);
            $table->string('edExemptionExpiry', 10);
            $table->date('edExemptionExpiryFormated');
            $table->string('octraiChargesApplicableFlag', 1);
            $table->double('meterRentRate', 10, 2);
            $table->double('serviceRentRate', 10, 2);
            $table->double('peakLoadExemptionCharges', 10, 2);
            $table->string('peakLoadExemptionChargesExpiry', 10);
            $table->date('peakLoadExemptionChargesExpiryFormated');
            $table->double('MMTSCorrectionFactor', 6, 2);
            $table->string('shuntCapacitorChargesApplicableFlag', 1);
            $table->double('capacityOfCapacitor', 9, 2);
            $table->double('MBCRentRate', 9, 2);
            $table->double('serviceChargeRate', 9, 2);
            $table->string('seasonStartDate', 10);
            $table->date('seasonStartDateFormated');
            $table->string('seasonEndDate', 10);
            $table->date('seasonEndDateFormated');
            $table->double('sundryChargesSOP', 10, 2);
            $table->double('sundryChargesED', 10, 2);
            $table->double('sundryChargesOCTRAI', 10, 2);
            $table->double('sundryAllowancesSOP', 10, 2);
            $table->double('sundryAllowancesED', 10, 2);
            $table->double('sundryAllowancesOCTRAI', 10, 2);
            $table->double('waterChargesProvisionalAmount', 10, 2);
            $table->double('otherCharges', 10, 2);
            $table->double('roundAdjustmentAmount', 10, 2);
            $table->double('adjustmentAmountSOP', 10, 2);
            $table->double('adjustmentAmountED', 10, 2);
            $table->double('adjustmentAmountOCTRAI', 10, 2);
            $table->double('provisionalAdjustmentAmountSOP', 10, 2);
            $table->double('provisionalAdjustmentAmountED', 10, 2);
            $table->double('provisionalAdjustmentAmountOCTRAI', 10, 2);
            $table->double('arrearSOPCurrentYear', 10, 2);
            $table->double('arrearEDCurrentYear', 10, 2);
            $table->double('arrearOCTRAICurrentYear', 10, 2);
            $table->double('arrearSOPPreviousYear', 10, 2);
            $table->double('arrearEDPreviousYear', 10, 2);
            $table->double('arrearOCTRAIPreviousYear', 10, 2);
            $table->double('advanceConsumptionDeposit', 10, 2);
            $table->double('courtCaseAmount', 10, 2);
            $table->double('previousKWHCycle1', 12, 3);
            $table->double('previousKWHCycle2', 12, 3);
            $table->double('previousKWHCycle3', 12, 3);
            $table->double('previousKWHCycle4', 12, 3);
            $table->double('previousKWHCycle5', 12, 3);
            $table->double('previousKWHCycle6', 12, 3);
            $table->double('averageKWHofAbove6Cycles', 12, 3);
            $table->double('samePeriodLastYearConsumptionKWH', 12, 3);
            $table->double('PF1', 6, 2);
            $table->double('PF2', 6, 2);
            $table->double('PF3', 6, 2);
            $table->double('averageofAboveThreePF', 6, 2);
            $table->double('standardPFOfConsumerCategory', 6, 2);
            $table->double('MD1', 12, 3);
            $table->double('MD2', 12, 3);
            $table->double('gaushalaFlag', 12, 3);
            $table->double('arrearWaterChargesCurrentYear', 12, 3);
            $table->double('arrearWaterChargesPreviousYear', 12, 3);
            $table->string('dataExpiryDate', 10);
            $table->date('dataExpiryDateFormated');
            $table->double('maxOfAboveSixMDI', 12, 3);
            $table->double('factorForCustomerCategory', 6, 2);
            $table->integer('DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory');
            $table->integer('HValueInLDHFSupplyHoursPerDayForCustomerCategory');
            $table->double('previousTotalEstimatedConsumptionKWH', 12, 3);
            $table->double('previousTotalEstimatedConsumptionKVAH', 12, 3);
            $table->double('previousEstimatedMDI', 12, 3);
            $table->string('nearestCashCounter', 30);
            $table->string('complaintCenterPhoneNumber', 15);
            $table->string('previousPaymentStatus', 10);
            $table->string('reasonForAdjustment', 30);
            $table->string('miscExpensesDetails', 30);
            $table->double('miscExpenses', 10, 2);
            $table->double('meterSecurityAmount', 10, 2);
            $table->string('waterChargesApplicableFlag', 1);
            $table->string('transformerCode', 5);
            $table->double('oldMeterRent', 10, 2);
            $table->string('expirationDateOfOldMeterRent', 10);
            $table->date('expirationDateOfOldMeterRentFormated');
            $table->double('oldMBCRent', 10, 2);
            $table->string('expirationDateOfOldMBCRent', 10);
            $table->date('expirationDateOfOldMBCRentFormated');
            $table->double('oldServiceCharges', 10, 2);
            $table->string('expirationDateOfOldServiceCharges', 10);
            $table->date('expirationDateOfOldServiceChargesFormated');
            $table->string('meterLocation', 1);
            $table->string('meterType', 1);
            $table->integer('version');
            $table->string('ML', 1);
            $table->string('MT', 1);
            $table->string('mobileNo', 10);
            $table->string('feederCode', 8);
            $table->integer('SCWSDAmountWithHeld');
            $table->integer('previousFyCons');
            $table->string('muncipalTaxApplicableFlag', 2);
            $table->string('PLogicApplicableFlag', 2);
            $table->integer('limitForPNore');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sap_inputs');
    }
}
