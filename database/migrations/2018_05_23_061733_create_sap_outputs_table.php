<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSapOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sap_outputs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subDivisonCode', 4);
            $table->string('MRU', 8);
            $table->string('connectedPoleNINNumber', 20);
            $table->string('neighbourMeterNo', 18);
            $table->string('streetName', 30);
            $table->string('installation', 12);
            $table->string('MR_DOCNumber', 20);
            $table->string('scheduledMRDate', 8);
            $table->string('SAPMeterNumber', 18);
            $table->integer('manufacturerSRNo');
            $table->string('manufacturerName', 30);
            $table->string('contractAcNumber', 15);
            $table->double('consumptionKWH', 12, 3);
            $table->double('consumptionKVAH', 12, 3);
            $table->double('consumptionKVA', 12, 3);
            $table->double('currentMeterReadingKWH', 12, 3);
            $table->double('currentMeterReadingKVA', 12, 3);
            $table->double('currentMeterReadingKVAH', 12, 3);
            $table->string('currentMeterReadingDate', 10);
            $table->string('currentMeterReadingTime', 6);
            $table->string('currentNoteFromMeterReader', 4);
            $table->double('previousMeterReadingKWH', 12, 3);
            $table->double('previousMeterReadingKVA', 12, 3);
            $table->double('previousMeterReadingKVAH', 12, 3);
            $table->string('previousMeterReadingDate', 8);
            $table->string('previousMeterReadingTime', 6);
            $table->string('previoustNoteFromMeterReader', 1);
            $table->string('octraiFlag', 1);
            $table->double('SOP', 12, 2);
            $table->double('ED', 12, 2);
            $table->double('OCTRAI', 12, 2);
            $table->double('DSSF', 12, 2);
            $table->double('surchargeLevied', 12, 2);
            $table->double('serviceRent', 12, 2);
            $table->double('meterRent', 12, 2);
            $table->double('serviceCharge', 12, 2);
            $table->double('monthlyMinimumCharges', 12, 2);
            $table->double('powerFactorSurcharges', 12, 2);
            $table->double('powerFactorIncentive', 12, 2);
            $table->double('demandCharges', 12, 2);
            $table->double('waterCharges', 12, 2);
            $table->double('voltageCharges', 12, 2);
            $table->double('peakLoadExemptionCharges', 12, 2);
            $table->double('sundryCharges', 12, 2);
            $table->double('miscellaneousCharges', 12, 2);
            $table->double('fuelAdjustment', 12, 2);
            $table->string('billNumber', 17);
            $table->integer('noOfDaysBilledFor');
            $table->integer('billCycle');
            $table->string('billDate', 10);
            $table->string('dueDate', 10);
            $table->string('billType', 1);
            $table->double('paymantAmount', 12, 2);
            $table->string('paymantMode', 10);
            $table->string('checkNo', 20);
            $table->string('bankName', 30);
            $table->string('paymentID', 16);
            $table->string('IFSCcode', 15);
            $table->string('MICRcode', 9);
            $table->string('paymentDate', 10);
            $table->string('paymentRemark', 30);
            $table->double('totBillAmount', 12, 2);
            $table->string('SBMnumber', 8);
            $table->string('meterReaderName', 30);
            $table->string('inHouseOutsourcedSBM', 10);
            $table->string('transformerCode', 5);
            $table->double('mcbRent', 12, 2);
            $table->double('LPSC', 12, 2);
            $table->double('totalAmountAfterDueDate', 12, 2);
            $table->double('totalOfNetSopNetEdNetOctrai', 12, 2);
            $table->string('subDivisionName', 20);
            $table->string('consumerLegacyNumber', 12);
            $table->string('consumerName', 30);
            $table->string('houseNumber', 10);
            $table->string('address', 100);
            $table->string('category', 5);
            $table->string('subCategory', 8);
            $table->integer('billingCycle');
            $table->string('billingGroup', 2);
            $table->integer('phaseCode');
            $table->double('sanctionedLoadKW', 6, 2);
            $table->double('totalOldMeterConsumptionUnitsKWH', 12, 3);
            $table->double('totalOldMeterConsumptionUnitsKVAH', 12, 3);
            $table->double('newMeterInitialReadingKWH', 12, 3);
            $table->double('newMeterInitialReadingKVAH', 12, 3);
            $table->double('sundryChargesSOP', 10, 2);
            $table->double('sundryChargesED', 10, 2);
            $table->double('sundryChargesOCTRAI', 10, 2);
            $table->double('sundryAllowancesSOP', 10, 2);
            $table->double('sundryAllowancesED', 10, 2);
            $table->double('sundryAllowancesOCTRAI', 10, 2);
            $table->double('otherCharges', 10, 2);
            $table->double('roundAdjustmentAmount', 10, 2);
            $table->double('adjustmentAmountSOP', 10, 2);
            $table->double('adjustmentAmountED', 10, 2);
            $table->double('adjustmentAmountOCTRAI', 10, 2);
            $table->double('provisionalAdjustmentAmountSOP', 10, 2);
            $table->double('provisionalAdjustmentAmountED', 10, 2);
            $table->double('provisionalAdjustmentAmountOCTRAI', 10, 2);
            $table->double('arrearSOPCurrentYear', 10, 2);
            $table->double('arrearEDCurrentYear', 10, 2);
            $table->double('arrearOCTRAICurrentYear', 10, 2);
            $table->double('arrearSOPPreviousYear', 10, 2);
            $table->double('arrearEDPreviousYear', 10, 2);
            $table->double('arrearOCTRAIPreviousYear', 10, 2);
            $table->double('advanceConsumptionDeposit', 10, 2);
            $table->string('complaintCenterPhoneNumber', 15);
            $table->double('meterSecurityAmount', 10, 2);
            $table->string('nearestCashCounter', 30);
            $table->double('multiplicationFactor', 6, 2);
            $table->double('overallMf', 6, 2);
            $table->double('contractedDemandKVA', 6, 2);
            $table->string('miscExpensesDetails', 30);
            $table->double('previousKWHCycle1', 12, 3);
            $table->double('previousKWHCycle2', 12, 3);
            $table->double('previousKWHCycle3', 12, 3);
            $table->double('previousKWHCycle4', 12, 3);
            $table->double('previousKWHCycle5', 12, 3);
            $table->double('previousKWHCycle6', 12, 3);
            $table->double('roundingPresent', 10, 2);
            $table->string('estimationType', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sap_outputs');
    }
}
