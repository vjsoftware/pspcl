<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdviceForDisconnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advice_for_disconnections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sysId');
            $table->string('inputCode');
            $table->string('sheetNo');
            $table->string('pageNo');
            $table->string('billingCycle');
            $table->string('billingGroup');
            $table->string('subdivisionCode');
            $table->string('ledgerGroup');
            $table->string('accountNo');
            $table->string('checkDigit');
            $table->date('dateOfAffecting');
            $table->string('meterReadingAffecting');
            $table->string('connectionStatusCode');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advice_for_disconnections');
    }
}
