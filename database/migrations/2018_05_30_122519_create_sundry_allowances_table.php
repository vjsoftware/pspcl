<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSundryAllowancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sundry_allowances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sysId');
            $table->string('inputCode');
            $table->string('sheetNo');
            $table->string('pageNo');
            $table->string('billingCycle');
            $table->string('billingGroup');
            $table->string('subdivisionCode');
            $table->string('ledgerGroup');
            $table->string('accountNo');
            $table->string('checkDigit');
            $table->string('sop');
            $table->string('ed');
            $table->string('oct');
            $table->string('code');
            $table->string('sundryRegister');
            $table->string('sundryItemNo');
            $table->string('controlNo');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sundry_allowances');
    }
}
