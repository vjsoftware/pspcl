<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use FarhanWazir\GoogleMaps\Facades\GMapsFacade;

Route::get('/', 'HomeController@index');

Route::get('soap/{key}/server', [
    'as' => 'zoap.server.wsdl',
    'uses' => 'zoapController@server'
]);

Route::post('soap/{key}/server', [
    'as' => 'zoap.server',
    'uses' => '\Viewflex\Zoap\ZoapController@server'
]);

Route::get('/map', 'HomeController@map');

Route::get('/token', function () {
    return Auth::user()->createToken('user');
});

Route::get('/message', function () {
    return view('message');
});



// Auth
Auth::routes();

Route::post('/employeeRegister', 'Auth\EmployeeRegisterController@register')
        ->name('employeeRegister')
        ->middleware('can:upload-bill');

Route::post('/sbaRegister', 'Auth\EmployeeRegisterController@sbaregister')
        ->name('sbaRegister')
        ->middleware('can:upload-bill');

Route::post('/profile', 'Auth\EmployeeRegisterController@changepassword')
        ->name('changepassword')
        ->middleware('can:upload-bill');

Route::get('/userSuccess', function () {
    return view('userSuccess');
});
Route::get('/sbaSuccess', function () {
    return view('sbaSuccess');
});
Route::get('/passwordSuccess', function () {
    return view('passwordSuccess');
});

Route::get('/profile', function () {
    return view('profile');
});

Route::get('/sbaregister', function () {
    return view('auth/sbaregister');
});

Route::get('/sba-register-edit/{id}', 'Auth\EmployeeRegisterController@edit');

Route::post('/sba-register-edit', 'Auth\EmployeeRegisterController@sbaRegisterUpdate')
      ->name('sbaRegisterUpdate');





Route::get('/home', 'HomeController@index')->name('home');


// Reports
Route::get('/pending-bills', 'ReportsController@pendingBills')
        ->middleware('can:upload-bill');
Route::post('/pending-bills', 'ReportsController@pendingBillsSearch')
        ->name('pendingBills')
        ->middleware('can:upload-bill');

Route::get('/mini-ledger', 'ReportsController@miniLedger')
        ->middleware('can:upload-bill');
Route::post('/mini-ledger', 'ReportsController@miniLedgerSearch')
        ->name('miniLedger')
        ->middleware('can:upload-bill');

Route::get('/track-meter-reader', 'ReportsController@trackMeterReader')
        ->middleware('can:upload-bill');

Route::post('/track-meter-reader', 'ReportsController@trackMeterReaderSearch')
        ->name('trackMeterReaderSearch')
        ->middleware('can:upload-bill');

Route::get('/meter-readers', 'ReportsController@meterReaders')
        ->middleware('can:upload-bill');

Route::get('/meter-reader-performance', 'ReportsController@meterReaderPerformance')
        ->middleware('can:upload-bill');

Route::post('/meter-reader-performance', 'ReportsController@meterReaderPerformanceSearch')
        ->name('meterReaderPerformanceSearch')
        ->middleware('can:upload-bill');

Route::get('/exceptionalReports', 'ReportsController@exceptional');

Route::get('/summaryReports', 'ReportsController@summary');

Route::get('/categoryWiseReports', 'ReportsController@category');

Route::get('/divisionWisePerfomanceReports', 'ReportsController@division');

Route::get('/multipleBillsSameLocation', 'ReportsController@multiple');

Route::get('/viewArrears', 'ReportsController@arrears');


Route::get('/view-billing-data', 'BillingController@viewBillingData')
        ->name('viewBillingData')
        ->middleware('can:upload-bill');
Route::get('/view-billing-data-records/{fileId}', 'BillingController@viewBillingDataRecords')
        ->name('viewBillingDataRecords')
        ->middleware('can:upload-bill');
Route::get('/exportOutputFile', 'BillingController@view')
        ->middleware('can:upload-bill');
Route::post('/export', 'BillingController@exportBillingDataRecords')
        ->name('exportBillingDataRecords')
        ->middleware('can:upload-bill');
Route::get('/exportOutputFileSpotBill', 'BillingController@viewSpotBill')
        ->middleware('can:upload-bill');
Route::post('/exportnew', 'BillingController@exportNewBillingDataRecords')
        ->name('exportOutputFileSpotBill')
        ->middleware('can:upload-bill');
Route::get('/exportOutputFileds', 'BillingController@viewDs')
        ->middleware('can:upload-bill');
Route::post('/exportds', 'BillingController@exportDs')
        ->name('exportDs')
        ->middleware('can:upload-bill');
Route::get('/gtexport', 'BillingController@viewGt')
        ->middleware('can:upload-bill');
Route::post('/exportGt', 'BillingController@exportGt')
        ->name('exportGt')
        ->middleware('can:upload-bill');


// Upload
Route::get('/upload', 'HomeController@upload')
        ->middleware('can:upload-bill');
Route::get('/success', 'HomeController@uploadSuccess')
        ->middleware('can:upload-bill');
Route::post('/upload', 'HomeController@uploadStore')
        ->name('uploadStore')
        ->middleware('can:upload-bill');

Route::post('/uploadSap', 'HomeController@sapDataStore')
        ->name('sapDataStore')
        ->middleware('can:upload-bill');
Route::get('/bind', 'BindMeterReaderController@index')
        ->name('bindView')
        ->middleware('can:upload-bill');
Route::post('/bind', 'BindMeterReaderController@store')
        ->name('bindPost')
        ->middleware('can:upload-bill');
Route::get('/bind/success', 'BindMeterReaderController@bindSuccess')
        ->name('bindSuccessView')
        ->middleware('can:upload-bill');


// Demo Bill
        Route::get('/bill', 'HomeController@bill')
        ->middleware('can:new-bill');
        Route::get('/search/masterkey/{masterkey}/{status}', 'HomeController@searchView')
        ->middleware('can:new-bill');
        Route::get('/search/masterkey/{masterkey}/{status}/{units}', 'HomeController@searchView')
        ->middleware('can:new-bill');
        Route::post('/bill', 'HomeController@billSearch')
        ->name('billSearch')
        ->middleware('can:new-bill');
        Route::post('/units', 'HomeController@units')
        ->name('units')
        ->middleware('can:new-bill');


//Punching
        Route::get('/batchDataForm', 'BatchDataFormController@batch');
        Route::post('/batchDataForm', 'BatchDataFormController@batchData')
              ->name('batchAdd');
        Route::post('/batchDataFormAjax', 'BatchDataFormController@batchDataAjax')
              ->name('batchAjax');

        Route::get('/changeOfTariff', 'ChangeOfTariffController@tariff');
        Route::post('/changeOfTariff', 'ChangeOfTariffController@tariffChange')
              ->name('tariff');
        Route::post('/changeOfTariffAjax', 'ChangeOfTariffController@tariffChangeAjax')
              ->name('tariffAjax');

        Route::get('/previousReadingCorrection','PreviousReadingCorrectionController@previous');
        Route::post('/previousReadingCorrection','PreviousReadingCorrectionController@previousReading')
              ->name('previousReading');
        Route::post('/previousReadingAjax', 'PreviousReadingCorrectionController@previousReadingAjax')
              ->name('previousReadingAjax');

        Route::get('/correctionOfMeterParticulars', 'CorrectionOfMeterParticularController@particular');
        Route::post('/correctionOfMeterParticulars', 'CorrectionOfMeterParticularController@store')
              ->name('correctionOfMeterParticulars');
        Route::post('/correctionOfMeterAjax', 'CorrectionOfMeterParticularController@particularDataAjax')
              ->name('correctionOfAjax');

        Route::get('/adviceForDisconnection', 'AdviceForDisconnectionController@advice');
        Route::post('/adviceForDisconnection', 'AdviceForDisconnectionController@store')
              ->name('adviceForDisconnection');

        Route::get('/correctionOfName', 'CorrectionOfNameController@name');
        Route::post('/correctionOfName', 'CorrectionOfNameController@store')
              ->name('correctionOfName');

        Route::get('/sundryCharges', 'SundryChargeController@sundry');
        Route::post('/sundryCharges', 'SundryChargeController@store')
              ->name('sundryCharges');

        Route::get('/changeOfMeter', 'ChangeOfMeterController@meter');
        Route::post('/changeOfMeter', 'ChangeOfMeterController@store')
              ->name('changeOfMeter');

        Route::get('/sundryAllowance', 'SundryAllowanceController@allowance');
        Route::post('/sundryAllowance', 'SundryAllowanceController@store')
             ->name('sundryAllowance');

        Route::get('/changeInLedgerMaster', 'ChangeInLedgerMasterController@ledger');
        Route::post('/changeInLedgerMaster', 'ChangeInLedgerMasterController@store')
             ->name('changeInLedgerMaster');

        Route::get('/changeOfAcd', 'ChangeOfAcdController@acd');
        Route::post('/changeOfAcd', 'ChangeOfAcdController@store')
            ->name('changeOfAcd');

        Route::get('/decrypt', 'Hashing@decrypt');
