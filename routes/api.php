<?php

// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Bill;
use App\Billed;
use App\sapInput;
use App\sapOutput;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/login', function (Request $request) {
  return $request->user();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api')->get('/data', function (Request $request) {
  // return $request->user();
  $userDetails = $request->user();
  // return $userDetails->id;
  $bills = Bill::where('meterReader', $userDetails->id)->get();
                // ->where('status', 1)->get();
  $billsUpdate = Bill::where('meterReader', $userDetails->id)->update(['status'=> 2]);
  return $bills;
    // return $request->user();
});

Route::middleware('auth:api')->get('/sapdata', function (Request $request) {
  $userDetails = $request->user();
  // return $userDetails->id;
  // $bills = Bill::where('meterReader', $userDetails->id)->get();
  $bills = sapInput::all();
                // ->where('status', 1)->get();
  // $billsUpdate = Bill::where('meterReader', $userDetails->id)->update(['status'=> 2]);
  return $bills;
});

Route::middleware('auth:api')->post('/sap-data', function (Request $request) {
  // return $request;
  $sapOutput = new sapOutput();
  $sapOutput->subDivisonCode = $request->subDivisonCode;
  $sapOutput->MRU = $request->MRU;
  $sapOutput->connectedPoleNINNumber = $request->connectedPoleNINNumber;
  $sapOutput->neighbourMeterNo = $request->neighbourMeterNo;
  $sapOutput->streetName = $request->streetName;
  $sapOutput->installation = $request->installation;
  $sapOutput->MR_DOCNumber = $request->MR_DOCNumber;
  $sapOutput->scheduledMRDate = $request->scheduledMRDate;
  $sapOutput->SAPMeterNumber = $request->SAPMeterNumber;
  $sapOutput->manufacturerSRNo = $request->manufacturerSRNo;
  $sapOutput->manufacturerName = $request->manufacturerName;
  $sapOutput->contractAcNumber = $request->contractAcNumber;
  $sapOutput->consumptionKWH = $request->consumptionKWH;
  $sapOutput->consumptionKVAH = $request->consumptionKVAH;
  $sapOutput->consumptionKVA = $request->consumptionKVA;
  $sapOutput->currentMeterReadingKWH = $request->currentMeterReadingKWH;
  $sapOutput->currentMeterReadingKVA = $request->currentMeterReadingKVA;
  $sapOutput->currentMeterReadingKVAH = $request->currentMeterReadingKVAH;
  $sapOutput->currentMeterReadingDate = $request->currentMeterReadingDate;
  $sapOutput->currentMeterReadingTime = $request->currentMeterReadingTime;
  $sapOutput->currentNoteFromMeterReader = $request->currentNoteFromMeterReader;
  $sapOutput->previousMeterReadingKWH = $request->previousMeterReadingKWH;
  $sapOutput->previousMeterReadingKVA = $request->previousMeterReadingKVA;
  $sapOutput->previousMeterReadingKVAH = $request->previousMeterReadingKVAH;
  $sapOutput->previousMeterReadingDate = $request->previousMeterReadingDate;
  $sapOutput->previousMeterReadingTime = $request->previousMeterReadingTime;
  $sapOutput->previoustNoteFromMeterReader = $request->previoustNoteFromMeterReader;
  $sapOutput->octraiFlag = $request->octraiFlag;
  $sapOutput->SOP = $request->SOP;
  $sapOutput->ED = $request->ED;
  $sapOutput->OCTRAI = $request->OCTRAI;
  $sapOutput->DSSF = $request->DSSF;
  $sapOutput->surchargeLevied = $request->surchargeLevied;
  $sapOutput->serviceRent = $request->serviceRent;
  $sapOutput->meterRent = $request->meterRent;
  $sapOutput->serviceCharge = $request->serviceCharge;
  $sapOutput->monthlyMinimumCharges = $request->monthlyMinimumCharges;
  $sapOutput->powerFactorSurcharges = $request->powerFactorSurcharges;
  $sapOutput->powerFactorIncentive = $request->powerFactorIncentive;
  $sapOutput->demandCharges = $request->demandCharges;
  $sapOutput->waterCharges = $request->waterCharges;
  $sapOutput->voltageCharges = $request->voltageCharges;
  $sapOutput->peakLoadExemptionCharges = $request->peakLoadExemptionCharges;
  $sapOutput->sundryCharges = $request->sundryCharges;
  $sapOutput->miscellaneousCharges = $request->miscellaneousCharges;
  $sapOutput->fuelAdjustment = $request->fuelAdjustment;
  $sapOutput->billNumber = $request->billNumber;
  $sapOutput->noOfDaysBilledFor = $request->noOfDaysBilledFor;
  $sapOutput->billCycle = $request->billCycle;
  $sapOutput->billDate = $request->billDate;
  $sapOutput->dueDate = $request->dueDate;
  $sapOutput->billType = $request->billType;
  $sapOutput->paymantAmount = $request->paymantAmount;
  $sapOutput->paymantMode = $request->paymantMode;
  $sapOutput->checkNo = $request->checkNo;
  $sapOutput->bankName = $request->bankName;
  $sapOutput->paymentID = $request->paymentID;
  $sapOutput->IFSCcode = $request->IFSCcode;
  $sapOutput->MICRcode = $request->MICRcode;
  $sapOutput->paymentDate = $request->paymentDate;
  $sapOutput->paymentRemark = $request->paymentRemark;
  $sapOutput->totBillAmount = $request->totBillAmount;
  $sapOutput->SBMnumber = $request->SBMnumber;
  $sapOutput->meterReaderName = $request->meterReaderName;
  $sapOutput->inHouseOutsourcedSBM = $request->inHouseOutsourcedSBM;
  $sapOutput->transformerCode = $request->transformerCode;
  $sapOutput->mcbRent = $request->mcbRent;
  $sapOutput->LPSC = $request->LPSC;
  $sapOutput->totalAmountAfterDueDate = $request->totalAmountAfterDueDate;
  $sapOutput->totalOfNetSopNetEdNetOctrai = $request->totalOfNetSopNetEdNetOctrai;
  $sapOutput->subDivisionName = $request->subDivisionName;
  $sapOutput->consumerLegacyNumber = $request->consumerLegacyNumber;
  $sapOutput->consumerName = $request->consumerName;
  $sapOutput->houseNumber = $request->houseNumber;
  $sapOutput->address = $request->address;
  $sapOutput->category = $request->category;
  $sapOutput->subCategory = $request->subCategory;
  $sapOutput->billingCycle = $request->billingCycle;
  $sapOutput->billingGroup = $request->billingGroup;
  $sapOutput->phaseCode = $request->phaseCode;
  $sapOutput->sanctionedLoadKW = $request->sanctionedLoadKW;
  $sapOutput->totalOldMeterConsumptionUnitsKWH = $request->totalOldMeterConsumptionUnitsKWH;
  $sapOutput->totalOldMeterConsumptionUnitsKVAH = $request->totalOldMeterConsumptionUnitsKVAH;
  $sapOutput->newMeterInitialReadingKWH = $request->newMeterInitialReadingKWH;
  $sapOutput->newMeterInitialReadingKVAH = $request->newMeterInitialReadingKVAH;
  $sapOutput->sundryChargesSOP = $request->sundryChargesSOP;
  $sapOutput->sundryChargesED = $request->sundryChargesED;
  $sapOutput->sundryChargesOCTRAI = $request->sundryChargesOCTRAI;
  $sapOutput->sundryAllowancesSOP = $request->sundryAllowancesSOP;
  $sapOutput->sundryAllowancesED = $request->sundryAllowancesED;
  $sapOutput->sundryAllowancesOCTRAI = $request->sundryAllowancesOCTRAI;
  $sapOutput->otherCharges = $request->otherCharges;
  $sapOutput->roundAdjustmentAmount = $request->roundAdjustmentAmount;
  $sapOutput->adjustmentAmountSOP = $request->adjustmentAmountSOP;
  $sapOutput->adjustmentAmountED = $request->adjustmentAmountED;
  $sapOutput->adjustmentAmountOCTRAI = $request->adjustmentAmountOCTRAI;
  $sapOutput->provisionalAdjustmentAmountSOP = $request->provisionalAdjustmentAmountSOP;
  $sapOutput->provisionalAdjustmentAmountED = $request->provisionalAdjustmentAmountED;
  $sapOutput->provisionalAdjustmentAmountOCTRAI = $request->provisionalAdjustmentAmountOCTRAI;
  $sapOutput->arrearSOPCurrentYear = $request->arrearSOPCurrentYear;
  $sapOutput->arrearEDCurrentYear = $request->arrearEDCurrentYear;
  $sapOutput->arrearOCTRAICurrentYear = $request->arrearOCTRAICurrentYear;
  $sapOutput->arrearSOPPreviousYear = $request->arrearSOPPreviousYear;
  $sapOutput->arrearEDPreviousYear = $request->arrearEDPreviousYear;
  $sapOutput->arrearOCTRAIPreviousYear = $request->arrearOCTRAIPreviousYear;
  $sapOutput->advanceConsumptionDeposit = $request->advanceConsumptionDeposit;
  $sapOutput->complaintCenterPhoneNumber = $request->complaintCenterPhoneNumber;
  $sapOutput->meterSecurityAmount = $request->meterSecurityAmount;
  $sapOutput->nearestCashCounter = $request->nearestCashCounter;
  $sapOutput->multiplicationFactor = $request->multiplicationFactor;
  $sapOutput->overallMf = $request->overallMf;
  $sapOutput->contractedDemandKVA = $request->contractedDemandKVA;
  $sapOutput->miscExpensesDetails = $request->miscExpensesDetails;
  $sapOutput->previousKWHCycle1 = $request->previousKWHCycle1;
  $sapOutput->previousKWHCycle2 = $request->previousKWHCycle2;
  $sapOutput->previousKWHCycle3 = $request->previousKWHCycle3;
  $sapOutput->previousKWHCycle4 = $request->previousKWHCycle4;
  $sapOutput->previousKWHCycle5 = $request->previousKWHCycle5;
  $sapOutput->previousKWHCycle6 = $request->previousKWHCycle6;
  $sapOutput->roundingPresent = $request->roundingPresent;
  $sapOutput->estimationType = $request->estimationType;
  $sapOutput->ml = $request->ml;
  $sapOutput->mtFlag = $request->mtFlag;
  $sapOutput->feederCode = $request->feederCode;
  $sapOutput->scWsdAmountWithHeld = $request->scWsdAmountWithHeld;
  $sapOutput->fixedCharges = $request->fixedCharges;
  $sapOutput->mt = $request->mt;
  $sapOutput->previousFyCons = $request->previousFyCons;
  $sapOutput->meterReadingHigh = $request->meterReadingHigh;
  $sapOutput->meterReadingNote = $request->meterReadingNote;
  $sapOutput->amountOfCowCess = $request->amountOfCowCess;
  $sapOutput->save();
  // return $sapOutput['subDivisonCode'];
  try {
      // $client = new SoapClient ( "some.aspx?WSDL" );
      $client = new SoapClient("http://115.249.65.153/ws_billing_data_receiver/sap_sbm_data.asmx?wsdl");
      $params = array(
        'reqArr' => [
          'SAP_RequestParams' => [
            'SUB_DIVISION_CODE' => $sapOutput['subDivisonCode'],
            'MRU' => $sapOutput['MRU'],
            'CONNECTED_POLE_INI_NUMBER' => $sapOutput['connectedPoleNINNumber'],
            'NEIGHBOR_METER_NO' => $sapOutput['neighbourMeterNo'],
            'STREET_NAME' => $sapOutput['streetName'],
            'INSTALLATION' => $sapOutput['installation'],
            'MR_DOC_NO' => $sapOutput['MR_DOCNumber'],
            'SCHEDULED_MRDATE' => "2018-06-11T21:32:52",
            'METER_NUMBER' => $sapOutput['SAPMeterNumber'],
            'MANUFACTURER_SR_NO' => $sapOutput['manufacturerSRNo'],
            'MANUFACTURER_NAME' => $sapOutput['manufacturerName'],
            'CONTRACT_ACCOUNT_NUMBER' => $sapOutput['contractAcNumber'],
            'CONSUMPTION_KWH' => $sapOutput['consumptionKWH'],
            'CONSUMPTION_KWAH' => $sapOutput['consumptionKVAH'],
            'CONSUMPTION_KVA' => $sapOutput['consumptionKVA'],
            'CUR_METER_READING_KWH' => $sapOutput['currentMeterReadingKWH'],
            'CUR_METER_READING_KVA' => $sapOutput['currentMeterReadingKVA'],
            'CUR_METER_READING_KVAH' => $sapOutput['currentMeterReadingKVAH'],
            'CUR_METER_READING_DATE' => "2018-06-11T21:32:52",
            'CUR_METER_READING_TIME' => $sapOutput['currentMeterReadingTime'],
            'CUR_METER_READER_NOTE' => $sapOutput['currentNoteFromMeterReader'],
            'PRV_METER_READING_KWH' => $sapOutput['previousMeterReadingKWH'],
            'PRV_METER_READING_KVA' => $sapOutput['previousMeterReadingKVA'],
            'PRV_METER_READING_KWAH' => $sapOutput['previousMeterReadingKVAH'],
            'PRV_METER_READING_DATE' => "2018-06-11T21:32:52",
            'PRV_METER_READING_TIME' => $sapOutput['previousMeterReadingTime'],
            'PRV_METER_READER_NOTE' => $sapOutput['previoustNoteFromMeterReader'],
            'OCTROI_FLAG' => $sapOutput['octraiFlag'],
            'SOP' => $sapOutput['SOP'],
            'ED' => $sapOutput['ED'],
            'OCTROI' => $sapOutput['OCTRAI'],
            'DSSF' => $sapOutput['DSSF'],
            'SURCHARGE_LEIVED' => $sapOutput['surchargeLevied'],
            'SERVICE_RENT' => $sapOutput['serviceRent'],
            'METER_RENT' => $sapOutput['meterRent'],
            'SERVICE_CHARGE' => $sapOutput['serviceCharge'],
            'MONTHLY_MIN_CHARGES' => $sapOutput['monthlyMinimumCharges'],
            'PF_SURCHARGE' => $sapOutput['powerFactorSurcharges'],
            'PF_INCENTIVE' => $sapOutput['powerFactorIncentive'],
            'DEMAND_CHARGES' => $sapOutput['demandCharges'],
            'FIXEDCHARGES' => "x",
            'VOLTAGE_SURCHARGE' => $sapOutput['voltageCharges'],
            'PEAKLOAD_EXEMPTION_CHARGES' => $sapOutput['peakLoadExemptionCharges'],
            'SUNDRY_CHARGES' => $sapOutput['sundryCharges'],
            'MISCELLANEOUS_CHARGES' => $sapOutput['miscellaneousCharges'],
            'FUEL_ADJUSTMENT' => $sapOutput['fuelAdjustment'],
            'BILL_NUMBER' => $sapOutput['billNumber'],
            'NO_OF_DAYS_BILLED' => $sapOutput['noOfDaysBilledFor'],
            'BILL_CYCLE' => $sapOutput['billCycle'],
            'BILL_DATE' => "2018-06-11T21:32:52",
            'DUE_DATE' => "2018-06-11T21:32:52",
            'BILL_TYPE' => $sapOutput['billType'],
            'PAYMENT_AMOUNT' => $sapOutput['paymantAmount'],
            'PAYMENT_MODE' => $sapOutput['paymantMode'],
            'CHECK_NO' => $sapOutput['checkNo'],
            'BANK_NAME' => $sapOutput['bankName'],
            'PAYMENT_ID' => $sapOutput['paymentID'],
            'IFSC_CODE' => $sapOutput['IFSCcode'],
            'MICRCODE' => $sapOutput['MICRcode'],
            'PAYMENT_DATE' => $sapOutput['paymentDate'],
            'PAYMENT_REMARK' => $sapOutput['paymentRemark'],
            'TOT_BILLAMOUNT' => $sapOutput['totBillAmount'],
            'SBM_NUMBER' => $sapOutput['SBMnumber'],
            'METER_READER_NAME' => $sapOutput['meterReaderName'],
            'INHOUSE_OUTSOURCED_SBM' => $sapOutput['inHouseOutsourcedSBM'],
            'TRANSFROMERCODE' => $sapOutput['transformerCode'],
            'MCB_RENT' => $sapOutput['mcbRent'],
            'LPSC' => $sapOutput['LPSC'],
            'TOT_AMT_DUE_DATE' => $sapOutput['totalAmountAfterDueDate'],
            'TOT_SOP_ED_OCT' => $sapOutput['totalOfNetSopNetEdNetOctrai'],
            'SUBDIVISIONNAME' => $sapOutput['subDivisionName'],
            'CONSUMERLEGACYNUMBER' => $sapOutput['consumerLegacyNumber'],
            'CONSUMERNAME' => $sapOutput['consumerName'],
            'HOUSENUMBER' => $sapOutput['houseNumber'],
            'ADDRESS' => $sapOutput['address'],
            'CATEGORY' => $sapOutput['category'],
            'SUBCATEGORY' => $sapOutput['subCategory'],
            'BILLINGCYCLE' => $sapOutput['billingCycle'],
            'BILLINGGROUP' => $sapOutput['billingGroup'],
            'PHASECODE' => $sapOutput['phaseCode'],
            'SANCTIONEDLOAD_KW' => $sapOutput['sanctionedLoadKW'],
            'TOTALOLDMETER_CONS_UNIT_KWH' => $sapOutput['totalOldMeterConsumptionUnitsKWH'],
            'TOTALOLDMETER_CONS_UNIT_KVAH' => $sapOutput['totalOldMeterConsumptionUnitsKVAH'],
            'NEWMETERINITIALREADING_KWH' => $sapOutput['newMeterInitialReadingKWH'],
            'NEWMETERINITIALREADING_KVAH' => $sapOutput['newMeterInitialReadingKVAH'],
            'SUNDRYCHRS_SOP' => $sapOutput['sundryChargesSOP'],
            'SUNDRYCHRS_ED' => $sapOutput['sundryChargesED'],
            'SUNDRYCHRS_OCTRAI' => $sapOutput['sundryChargesOCTRAI'],
            'SUNDRYALLOWANCES_SOP' => $sapOutput['sundryAllowancesSOP'],
            'SUNDRYALLOWANCES_ED' => $sapOutput['sundryAllowancesED'],
            'SUNDRYALLOWANCES_OCTRAI' => $sapOutput['sundryAllowancesOCTRAI'],
            'OTHERCHRS' => $sapOutput['otherCharges'],
            'ROUNDADJUSTMENTAMNT' => $sapOutput['roundAdjustmentAmount'],
            'ADJUSTMENTAMT_SOP' => $sapOutput['adjustmentAmountSOP'],
            'ADJUSTMENTAMT_ED' => $sapOutput['adjustmentAmountED'],
            'ADJUSTMENTAMT_OCTRAI' => $sapOutput['adjustmentAmountOCTRAI'],
            'PROVISIONALADJUSTMENTAMT_SOP' => $sapOutput['provisionalAdjustmentAmountSOP'],
            'PROVISIONALADJUSTMENTAMT_ED' => $sapOutput['provisionalAdjustmentAmountED'],
            'PROVADJUSTMENTAMT_OCTRAI' => $sapOutput['provisionalAdjustmentAmountOCTRAI'],
            'ARREARSOPCURRENTYEAR' => $sapOutput['arrearSOPCurrentYear'],
            'ARREAREDCURRENTYEAR' => $sapOutput['arrearEDCurrentYear'],
            'ARREAROCTRAICURRENTYEAR' => $sapOutput['arrearOCTRAICurrentYear'],
            'ARREARSOPPREVIOUSYEAR' => $sapOutput['arrearSOPPreviousYear'],
            'ARREAREDPREVIOUSYEAR' => $sapOutput['arrearEDPreviousYear'],
            'ARREAROCTRAIPREVIOUSYEAR' => $sapOutput['arrearOCTRAIPreviousYear'],
            'ADVANCECONS_DEPOSIT' => $sapOutput['advanceConsumptionDeposit'],
            'COMPLAINTCENTERPHONENUMBER' => $sapOutput['complaintCenterPhoneNumber'],
            'METERSECURITYAMT' => $sapOutput['meterSecurityAmount'],
            'NEARESTCASHCOUNTER' => $sapOutput['nearestCashCounter'],
            'MULTIPLICATIONFACTOR' => $sapOutput['multiplicationFactor'],
            'OVERALLMF' => $sapOutput['overallMf'],
            'CONTRACTEDLOAD_KVA' => $sapOutput['contractedDemandKVA'],
            'MISCEXPENSESDETAILS' => $sapOutput['miscExpensesDetails'],
            'PREVIOUSKWHCYCLE_1' => $sapOutput['previousKWHCycle1'],
            'PREVIOUSKWHCYCLE_2' => $sapOutput['previousKWHCycle2'],
            'PREVIOUSKWHCYCLE_3' => $sapOutput['previousKWHCycle3'],
            'PREVIOUSKWHCYCLE_4' => $sapOutput['previousKWHCycle4'],
            'PREVIOUSKWHCYCLE_5' => $sapOutput['previousKWHCycle5'],
            'PREVIOUSKWHCYCLE_6' => $sapOutput['previousKWHCycle6'],
            'ROUNDING_PRESENT' => $sapOutput['roundingPresent'],
            'ESTIMATION_TYPE' => $sapOutput['estimationType'],
            'RECTYPE' => "1",
            'USERID' => "STRTECH",
            'PASSCODE' => "PSPCL@321",
            'VID' => "1",
            ]
          ]

          );

      $result = $client->PushSAP_Data($params);
      return $result;

      $xml = simplexml_load_string($result->SAP_ResponseParams->any);
      $res = $xml->STATUS;
      return $xml;
      echo "<pre>" ;
      foreach ($xml as $key => $value)
      {
        $key . " = " .$value . PHP_EOL;
          // foreach($value as $ekey => $eValue)
          // {
          //     print($ekey . " = " . $eValue . PHP_EOL);

          // }
      }

  } catch ( SoapFault $fault ) {
    echo "error";
      trigger_error ( "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR );
  }
  return "Seccess";
});

Route::middleware('jsonx')->get('/sap-push',  function (Request $request) {
  return [
    'successMessage' => "Y"
  ];
  return $request;
  $bill = new sapInput();
  $bill->subDivisionCode = $request->subDivisionCode;
  $bill->subDivisionName = $request->subDivisionName;
  $bill->MRU = $request->MRU;
  $bill->MR_DOCNumber = $request->MR_DOCNumber;
  $bill->neighbourMeterId = $request->neighbourMeterId;
  $bill->connectedPoleNINNumber = $request->connectedPoleNINNumber;
  $bill->consumerLegacyNumber = $request->consumerLegacyNumber;
  $bill->contractAcNumber = $request->contractAcNumber;
  $bill->consumerName = $request->consumerName;
  $bill->houseNumber = $request->houseNumber;
  $bill->streetNumber = $request->streetNumber;
  $bill->address = $request->address;
  $bill->category = $request->category;
  $bill->subCategory = $request->subCategory;
  $bill->moveInFlag = $request->moveInFlag;
  $bill->categoryChangeFlag = $request->categoryChangeFlag;
  $bill->categoryChangeDate = $request->categoryChangeDate;
  $bill->oldCategory = $request->oldCategory;
  $bill->oldSubCategory = $request->oldSubCategory;
  $bill->industryCode = $request->industryCode;
  $bill->billingCycle = $request->billingCycle;
  $bill->billingCycleType = $request->billingCycleType;
  $bill->billingGroup = $request->billingGroup;
  $bill->triVectorMeterFlag = $request->triVectorMeterFlag;
  $bill->installation = $request->installation;
  $bill->meterMnfSerialNumber = $request->meterMnfSerialNumber;
  $bill->meterManufacturerName = $request->meterManufacturerName;
  $bill->meterSAPSerialNumber = $request->meterSAPSerialNumber;
  $bill->multiplicationFactor = $request->multiplicationFactor;
  $bill->overallMf = $request->overallMf;
  $bill->oldMf = $request->oldMf;
  $bill->numberOfDigits = $request->numberOfDigits;
  $bill->lineCtRatio = $request->lineCtRatio;
  $bill->externalPtRatio = $request->externalPtRatio;
  $bill->phaseCode = $request->phaseCode;
  $bill->sanctionedLoadKW = $request->sanctionedLoadKW;
  $bill->contractedDemandKVA = $request->contractedDemandKVA;
  $bill->sanctionedLoadChangeFlag = $request->sanctionedLoadChangeFlag;
  $bill->sanctionedLoadChangeDate = $request->sanctionedLoadChangeDate;
  $bill->oldSanctionedLoadKW = $request->oldSanctionedLoadKW;
  $bill->admissibleVoltage = $request->admissibleVoltage;
  $bill->supplyVoltage = $request->supplyVoltage;
  $bill->noOfBOardEmployees6 = $request->noOfBOardEmployees6;
  $bill->noOfBOardEmployees7 = $request->noOfBOardEmployees7;
  $bill->noOfBOardEmployees8 = $request->noOfBOardEmployees8;
  $bill->scheduleMeterReadDate = $request->scheduleMeterReadDate;
  $bill->previousReadingDate = $request->previousReadingDate;
  $bill->previousBillDate = $request->previousBillDate;
  $bill->previousReadingKWH = $request->previousReadingKWH;
  $bill->previousReadingKVA = $request->previousReadingKVA;
  $bill->previousReadingKVAH = $request->previousReadingKVAH;
  $bill->previousMeterStatus = $request->previousMeterStatus;
  $bill->previousBillType = $request->previousBillType;
  $bill->meterChangeFlag = $request->meterChangeFlag;
  $bill->meterChangeDate = $request->meterChangeDate;
  $bill->totalOldMeterConsumptionUnitsKWH = $request->totalOldMeterConsumptionUnitsKWH;
  $bill->totalOldMeterConsumptionUnitsKVAH = $request->totalOldMeterConsumptionUnitsKVAH;
  $bill->oldMeterTotalRentAmount = $request->oldMeterTotalRentAmount;
  $bill->oldMeterTotalServiceRentAmount = $request->oldMeterTotalServiceRentAmount;
  $bill->oldMeterTotalServiceChargeAmount = $request->oldMeterTotalServiceChargeAmount;
  $bill->oldMeterTotalMBCRentAmount = $request->oldMeterTotalMBCRentAmount;
  $bill->newMeterInitialReadingKWH = $request->newMeterInitialReadingKWH;
  $bill->newMeterInitialReadingKVAH = $request->newMeterInitialReadingKVAH;
  $bill->reconnectionFlag = $request->reconnectionFlag;
  $bill->reconnectionDate = $request->reconnectionDate;
  $bill->consumptionBeforeDisConnectionKWH = $request->consumptionBeforeDisConnectionKWH;
  $bill->consumptionBeforeDisConnectionKVAH = $request->consumptionBeforeDisConnectionKVAH;
  $bill->periodBeforeDisConnection = $request->periodBeforeDisConnection;
  $bill->edExemptedFlag = $request->edExemptedFlag;
  $bill->edExemptionExpiry = $request->edExemptionExpiry;
  $bill->octraiChargesApplicableFlag = $request->octraiChargesApplicableFlag;
  $bill->meterRentRate = $request->meterRentRate;
  $bill->serviceRentRate = $request->serviceRentRate;
  $bill->peakLoadExemptionCharges = $request->peakLoadExemptionCharges;
  $bill->peakLoadExemptionChargesExpiry = $request->peakLoadExemptionChargesExpiry;
  $bill->MMTSCorrectionFactor = $request->MMTSCorrectionFactor;
  $bill->shuntCapacitorChargesApplicableFlag = $request->shuntCapacitorChargesApplicableFlag;
  $bill->capacityOfCapacitor = $request->capacityOfCapacitor;
  $bill->MBCRentRate = $request->MBCRentRate;
  $bill->serviceChargeRate = $request->serviceChargeRate;
  $bill->seasonStartDate = $request->seasonStartDate;
  $bill->seasonEndDate = $request->seasonEndDate;
  $bill->sundryChargesSOP = $request->sundryChargesSOP;
  $bill->sundryChargesED = $request->sundryChargesED;
  $bill->sundryChargesOCTRAI = $request->sundryChargesOCTRAI;
  $bill->sundryAllowancesSOP = $request->sundryAllowancesSOP;
  $bill->sundryAllowancesED = $request->sundryAllowancesED;
  $bill->sundryAllowancesOCTRAI = $request->sundryAllowancesOCTRAI;
  $bill->waterChargesProvisionalAmount = $request->waterChargesProvisionalAmount;
  $bill->otherCharges = $request->otherCharges;
  $bill->roundAdjustmentAmount = $request->roundAdjustmentAmount;
  $bill->adjustmentAmountSOP = $request->adjustmentAmountSOP;
  $bill->adjustmentAmountED = $request->adjustmentAmountED;
  $bill->adjustmentAmountOCTRAI = $request->adjustmentAmountOCTRAI;
  $bill->provisionalAdjustmentAmountSOP = $request->provisionalAdjustmentAmountSOP;
  $bill->provisionalAdjustmentAmountED = $request->provisionalAdjustmentAmountED;
  $bill->provisionalAdjustmentAmountOCTRAI = $request->provisionalAdjustmentAmountOCTRAI;
  $bill->arrearSOPCurrentYear = $request->arrearSOPCurrentYear;
  $bill->arrearEDCurrentYear = $request->arrearEDCurrentYear;
  $bill->arrearOCTRAICurrentYear = $request->arrearOCTRAICurrentYear;
  $bill->arrearSOPPreviousYear = $request->arrearSOPPreviousYear;
  $bill->arrearEDPreviousYear = $request->arrearEDPreviousYear;
  $bill->arrearOCTRAIPreviousYear = $request->arrearOCTRAIPreviousYear;
  $bill->advanceConsumptionDeposit = $request->advanceConsumptionDeposit;
  $bill->courtCaseAmount = $request->courtCaseAmount;
  $bill->previousKWHCycle1 = $request->previousKWHCycle1;
  $bill->previousKWHCycle2 = $request->previousKWHCycle2;
  $bill->previousKWHCycle3 = $request->previousKWHCycle3;
  $bill->previousKWHCycle4 = $request->previousKWHCycle4;
  $bill->previousKWHCycle5 = $request->previousKWHCycle5;
  $bill->previousKWHCycle6 = $request->previousKWHCycle6;
  $bill->averageKWHofAbove6Cycles = $request->averageKWHofAbove6Cycles;
  $bill->samePeriodLastYearConsumptionKWH = $request->samePeriodLastYearConsumptionKWH;
  $bill->PF1 = $request->PF1;
  $bill->PF2 = $request->PF2;
  $bill->PF3 = $request->PF3;
  $bill->averageofAboveThreePF = $request->averageofAboveThreePF;
  $bill->standardPFOfConsumerCategory = $request->standardPFOfConsumerCategory;
  $bill->MD1 = $request->MD1;
  $bill->MD2 = $request->MD2;
  $bill->gaushalaFlag = $request->gaushalaFlag;
  $bill->arrearWaterChargesCurrentYear = $request->arrearWaterChargesCurrentYear;
  $bill->arrearWaterChargesPreviousYear = $request->arrearWaterChargesPreviousYear;
  $bill->dataExpiryDate = $request->dataExpiryDate;
  $bill->maxOfAboveSixMDI = $request->maxOfAboveSixMDI;
  $bill->factorForCustomerCategory = $request->factorForCustomerCategory;
  $bill->DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory = $request->DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory;
  $bill->HValueInLDHFSupplyHoursPerDayForCustomerCategory = $request->HValueInLDHFSupplyHoursPerDayForCustomerCategory;
  $bill->previousTotalEstimatedConsumptionKWH = $request->previousTotalEstimatedConsumptionKWH;
  $bill->previousTotalEstimatedConsumptionKVAH = $request->previousTotalEstimatedConsumptionKVAH;
  $bill->previousEstimatedMDI = $request->previousEstimatedMDI;
  $bill->nearestCashCounter = $request->nearestCashCounter;
  $bill->complaintCenterPhoneNumber = $request->complaintCenterPhoneNumber;
  $bill->previousPaymentStatus = $request->previousPaymentStatus;
  $bill->reasonForAdjustment = $request->reasonForAdjustment;
  $bill->miscExpensesDetails = $request->miscExpensesDetails;
  $bill->miscExpenses = $request->miscExpenses;
  $bill->meterSecurityAmount = $request->meterSecurityAmount;
  $bill->waterChargesApplicableFlag = $request->waterChargesApplicableFlag;
  $bill->transformerCode = $request->transformerCode;
  $bill->oldMeterRent = $request->oldMeterRent;
  $bill->expirationDateOfOldMeterRent = $request->expirationDateOfOldMeterRent;
  $bill->oldMBCRent = $request->oldMBCRent;
  $bill->expirationDateOfOldMBCRent = $request->expirationDateOfOldMBCRent;
  $bill->oldServiceCharges = $request->oldServiceCharges;
  $bill->expirationDateOfOldServiceCharges = $request->expirationDateOfOldServiceCharges;
  $bill->meterLocation = $request->meterLocation;
  $bill->meterType = $request->meterType;
  $bill->version = $request->version;
  $bill->ML = $request->ML;
  $bill->MT = $request->MT;
  $bill->mobileNo = $request->mobileNo;
  $bill->feederCode = $request->feederCode;
  $bill->SCWSDAmountWithHeld = $request->SCWSDAmountWithHeld;
  $bill->previousFyCons = $request->previousFyCons;
  $bill->muncipalTaxApplicableFlag = $request->muncipalTaxApplicableFlag;
  $bill->PLogicApplicableFlag = $request->PLogicApplicableFlag;
  $bill->limitForPNore = $request->limitForPNore;

  $bill->save();
  return "Success";
});
Route::get('/sap-soap',  'preDataController@show');

Route::middleware('auth:api')->post('/data', function (Request $request) {

  if ($request->id != '') {
    $read = Bill::find($request->id);
    $readStatus = $read->status;

    $userDetails = $request->user();
    // return $userDetails->id;
    if ($readStatus !== 3) {
      $newBill = new Billed();
      $newBill->blMeterReader = $userDetails->id;
      $newBill->blSubDivisionCode = substr($request->blMasterKey, 0, 3);
      $newBill->blMasterKey = $request->blMasterKey;
      $newBill->blConsumerName = $request->blConsumerName;
      $newBill->blTariffType = $request->blTariffType;
      $newBill->blBillingGroup = $request->blBillingGroup;
      $newBill->blBillingCycle = $request->blBillingCycle;
      $newBill->blCurrentMeterRent = $request->blCurrentMeterRent;
      $newBill->blCurrentServiceRent = $request->blCurrentServiceRent;
      $newBill->blPresentReading = $request->blPresentReading;
      $newBill->blPresentReadingDate = $request->blPresentReadingDate;
      $readDateFormat = DateTime::createFromFormat('d/m/Y', $request->blPresentReadingDate);
      $newBill->blPresentReadingDateFormated = $readDateFormat->format('Y-m-d');
      $newBill->blPresentStatus = $request->blPresentStatus;
      $newBill->blUnitsConsumed = $request->blUnitsConsumed;
      $newBill->blDays = $request->blDays;
      $newBill->blMMCFlag = $request->blMMCFlag;
      $newBill->blCurrentSop = $request->blCurrentSop;
      $newBill->blCurrentEd = $request->blCurrentEd;
      $newBill->blCurrentOctroi = $request->blCurrentOctroi;
      $newBill->blNetSop = $request->blNetSop;
      $newBill->blNetEd = $request->blNetEd;
      $newBill->blNetOctroi = $request->blNetOctroi;
      $newBill->blCurrentRoundingAmt = $request->blCurrentRoundingAmt;
      $newBill->blAmountBeforeDue = $request->blAmountBeforeDue;
      $newBill->blSurCharge = $request->blSurCharge;
      $newBill->blDueDateByCash = $request->blDueDateByCash;
      $newBill->blDueDateByCheque = $request->blDueDateByCheque;
      $newBill->blAmountAfterDue = $request->blAmountAfterDue;
      $newBill->blTime = $request->blTime;
      $newBill->blConcessionalUnits = $request->blConcessionalUnits;
      $newBill->blCurrentIDFWithSign = $request->blCurrentIDFWithSign;
      $newBill->blCurrentCowCessWithSign = $request->blCurrentCowCessWithSign;
      $newBill->blCurrentWaterSewageChargeWithSign = $request->blCurrentWaterSewageChargeWithSign;
      $newBill->bltotalIDFWithSign = $request->bltotalIDFWithSign;
      $newBill->blTotalCowCessWithSign = $request->blTotalCowCessWithSign;
      $newBill->blTotalCurrentWaterSewarageChargeWithSign = $request->blTotalCurrentWaterSewarageChargeWithSign;
      $newBill->blCurrentFCAChargesWithSign = $request->blCurrentFCAChargesWithSign;
      $newBill->blCurrentFixedChargesWithSign = $request->blCurrentFixedChargesWithSign;
      $newBill->blLat = $request->blLat;
      $newBill->blLong = $request->blLong;
      $newBill->blEmail = $request->blEmail;
      $newBill->blMobile = $request->blMobile;
      $newBill->blRemark = $request->blRemarks;
      $newBill->save();

      $billDetails = Bill::find($newBill->id);
      try {
          // $client = new SoapClient ( "some.aspx?WSDL" );
          $client = new SoapClient("http://115.249.65.153/ws_billing_data_receiver/nonsap_sbm_data.asmx?wsdl");
          $params = array(
            'reqArr' => [
              'SAP_RequestParams' => [
                'ACCOUNTNO' => $newBill['blMasterKey'],
                'NAME' => $newBill['blConsumerName'],
                'ADDRESS' => $billDetails['consumerAddress'], // From Bills Table
                'ISSUEDATE' => $newBill['blPresentReadingDate'],
                'DUEDATECASH' => "2018-06-11T21:32:52", // Need to format
                'DUEDATECHEQUE' => "2018-06-11T21:32:52", // Need to format
                'VILLCITYNAME' => "X",
                'SUBDIVNAME' => $billDetails['subDivisionName'], // Form Bills Table
                'BILLNO' => "X", // Need to Generate
                'CURRENTREADINGDATE' => $newBill['blPresentReadingDate'],
                'PREVREADINGDATE' => $billDetails['previousReadingDate'], // Frm bills Table
                'BILLPERIOD' => $newBill['blDays'],
                'TARIFFTYPE' => $newBill['blTariffType'],
                'CONNECTEDLOAD' => $billDetails['connectedLoadInKW'], // Verify
                'PREVBILLSTATUS' => $billDetails['perviousCode'], // From bills Table
                'METERNO' => $billDetails['meterNo'], // From bills Table
                'SECURITYAMT' => $billDetails['securityDeposit'], // From bills Table
                'BILLSTATUS' => $newBill['blPresentStatus'],
                'BILLCYCLE' => $newBill['blBillingCycle'],
                'BILLGROUP' => $newBill['blBillingGroup'],
                'METERREADINGCURRENT' => $newBill['blPresentReading'], // Verify
                'METERREADINGPREV' => $billDetails['consumtionOfOldMeter'], // From bills Table "Verify"
                'LINECTRATIO' => $billDetails['lineCtRatio'], // From Bills Table
                'METERCTRATIO' => $billDetails['meterCtRatio'], // From Bills Table
                'METERMULTIPLIER' => $billDetails['meterMultiplier'], // From Bills Table
                'OVERMETERMULTIPLIER' => $billDetails['overallMultiplingFactor'], // From Bills Table
                'METERCODE' => "X", // Verify
                'UNITSCONSUMEDNEW' => $newBill['blUnitsConsumedNew'],
                'UNITSCONSUMEDOLD' => $newBill['blUnitsConsumedOld'],
                'TOTALUNITSCONSUMED' => $newBill['blUnitsConsumed'],
                'CURRENTSOP' => $newBill['blCurrentSop'],
                'CURRENTED' => $newBill['blCurrentEd'],
                'OCTROI' => $newBill['blCurrentOctroi'],
                'METERRENT' => $newBill['blCurrentMeterRent'],
                'SERVICERENT' => $newBill['blCurrentServiceRent'],
                'ADJAMT' => $billDetails['adjustmentAmount'], // From Bills Table
                'ADJPERIOD' => $billDetails['periodOfAdjustmentDetail'], // From Bills TAble
                'ADJREASON' => $billDetails['reason'], // From Bills TAble
                'CONSUNITS' => $newBill['blConcessionalUnits'], // Verify
                'FIXEDCHARGES' => $newBill['blCurrentFixedChargesWithSign'], // Verify
                'FUELCOST' => $newBill['blCurrentFCAChargesWithSign'], // Verify
                'VOLTCLASSCHARGES' => $billDetails['voltageClass'], // From Bills Table
                'PREVTOTALARR' => $billDetails['previousArrear'], // From Bills Table
                'CURRENTTOTALARREAR' => $billDetails['currentArrear'], // From Bills Table
                'OTHERCHARGES' => $billDetails['avrageOthers'], // Verify
                'SUNDARYCHARGES' => (int) $billDetails['sundryIDF'] + (int) $billDetails['sundryCowCess'] + (int) $billDetails['sundryWaterSewarageChargesCode'], // Verify
                'SUNDARYALLOWANCE' => (int) $billDetails['allowanceIDF'] + (int) $billDetails['allowanceCowCess'] + (int) $billDetails['allowanceWaterSewarageChargesCode'], // Verify
                'CURRENTROUNDEDAMT' => $newBill['blCurrentRoundingAmt'],
                'PREVROUNDEDAMT' => $billDetails['previousRoundingAmount'],
                'TOTALNETSOP' => $newBill['blNetSop'],
                'TOTALNETED' => $newBill['blNetEd'],
                'TOTALNETOCTROI' => $newBill['blNetOctroi'],
                'TOTALAMT' => $newBill['blAmountBeforeDue'], // Verify
                'TOTALSURCHARGE' => $newBill['blSurCharge'],
                'TOTALAMTGROSS' => $newBill['blAmountAfterDue'], // Verify
                'BILLYEAR' => Date('Y'), // Verify
                'SUNDARYMESSAGE' => "X", // Verify
                'SUNDARYDTFROM' => "X", // Verify
                'SUNDARYDTTO' => "X", // Verify
                'LINE1' => "X", // Verify
                'LINE2' => "X", // Verify
                'PREVCYCLECONSUMPTION1' => substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
                'PREVCYCLECONSUMPTION2' => substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
                'PREVCYCLECONSUMPTION3' => substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
                'PREVCYCLECONSUMPTION4' => substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
                'PREVCYCLECONSUMPTION5' => substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
                'PREVCYCLECONSUMPTION6' => substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
                'AMTPAID' => "X",
                'AMTDATE' => "X",
                'WEBCCCR' => "X", // Verify
                'FINANCIALYEAR' => "X", // Verify
                'SPOTREC' => "X", // Verify
                'CURRIDF' => $newBill['blCurrentIDFWithSign'], // Verify
                'CCOWCESS' => $newBill['blCurrentCowCessWithSign'], // Verify
                'CWATCESS' => $newBill['blCurrentWaterSewageChargeWithSign'],
                'TOTIDF' => $newBill['bltotalIDFWithSign'],
                'TCOWCESS' => $newBill['blTotalCowCessWithSign'],
                'TWATCESS' => $newBill['blTotalCurrentWaterSewarageChargeWithSign'],
                'EXCOL' => "1",
                'USERID' => "STRTECH",
                'PASSCODE' => "PSPCL@321",
                'VID' => "1"
                ]
              ]

              );

          $result = $client->PushSAP_Data($params);
          return $result;

          $xml = simplexml_load_string($result->SAP_ResponseParams->any);
          $res = $xml->STATUS;
          return $xml;
          echo "<pre>" ;
          foreach ($xml as $key => $value)
          {
            $key . " = " .$value . PHP_EOL;
              // foreach($value as $ekey => $eValue)
              // {
              //     print($ekey . " = " . $eValue . PHP_EOL);

              // }
          }

      } catch ( SoapFault $fault ) {
        echo "error";
          trigger_error ( "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR );
      }

      $billsUpdate = Bill::where('id', $request->id)
                         ->where('status', 2)
                         ->update(
                           [
                             'status' => 3
                           ]
                         );
       if ($newBill) {
         $message['status'] = 'success';
         // return $newBill;
       } else {
         $message['status'] = 'Error';
       }
    } else {
      $message['status'] = 'Already Updated To Server';
    }

    // return Bill::find($request->id);

    // if ($readStatus == 3) {
    //   $message['status'] = 'Already Submitted';
    // } else {
    //
    // }
  } else {
    $message['status'] = 'Error';
  }

  return $message;
});

Route::middleware('auth:api')->post('/in-complete-data', function (Request $request) {

  $implode = explode(",", $request->ids);
    $billUpdate = Bill::whereIn('id', $implode)->update(['status' => 0, 'meterReader' => 0]);

    if ($billUpdate) {
      $message['status'] = 'success';
    } else {
      $message['status'] = 'error';
    }

    return $message;

});


// Route::get('/my_first_api', function () {
//   return 'po';
// });
Route::get('/my_first_api', 'HomeController@my_first_api');
